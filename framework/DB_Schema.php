<?php 

/*********************************************************************
 *
 *	Derek's Generic Schema Class
 *
 *	The generic class is meant for connecting HTML forms to insert, update, delete, etc commands.
 *
 *	To this end, there are generic find, insert, get, update, delete methods in this class.
 *
 *	When you subclass this class, override find/insert etc if you need to customize the logic as to
 *	how these tasks are performed.  Generally, if your field names match the table column names, you
 *	don't have to do anything further as the generic logic will line up nicely.
 *
 *	If your table or app logic needs to make a slightly more complex model, override the _construct
 *	method and set your own schema info:
 *
 *	Example:
 *
 *	class EmployeeModel extends DB_Schema {
 *		public function _construct() {
 *			parent::_construct(); //always call the parent first
 *
 *			$this->table = "employees"; //set your table name
 *			$this->primaryKey = "employeeID";  //set your primary key if its different from 'id'
 *			$this->init(); //initialize the class's structures
 *			//now, if you have any constraints or other DB manipulations like WHERE clauses, apply them
 *			$this->DB->constrain("account_id",$_SESSION['account_id']);
 * 		}
 *  }
 *
 *	That's it, you are done.  You can now do this:
 *
 *	$employeeModel = new EmployeeModel();
 *	$person = $employeeModel->find(100);  //let's go find employee #100, this kicks back a single DB_Record
 *
 *  You can also do:
 *
 *  $employeeModel->update($_POST); //does POST contain form fields that match this table's column names? This will work.
 *
 *	$employeeModel->delete(100);  //oops we fired #100
 *
 *	That's it, now your subclass only contains relevant override code if needed and is otherwise just a little bit of
 *	schema data.
 * 
 *********************************************************************/

class DB_Schema {
	public $table;
	public $primaryKey;
	public $DB;
	public $initialized = false;
	public $columns;
	public $formTypes = [];
	public $displayOrder = [];
	public $linkFormats = [];
	public $linkables = [];
	public $orderBy = "";
	public $limit = "";
	
	public function __construct() {
		$this->table = "TABLE_NOT_SET_ERROR";
		$this->primaryKey = "id";
	}
	
	/**
	 * Conform function
	 *
	 * purpose of this function is to accept array of values, usually from $_POST, check it against any entries in the
	 * $columns array, and then return an array where they match up.
	 *
	 * Example:
	 * $_POST = ["command" => "update",
	 *           "specialFormItem" => "62",
	 *           "storeCookie" => "true",
	 *           "id" => "10",
	 *           "startDate" => "20170301"];
	 *
	 *  So we got sent a form with POST data that includes fields that belong to our record, but also got fields that
	 *  might be related to css or javascript purposes.  Our schema is stored in $columns:
	 *
	 *  $columns = ["id","startDate","endDate","description"];
	 *
	 *  This function then crosses the two and returns:
	 *
	 *  $conforming = ["id" => "10", "startDate" => "20170301"];
	 *
	 *  After we get the conforming fields, we can do further data validation and allow a database action like update
	 *  or insert.
	 *
	 *  If you didn't bother to define any column schema, it accepts all the inbound fields without complaint.
	 * 
	 */
	public function conform($fields) {
		if (count($this->columns)==0) return $fields;
		$conform = [];
		foreach ($this->columns as $fieldname) {
			if (array_key_exists($fieldname,$fields)) {
				$conform[$fieldname] = $fields[$fieldname];
			}
		}
		return $conform;
	}
	
	public function constrain($where, $val, $operator = " = ",$conCat = " AND ") {
		if ($this->initialized == false) return;
		$this->DB->constrain($where, $val, $operator = " = ",$conCat = " AND ");
	}
	
	public function learnColumns(){
		$db = Database::GetSharedDatabase();
		$result = $db->query("select * from " . $this->table);
		$fields = $result->fetch_fields();
		foreach ($fields as $field) {
		    $this->columns[] = $field->name;
			$this->formTypes[$field->name] = ["type" => "text"];
		}
	}
	
	public function init() {
		$this->learnColumns();
		$this->initialized = true;
		$this->DB = DB::table($this->table);
		$this->DB->primaryKey = $this->primaryKey;
	}
	
	
	//this should be overridden for custom WHERE and SELECT logic
	public function recordProvider() {
		return $this->DB->orderBy($this->orderBy)->limit($this->limit)->get(); 
	}
	
	public function getAll() {
		if ($this->initialized == false) return null;
        return $this->recordProvider(); 
    }
	
	public function search($column,$value) {
		if ($this->initialized == false) return null;
		$this->DB->whereLike($this->table.".".$column,$value);
		$record = $this->recordProvider();
        return $record;
	}
	
	public function find($ID) {
		if ($this->initialized == false) return null;
		$this->DB->where($this->table.".".$this->primaryKey,$ID);
		$record = $this->recordProvider();
		if ($record!=null) {
			$record = $record[0]; //find should only return a single record, not array
		}
        return $record;
    }
    
	public function insert($fields) {
		if ($this->initialized == false) return FAILURE;
		$fields = $this->conform($fields);
		$record = new DB_Record($fields);
		$record->primaryKey = $this->primaryKey;
		if ($this->DB->insert($record)) {
			return SUCCESS;
		} else {
			//echo $this->DB->error;
		    return FAILURE;
		}
	}
    
	public function update($fields) {
		if ($this->initialized == false) return FAILURE;
		$fields = $this->conform($fields);
		$record = new DB_Record($fields, true);
		$record->primaryKey = $this->primaryKey;
		if ($this->DB->update($record)) {
		    return SUCCESS;
		} else {
		    return FAILURE;
		}
	}
    
	public function delete($ID) {
		if ($this->initialized == false) return;
		$this->DB->where($this->primaryKey,$ID)->delete();
		$this->DB->reset();//clear whatever where clause this makes
	}
	
	public function formElements($activeRecord = null) {
		ob_start();
		foreach ($this->columns as $formElement){
			$label = isset($this->formTypes[$formElement]["label"]) ? $this->formTypes[$formElement]["label"] : ucwords($formElement);
			$value = "";
			if ($activeRecord != null) {
				$value = $activeRecord->attributes[$formElement];
			}
			switch ($this->formTypes[$formElement]["type"]) {
				case "text":
					echo '<div class="row form-row project-name">' . "\r\n";
					echo '<label class="new-proj-label">' . $label . ':</label>' . "\r\n";
					echo '<input type="text" value="' . $value . '" name="' . $formElement . '" placeholder="" class="form-control input-border new-proj-input required">' . "\r\n";
					echo '</div>'."\r\n";
					break;
				case "textarea":
					echo '<div class="row form-row project-name">';
                    echo '<label class="new-proj-label">' . $label . ':</label>';
                    echo '<textarea name="' . $formElement . '" placeholder="" required class="form-control input-border new-proj-input required">' . $value . '</textarea>';
                    echo '</div>';
					break;
				case "hidden":
					echo '<input type="hidden" value="' . $value . '" name="' . $formElement . '" class="form-control input-border new-proj-input required">' . "\r\n";
					break;
				default:
					// Do nothing
			}
		}
		return ob_get_clean();
	}
	
	
	public function tableElements($perPage = 20, $curPage = 1) {
		$displayer = new DB_Displayer();
		$displayer->setFormat($this);
		ob_start();
		echo $displayer->table($this->getAll());
		return ob_get_clean();
	}
	
}

class SampleModel extends DB_Schema {
	
	public function __construct() {
		parent::__construct();
		$this->table = "clients";
		$this->primaryKey = "id";
		$this->columns = ["id", "first", "last", "street", "city", "state", "zip", "phone1", "email"];
		$this->init();
		$this->DB->constrain("account_id", $_SESSION['account_id']);
	}
	
}