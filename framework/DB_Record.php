<?php
/************************************************
 *
 *  Derek's Generic DB_Record
 *
 *  This class can represent any mysql table.
 *
 *  When constructed, pass it an array of mysql column names and their values:
 *
 *  $myRecord = new DB_Record(["id" => 20, "projectname" => "New Project", "status" => "Open"]);
 *
 *  When constructed, the attributes you send are converted to public properties of the class and can then
 *  be accessed by their name like so:
 *
 *  $myRecord->id
 *  $myRecord->projectname
 *  $myRecord->status
 *
 *  If you update the record properties:
 *
 *  $myRecord->status = "Closed";
 *
 *  Then you can pass the record to a DB object to have it auto update the record in the database:
 *
 *  DB::table('tickets')->update($myRecord);
 *
 *  The update method will use the helper function synchronize to match the public properties with the database
 *  column names again.
 *
 *
 */
class DB_Record {

    // $exists
    //  - If this record was loaded from a live db, this will be set to true
    public $exists = false;
    
    // $attributes
    //  - The column names and values of this record
    public $attributes = [];
    
    // $primaryKey
    //  - The column we are calling the primary
    public $primaryKey = "id";

    /**
     *
     *  construct
     *
     *  converts attributes array to properties
     *
     */
    public function __construct($attributes = [], $existing = false) {
        $this->exists = $existing;
        foreach ($attributes as $key => $val) {
            $this->append($key, $val);
        }
    }
    
    /**
     *
     *  synchronize
     *
     *  converts properties back to attributes array, updating values
     *
     */
    public function synchronize() {
        foreach ($this->attributes as $key => $val) {
            $this->attributes[$key] = $this->$key;
        }
    }
    
    
    
    /**
     *
     *  append
     *
     *  adds a field to the record
     *
     */
    public function append($field, $value) {
        $this->attributes[$field] = $value;
        $this->$field = $value;
    }
}




?>
