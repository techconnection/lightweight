<?php 

/**
 * Language Library Class
 * 
 * This language library will be loaded in the application controller. So you 
 * can always use the language library in your controller classes.
 * @example $this->language->get('filename/key');
 */
class Language
{
    /**
     * Return language keys
     * 
     * The language library includes the language files and returns keys from them.
     * The parameter passed to the get method should be a string with the filename 
     * and key seperated by a forward slash.
     * @example $this->language->get('home/key');
     * 
     * @param string $file_and_key
     * @return string
     */
    public function get($path)
    {
        // Make file and key an array.
        $key_path = explode('/', $path);

        // If the file value of the key path array is set the file will be that value.
        $file = isset($key_path[0]) ? $key_path[0] : 'home';

        // If the key value of the key path array is set the key will be that value.
        $key = isset($key_path[1]) ? $key_path[1] : '';

        // Verify the language is found and is a file.
        if (is_file(LANGUAGE_DIR . '/english/' . $file . '.php')) {
            $file = $file;
        } else {
            $file = 'home';
        }

        // Include language file.
        include LANGUAGE_DIR . '/english/' . $file . '.php';

        // Return key or error.
        if (isset($_[$key])) {
            return $_[$key];
        } else {
            return 'Error! Language key ' . $key_path[1] . ' not found.';
        }
    }
}