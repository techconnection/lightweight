<?php

/***************************************************************
 *
 *
 *  Factory.php
 *
 *
 *  As this application becomes more complex, objects and classes will be more and more abstract with more
 *  types of incoming constructors, more parameters and so on.  The factory classes are designed to factor
 *  away repetitive setup code and generate objects we need, set up their parameters in advance and then
 *  simply return the object.  Most of the functions will be static.
 *
 *
 *
 *
 ***************************************************************/

class App {
    
    //this factory function builds a DB object that already has the contraint of account_id = the account of whoever logged in.
    //this way the methods and functions we call later will not somehow cross into an account that they aren't a member of.
    public static function table($table) {
        $db = DB::table($table)->constrain("account_id",$_SESSION['account_id']);
        return $db;
    }
    
    public static function people() {
        $db = DB::table("people")->constrain("account_id",$_SESSION['account_id']);
        return $db;
    }
    
    public static function places() {
        $db = DB::table("places")->constrain("account_id",$_SESSION['account_id']);
        return $db;
    }
    
    public static function resources() {
        $db = DB::table("resources")->constrain("account_id",$_SESSION['account_id']);
        return $db;
    }
    
    public static function resource_assignment() {
        $db = DB::table("resource_assignments")->constrain("account_id",$_SESSION['account_id']);
        return $db;
    }
    
    public static function documents() {
        $db = DB::table("documents")->constrain("account_id",$_SESSION['account_id']);
        return $db;
    }
    
    public static function clients() {
        $db = DB::table("clients")->constrain("account_id",$_SESSION['account_id']);
        return $db;
    }
    
    public static function tickets() {
        $db = DB::table("tickets")->constrain("tickets.account_id",$_SESSION['account_id']);
        return $db;
    }
    
    public static function users() {
        $db = DB::table("users")->constrain("account_id",$_SESSION['account_id']);
        $db->primaryKey = "user_id";
        return $db;
    }
}

/**************************************************************
 *
 *  handy echo functions
 *
 * usage: if you wish to print a var but only if it contains data.  This way no white space or wasted labels.
 *
 * Ex: $var = "Mr. Smith"
 *
 * echoif($var,"Dear ",", thanks for visiting.")
 *
 * Output = Dear Mr. Smith, thanks for visiting.
 *
 * If $var is null or not set, output is blank.
 *
 */

 function echoif($var,$pre='',$post=''){
    if(isset($var)){
        if(!is_null($var)) {
            if($var != '') {
                echo $pre.$var.$post;
            }
        }
    }
 }
 
 /*************************************************************
  *
  * echoelse
  *
  * usage: if you wish to print a var, but if null print an alternate value. Made to reduce inline if-else blocks.
  *
  * Ex: $var1 = "Mr. Smith"
  * Ex: $var2 = "Recipient"
  *
  * echoelse($var1,$var2, "Dear ",", thanks for visiting.")
  *
  * Output = Dear Mr. Smith, thanks for visiting.
  *
  * If $var is null or not set, output is:
  *
  * Output = Dear Recipient, thanks for visiting.
  *
  **/
 function echoelse($var,$var2,$pre='',$post='') {
    if(isset($var)){
        if(!is_null($var)) {
            if($var != '') {
                echo $pre.$var.$post;
                return;
            }
        }
    }
    echo $pre.$var2.$post;
 }
?>