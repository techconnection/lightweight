<?php

/**
 * Sessions Library Class
 *
 * The session library logs user data based on the session user id and 
 * handles session related data.
 */ 
class Sessions
{
    /**
     * Users id number
     * @var int
     */
    public $id;

    /**
     * Users group number
     * @var int
     */
    public $group;

    /**
     * Users firstname
     * @var string
     */
    public $firstname;

    /**
     * Users lastname
     * @var string
     */
    public $lastname;

    /**
     * Users username
     * @var string
     */
    public $username;

    /**
     * Users unique key
     * @var string
     */
    public $key;

    /**
     * Users email
     * @var string
     */
    public $email;

    /**
     * Users website
     * @var string
     */
    public $website;

    /**
     * Users birthday
     * @var string
     */
    public $birthday;

    /**
     * Date and time the user registered with the site
     * @var string
     */
    public $registered;

    /**
     * Users avatar
     * @var string
     */
    public $avatar;

    /**
     * Log a user session.
     * 
     * Check if the user id session is set. If the session is set access
     * the user model user the session user id and get the user data 
     * related to the session.
     * 
     * @return array
     */
    public function isLogged()
    {
        // Check if the session is set.
        if (isset($_SESSION['user_id'])) { 

            // Require and instantiate the user model class.
            require_once MODELS_DIR . '/user.php';
            $user_model = new UserModel();

            // Get everything from the users table where the user id is the same
            // as the session user id.
            $user_data = $user_model->getUser('user_id', $_SESSION['user_id']);

            // Create an array of the users data that was retrieved from the database.
            $this->id = $user_data->user_id;
            $this->group = $user_data->user_group;
            $this->firstname = $user_data->firstname;
            $this->lastname = $user_data->lastname; 
            $this->username = $user_data->username; 
            $this->key = $user_data->user_key; 
            $this->email = $user_data->email;
            $this->website = $user_data->website; 
            $this->birthday = $user_data->birthday;
            //$this->registered = $user_data->date_registered; 
            $this->avatar = $user_data->avatar; 
        } 

        // Set the users last activity to right now.
        // $this->updateLastActivity();

        // Return the logged user data array.
        if (isset($this->id)) {
            return $this->id;
        } else {
            return null;
        }
    }

    /**
     * Create a session.
     * 
     * @param string $session_name 
     * @param mixed $session_value 
     */
    public function createSession($session_name, $session_value)
    {
        if (!isset($_SESSION[$session_name])) {
            $_SESSION[$session_name] = $session_value;
        }
    }

    /**
     * Create a cookie.
     * 
     * @param string $name
     * @param mixed $value
     * @param int $expire 
     */
    public function createCookie($name, $value, $expire)
    {
        setcookie($name, $value, $expire, '/');
    }

    /**
     * Get the inactivity limit from settings.
     * 
     * @return string
     */
    public function getInactivityLimit()
    {
        // Require and instantiate the settings model class.
        require_once MODELS_DIR . '/settings.php';
        $settings_model = new SettingsModel();

        // Get the inactivity limit setting then get the value of it and return the value.
        $inactivity_setting = $settings_model->getSetting('name', 'inactivity_limit');
        $inactivity_limit = $inactivity_setting->value;
        return $inactivity_limit;
    }

    /**
     * Update the users last activity.
     */
    public function updateLastActivity()
    {
        $con = new Database();
        $time = time();
        $insert = $con->mysqli->prepare('UPDATE `users` SET `last_active` = ? WHERE `id` = ?');
        $insert->bind_param('ss', $time, $_SESSION['id']);
        $insert->execute();
    }
}