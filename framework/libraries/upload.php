<?php 

/**
 * Upload Library Class
 *
 * This class is will be used to validate and upload files to the server.
 */
class Upload
{
    /**
     * Name of the file to upload.
     * @var string
     */
    public $filename;

    /**
     * Temporary name of the file to upload.
     * @var string
     */
    public $filetmp;

    /**
     * Type of the file to upload.
     * @var string
     */
    public $filetype;

    /**
     * Size of the file to upload.
     * @var int
     */
    public $filesize;

    /**
     * Error if the selected file is invalid.
     * @var string
     */
    public $file_error;

    /**
     * Array of the filename to upload.
     * @var array
     */
    public $tmp;

    /**
     * Extension of the file to upload.
     * @var string
     */
    public $fileext;

    /**
     * Alert if the file to upload has an invalid extension.
     * @var boolean
     */
    public $file_invalid = null;

    /**
     * Alert if the file to upload is too large.
     * @var boolean
     */
    public $filebig = null;

    /**
     * Alert if the upload is successful or not.
     * @var boolean
     */
    public $upload_success = null;

    public function uploadImage($source, $output_dir)
    {
        $extensions = ['jpg', 'jpeg', 'png', 'gif'];

        $this->process($source);

        if (in_array($this->fileext, $extensions) === false) {
            $this->file_invalid = true;
        } 
        if ($this->filesize > 1048576) {
            $this->filebig = true;
        }

        if ($this->move($output_dir)) {
            $this->upload_success = true;
        }
    }

    public function uploadDoc($source, $output_dir)
    {
        $extensions = ['txt', 'pdf', 'doc', 'docx'];

        $this->process($source);

        if (in_array($this->fileext, $extensions) === false) {
            $this->file_invalid = true;
        } 
        if ($this->filesize > 1048576) {
            $this->filebig = true;
        }
        
        if ($this->move($output_dir)) {
            $this->upload_success = true;
        }
    }

    public function process($source)
    {
        $this->filename = trim(strtolower(str_replace(' ', '_', $source['name'])));          
        $this->filename = mt_rand(100000, 999999) . '_' . $this->filename;
        $this->filename = preg_replace('#[^a-z0-9._]#i', '', $this->filename);
        $this->filetmp = $source['tmp_name'];
        $this->filetype = $source['type'];
        $this->filesize = $source['size'];
        $this->file_error = $source['error'];
        $this->tmp = explode('.', $this->filename);
        $this->fileext = strtolower(end($this->tmp));
    }

    public function move($output_dir)
    {
        if (move_uploaded_file($this->filetmp, ROOT_DIR . $output_dir . $this->filename)) {
            return true;
        } else {
            return false;
        }
    }
}