<?php 

/**
 * Image Library Class
 *
 * This library is used to process images that have been uploaded.
 */
class Image
{   
    /**
     * Alert if the image has been cropped successfuly.
     * @var null
     */
    public $image_cropped = null;

    public function cropImage($source = false, $width = 60, $height = 60, $filename = false, $output_dir = false)
    {
        if ($source === false) {
            exit('No source file parameter provided.');
        }

        if ($filename === false) {
            exit('No filename parameter provided.');
        }

        if ($output_dir === false) {
            exit('No output directory provided.');
        }

        $file_type = mime_content_type($source);

        if ($file_type == 'image/jpg' || $file_type == 'image/jpeg') {
            $new_img = imagecreatefromjpeg($source);
        }
        if ($file_type == 'image/png') {
            $new_img = imagecreatefrompng($source);
        }
        if ($file_type == 'image/gif') {
            $new_img = imagecreatefromgif($source);
        }

        list($w, $h) = getimagesize($source);

        if ($w > $h) {
            $new_height = $height;
            $new_width = floor($w * ($new_height / $h));
            $crop_x = ceil(($w - $h) / 2);
            $crop_y = 0;
        } else {
            $new_width = $width;
            $new_height = floor( $h * ( $new_width / $w ));
            $crop_x = 0;
            $crop_y = ceil(($h - $w) / 2);
        }

        $quality = 100;
        $tmp_img = imagecreatetruecolor($width, $height);

        imagecopyresampled($tmp_img, $new_img, 0, 0, $crop_x, $crop_y, $new_width, $new_height, $w, $h);

        header('Content-Type: image/jpeg');
        if (imagejpeg($tmp_img, ROOT_DIR . $output_dir . '/' . $filename, $quality)) {
            imagedestroy($tmp_img);
            $this->image_cropped = true;
        }          
    }
}