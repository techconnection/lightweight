<?php 

/*
|--------------------------------------------------------------------------
| Mail Library
|--------------------------------------------------------------------------
| Mail library requires the phpmailer library and is used to send emails
| from the site.
*/

class Mail
{
    /**
    * Set to TRUE if the mail is sent.
    * @var bool
    */
    public $send_success = NULL;

    /**
    * Set to FALSE if the mail is not sent.
    * @var bool
    */
    public $send_fail = NULL;

    public function sendMail($send_email, $reply_email, $subject, $body, $message)
    {
        require_once PLUGINS_DIR . '/phpmailer/PHPMailerAutoload.php';

        $mail = new PHPMailer();
        // $mail->SMTPDebug = 2;                                                           // Enable verbose debug output
        $mail->isSMTP();                                                                   // Set mailer to use SMTP
        $mail->Host = 'smtp.office365.com';                                                // Specify main and backup SMTP servers
        $mail->SMTPAuth = TRUE;                                                            // Enable SMTP authentication
        $mail->Username = 'tickets@yourtechconnection.com';                                  // SMTP username
        $mail->Password = '3Yf9gD48jUaZXMb';                                                      // SMTP password
        $mail->SMTPSecure = 'tls';                                                         // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                                                 // TCP port to connect to
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            )
        );
        $mail->SetFrom('tickets@yourtechconnection.com', SITENAME);
        // $mail->AddAddress($send_email);                                                 // Add a recipient
        foreach ($send_email as $email) { 
            $mail->AddAddress($email); 
        }
        $mail->addReplyTo($reply_email);
        $mail->isHTML(TRUE);                                                               // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->AltBody = $message;
        if ($mail->send()) {
            $this->send_success = TRUE;
        } else {
            $this->send_fail = TRUE;
        }
    }

    public function getTemplate($file)
    {
        $file = APPLICATION_DIR . '/storage/templates/email/' . $file . '.txt';
        ob_start();
        require($file);
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public function preventSpam($message)
    {
        $keywords = ['http', '://', 'www', '.com', '.org', '.biz'];

        foreach ($keywords as $key) {
            if (strpos($message, $key)) {
                exit('You are ban for spamming.');
            }
        }
    }  
}