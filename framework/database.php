<?php 

/*
|--------------------------------------------------------------------------
| Database
|--------------------------------------------------------------------------
| The Database class connects to the database and exits with an error if 
| it cannot.
*/

final class Database {

    /**
    * Access the database object.
    * @var object
    */
    public $mysqli;

    private function __construct() {
        //$this->mysqli = new mysqli(HOSTNAME, USERNAME, PASSWORD, DBNAME, 3306, 'localhost:/Applications/MAMP/tmp/mysql/mysql.sock');
        $this->mysqli = new mysqli(HOSTNAME, USERNAME, PASSWORD, DBNAME);
        if ($this->mysqli->connect_error) {
            exit('Cannot connect to database.<br>' .  $this->mysqli->connect_error);
        }
    }

    public static function GetSharedDatabase() {
        static $inst = null;
        if ($inst === null) {
            $inst = new Database();
        }
        return $inst;
    }
    
    public function query($this_query) {
        $this_result = mysqli_query($this->mysqli, $this_query);
        if(mysqli_errno($this->mysqli)) {
            echo "PHP Could not run the query: " . $this_query;
            echo "Reason: " . mysqli_error($this->mysqli);
            //die;
        }
        return $this_result;
    }
    
    public function getRecords($table, $conditions = null) {
        $s = "select * from " . $table;
        if($conditions != null) {
            $s = $s." where " . $conditions;
        }
        $q = $this->query($s);
        $x = 0;
        $this_array = array();
        while ($row = mysqli_fetch_assoc($q)) {
                $this_array[$x] = $row;
                $x++;
        }
        return $this_array;
    }
}