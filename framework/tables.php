<?php

//these rough-in functions will modify the local database if needed columns aren't present,
function addColumn($name, $type, $table) 
{
    $db = Database::GetSharedDatabase();
    $s = "ALTER TABLE " . $table . " ADD " . $name . " " . $type;
    $result = $db->query($s);
    echo "One-time alter of " . $table . " to add column :" . $name . "<br>";
}

function tableExists($name) 
{
    $s = "SHOW TABLES LIKE '" . $name . "'";
    $db = Database::GetSharedDatabase();
    $result = $db->query($s);
    while ($row = mysqli_fetch_assoc($result)) {
        return true;
    }
    return false;
}

function confirmTable($name) 
{
    if (!tableExists($name)) {
        $s = "CREATE TABLE " . $name . " (id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY (id))";
        $db = Database::GetSharedDatabase();
        $result = $db->query($s);
        echo "One time creation of table: ".$name . "<br />";
    }
}

function columnExists($name, $table) 
{
    $name = strtolower($name);
    $db = Database::GetSharedDatabase();
    $result = $db->query("select * from " . $table);
    $fields = $result->fetch_fields();
    foreach ($fields as $field) {
        if (strtolower($field->name) == $name) {
            return true;
        }
    }
    return false;
}

function confirmColumn($name, $type, $table, $value = '') 
{
    if (!columnExists($name, $table)) {
        addColumn($name, $type, $table);
        if ($value != '') {
            $db = DB::table($table);
            $db->where(1,1)->massUpdate([$name => $value]);
        }
    }
}

function addClientTable() {
    $db = Database::GetSharedDatabase();
    $query = "CREATE TABLE IF NOT EXISTS `clients` (
        `account_id` int(10) unsigned NOT NULL DEFAULT '0',
        `id` int(20) NOT NULL AUTO_INCREMENT,
        `company` text NOT NULL,
        `first` varchar(100) NOT NULL,
        `last` varchar(100) NOT NULL,
        `street` varchar(100) NOT NULL,
        `city` varchar(100) NOT NULL,
        `state` varchar(100) NOT NULL,
        `zip` varchar(100) NOT NULL,
        `phone1` varchar(15) NOT NULL,
        `phone2` varchar(15) NOT NULL,
        `email` varchar(255) NOT NULL,
        `nickname` text,
        `portal_enabled` int(1) DEFAULT NULL,
        `token` varchar(64) DEFAULT NULL,
        PRIMARY KEY (`account_id`,`id`)
    ) ENGINE = MyISAM  DEFAULT CHARSET = utf8 AUTO_INCREMENT = 528 ;";

    $result = $db->query($query);
    echo 'One-time add of table<br>';
    echo $query;
}

function addResetTable() {
    $db = Database::GetSharedDatabase();
    $query = 'CREATE TABLE `reset_links` (
        `id` int(11) NOT NULL auto_increment PRIMARY KEY,
        `email` varchar(100) CHARACTER SET utf8 NOT NULL,
        `token` varchar(100) CHARACTER SET utf8 NOT NULL,
        `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
    )';

    $result = $db->query($query);
    echo 'One-time add of table<br>';
    echo $query;
}

function addPotentialsTable() {
    $db = Database::GetSharedDatabase();
    $query = 'CREATE TABLE `potentials` (
        `id` int(11) NOT NULL auto_increment PRIMARY KEY,
        `company` varchar(255) CHARACTER SET utf8 NOT NULL,
        `address` varchar(255) CHARACTER SET utf8 NOT NULL,
        `phone` varchar(255) CHARACTER SET utf8 NOT NULL,
        `username` varchar(255) CHARACTER SET utf8 NOT NULL,
        `email` varchar(255) CHARACTER SET utf8 NOT NULL,
        `password` varchar(255) CHARACTER SET utf8 NOT NULL,
        `user_key` varchar(255) CHARACTER SET utf8 NOT NULL,
        `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
    )';

    $result = $db->query($query);
    echo 'One-time add of table<br>';
    echo $query;
}

function addSettingsTable() {
    $db = Database::GetSharedDatabase();

    $query = 'CREATE TABLE `settings` (
        `id` int(11) NOT NULL auto_increment PRIMARY KEY,
        `name` varchar(56) CHARACTER SET utf8 DEFAULT NULL,
        `value` int(11) DEFAULT NULL
    )';

    $result = $db->query($query);

    $query = 'INSERT INTO `settings` (`id`, `name`, `value`) VALUES (NULL, "strong_pw", 0)';
    $result = $db->query($query);
    $query = 'INSERT INTO `settings` (`id`, `name`, `value`) VALUES (NULL, "verify_logout", 1)';
    $result = $db->query($query);
    $query = 'INSERT INTO `settings` (`id`, `name`, `value`) VALUES (NULL, "inactivity_limit", 5)';
    $result = $db->query($query);

    echo 'One-time add of table<br>';
    echo $query;
}

function addUserTable()
{
    $db = Database::GetSharedDatabase();
    $query = 'CREATE TABLE `users` (
        `user_id` int(11) NOT NULL auto_increment PRIMARY KEY,
        `account_id` int(11) DEFAULT NULL,
        `parent_id` int(11) DEFAULT NULL,
        `user_group` boolean NOT NULL DEFAULT 0,
        `user_key` varchar(60) CHARACTER SET utf8 NOT NULL,
        `company` varchar(100) CHARACTER SET utf8 NOT NULL,
        `address` text CHARACTER SET utf8 NOT NULL,
        `phone` varchar(60) CHARACTER SET utf8 NOT NULL,
        `firstname` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
        `lastname` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
        `username` varchar(20) CHARACTER SET utf8 NOT NULL,
        `email` varchar(56) CHARACTER SET utf8 NOT NULL,
        `website` varchar(56) CHARACTER SET utf8 DEFAULT NULL,
        `birthday` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
        `country` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
        `privacy` boolean NOT NULL DEFAULT 0,
        `password` varchar(60) CHARACTER SET utf8 NOT NULL,
        `avatar` varchar(100) CHARACTER SET utf8 DEFAULT "default_avatar.png" NOT NULL,
        `theme` varchar(11) CHARACTER SET utf8 DEFAULT "default",
        `date_registered` timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
        `last_activity` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
        `ip` varchar(56) CHARACTER SET utf8 NOT NULL,
        `locked_time` varchar(56) CHARACTER SET utf8 DEFAULT NULL,
        `login_attempts` int(2) NOT NULL DEFAULT 0
    )';

    $result = $db->query($query);
    echo "One-time add of table<br />";
    echo $query;
}

function addTicketTable() {
    $db = Database::GetSharedDatabase();
    $query = "CREATE TABLE IF NOT EXISTS `tickets` (
        `account_id` int(10) unsigned NOT NULL DEFAULT '0',
        `id` int(20) NOT NULL AUTO_INCREMENT,
        PRIMARY KEY (`account_id`,`id`)
    ) ENGINE = MyISAM  DEFAULT CHARSET = utf8 AUTO_INCREMENT = 528 ;";
    $result = $db->query($query);
    echo "One-time add of table<br />";
    echo $query;
}

function confirmClients() {
    if (!tableExists("clients")) {
        addClientTable();
    }
}

function confirmUsers() 
{
    if (!tableExists('users')) {
        addUserTable();
    }
}

function confirmSettings() {
    if (!tableExists("settings")) {
        addSettingsTable();
    }
}

function confirmResetLinks() 
{
    if (!tableExists('reset_links')) {
        addResetTable();
    }
}

function confirmPotentials() {
    if (!tableExists("potentials")) {
        addPotentialsTable();
    }
}
function confirmTickets() {
    if (!tableExists("tickets")) {
        addTicketTable();
    }
}

function versionCheck() {
    confirmClients();
    confirmUsers();
    confirmTickets();
    confirmPotentials();
    confirmResetLinks();
    confirmSettings();
    confirmColumn('account_id', 'int(11) null', 'users', '1');

    confirmColumn('company', 'varchar(255) null', 'users', '1');
    confirmColumn('address', 'varchar(255) null', 'users', '1');
    confirmColumn('phone', 'varchar(255) null', 'users', '1');
    confirmColumn('login_attempts', 'varchar(255) null', 'users', '1');
    
    confirmColumn('parent_id', 'int(11) null', 'tickets');
    confirmColumn('status', 'varchar(100) null', 'tickets');
    confirmColumn('type', 'varchar(255) null', 'tickets', 'project');
    confirmColumn('projectname', 'varchar(255) null', 'tickets');
    confirmColumn('scope', 'text null', 'tickets', 'Describe the project.');
    confirmColumn('startDate', 'varchar(255) null', 'tickets');
    confirmColumn('endDate', 'varchar(255) null', 'tickets');
    confirmColumn('assettag', 'varchar(255) null', 'tickets');
    confirmColumn('class', 'varchar(255) null', 'tickets');
    confirmColumn('state', 'varchar(255) null', 'tickets', "active");
    confirmColumn('label', 'varchar(255) null', 'tickets');
    confirmColumn('estimate', 'varchar(255) null', 'tickets');
    confirmColumn('po', 'varchar(255) null', 'tickets');
    confirmColumn('slack', 'varchar(255) null', 'tickets');
    confirmColumn('location', 'varchar(255) null', 'tickets');
    confirmColumn('clientid', 'int(11) null', 'tickets', '0');
    confirmColumn('account_id', 'int(11) null', 'tickets', '1');

    confirmTable('clients');
    
    confirmTable('documents');
    confirmColumn('projectid', 'int null', 'documents', '0');
    confirmColumn('nameondisk', 'varchar(255) null', 'documents');
    confirmColumn('documentname', 'varchar(255) null', 'documents');
    confirmColumn('account_id', 'int(11) null', 'documents', '1');
    confirmColumn('dateuploaded', 'datetime null DEFAULT CURRENT_TIMESTAMP;', 'documents', '1');
    
    confirmTable('master_accounts');
    confirmColumn('company', 'varchar(255) null', 'master_accounts');
    confirmColumn('token', 'varchar(255) null', 'master_accounts');
    confirmColumn('address', 'varchar(255) null', 'master_accounts');
    confirmColumn('primary_phone', 'varchar(255) null', 'master_accounts');
    confirmColumn('admin_user_id', 'int null', 'master_accounts');
    confirmColumn('admin_email', 'varchar(255) null', 'master_accounts');
    confirmColumn('logo', 'varchar(255) null', 'master_accounts');
    confirmColumn('license', 'varchar(255) null', 'master_accounts');
    confirmColumn('verified', 'varchar(255) null', 'master_accounts');
    confirmColumn('temp_password', 'varchar(255) null', 'master_accounts');
    if (DB::table('master_accounts')->where('id', 1)->getOne() == null) {
        $firstAccountRec = new DB_Record(['company' => 'TCI', 'admin_user_id' => 1, 'logo' => 'logo.png', 'verified' => 1]);
        DB::table('master_accounts')->insert($firstAccountRec);
        echo 'Default master account created.';
    }
    confirmTable('history');
    confirmColumn('date_added', 'DATETIME not null default CURRENT_TIMESTAMP', 'history');
    confirmColumn('username', 'varchar(255) null', 'history');
    confirmColumn('account_id', 'int(11) null', 'history');
    confirmColumn('userid', 'int(11) null', 'history');
    confirmColumn('ticketid', 'int(11) null', 'history');
    confirmColumn('clientid', 'int(11) null', 'history');
    confirmColumn('resourceid', 'int(11) null', 'history');
    confirmColumn('description', 'varchar(255) null', 'history');
    
    confirmTable('invites');
    confirmColumn('email', 'varchar(255) null', 'invites');
    confirmColumn('token', 'varchar(255) null', 'invites');
    confirmColumn('account_id', 'int(11) null', 'invites', '1');
    confirmColumn('date_added', 'DATETIME not null default CURRENT_TIMESTAMP', 'invites');

    confirmTable('potentials');
    confirmColumn('company', 'varchar(255) null', 'potentials');
    confirmColumn('address', 'varchar(255) null', 'potentials');
    confirmColumn('phone', 'varchar(255) null', 'potentials');
    confirmColumn('username', 'varchar(255) null', 'potentials');
    confirmColumn('email', 'varchar(255) null', 'potentials');
    confirmColumn('password', 'varchar(255) null', 'potentials');
    confirmColumn('user_key', 'varchar(255) null', 'potentials');

    confirmTable('resources');
    confirmColumn('type', 'varchar(255) null', 'resources');
    confirmColumn('name', 'varchar(255) null', 'resources');
    confirmColumn('status', 'varchar(255) null', 'resources', 'Active');
    confirmColumn('account_id', 'int(11) null', 'resources');
    confirmTable('resource_assignments');
    confirmColumn('resource_id', 'int(11) null', 'resource_assignments');
    confirmColumn('ticket_id', 'int(11) null', 'resource_assignments');
    confirmColumn('account_id', 'int(11) null', 'resource_assignments');
    
    confirmTable('people');
    confirmColumn('parent_id', 'int(11) null', 'people');
    confirmColumn('type', 'varchar(255) null', 'people');
    confirmColumn('first', 'varchar(255) null', 'people');
    confirmColumn('last', 'varchar(255) null', 'people');
    confirmColumn('status', 'varchar(255) null', 'people', 'Active');
    confirmColumn('work', 'varchar(255) null', 'people');
    confirmColumn('mobile', 'varchar(255) null', 'people');
    confirmColumn('email', 'varchar(255) null', 'people');
    confirmColumn('tags', 'varchar(255) null', 'people');
    confirmColumn('account_id', 'int(11) null', 'people');
    
    confirmTable('places');
    confirmColumn('parent_id', 'int(11) null', 'places');
    confirmColumn('type', 'varchar(255) null', 'places');
    confirmColumn('name', 'varchar(255) null', 'places');
    confirmColumn('street', 'varchar(255) null', 'places');
    confirmColumn('city', 'varchar(255) null', 'places');
    confirmColumn('state', 'varchar(255) null', 'places');
    confirmColumn('zip', 'varchar(255) null', 'places');
    confirmColumn('room', 'varchar(255) null', 'places');
    confirmColumn('tags', 'varchar(255) null', 'places');
    confirmColumn('status', 'varchar(255) null', 'places', 'Active');
    confirmColumn('class', 'varchar(255) null', 'places', 'Office');
    confirmColumn('account_id', 'int(11) null', 'places');

    confirmColumn('theme', 'varchar(11) not null default "default"', 'users');
    confirmColumn('website', 'varchar(100) null', 'users');
    confirmColumn('birthday', 'varchar(100) null', 'users');
}