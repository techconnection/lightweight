<?php

/***********************************************
 *
 *  Derek's generic DB class
 *  
 *  This class interfaces with mysql in a generic fashion, in combination with DB_Records.
 *  I got tired of making custom functions to update each kind of table I had made over the years
 *  and I came across the Query Builder concept when viewing Laravel code.  I couldn't see how
 *  Laravel saved any time since I am too old school and procedural oriented but the way it
 *  handles database concepts generically was interesting so I made a lightweight version of
 *  the concept.
 *  
 *  A static helper method is typical way to build it:
 *
 *  $db = DB::table('tableToUse');
 *
 *  We assume the primary key is named 'id', however you can change it to another value:
 *
 *  $db->primaryKey = "myId";
 *
 *  Once you make a DB object you can then get records from it like this:
 *
 *  $recordSet = $db->get();   //essentially this is SELECT * FROM 'tableToUse'
 *
 *  To make some more interesting db queries you might add WHERE clauses:
 *
 *  $records = $db->where("status","Open")->get();      //essentially this is SELECT * FROM 'tableToUse' WHERE 'status' = 'Open'
 *
 *  You might also select only a subset of interesting columns from the table:
 *
 *  $records = $db->select("id,name,status")->where("status","Open")->get();   //essentially this is SELECT id,name,status FROM 'tableToUse' WHERE 'status' = 'Open'
 *
 *  You can chain or repeat calls to where, as its effects are cumulative:
 *
 *  $records = $db->select("id, name")
 *                ->where("status","Open")
 *                ->where("isPaid","No")
 *                ->get();
 *  
 *  You can also do versions of it where you perform an OR or a NOT operation:
 *
 *  $records = $db->whereNot("status","Quote")
 *                ->orWhere("status","Estimate")
 *                ->get();     ////essentially this is SELECT * FROM 'tableToUse' WHERE 'status' != 'Quote' OR 'status' = 'Estimate'
 *
 *  If you want to try a little more complex operation you can add more constraints:
 *
 *  $records = $db->select("id, name")
 *                ->where("status","Open")
 *                ->orderBy("status")
 *                ->limit("5")
 *                ->get();
 *
 *  Additionally, it can be useful to add constraints to the database selection criteria that never change, so one might do:
 *
 *  $db->constrain("masteraccountnumber",10);
 *
 *  And then perform normal queries:
 *
 *  $records = $db->select("id, name")
 *                ->where("status","Open")
 *                ->orderBy("status")
 *                ->limit("5")
 *                ->get();
 *                
 *  The results of this call will only include items that match the constraints as well as the WHERE clauses.  This was added
 *  to the class for times when you don't want to specify the same WHERE clauses over and over again.  You can refactor them
 *  to a single constraint and then perform your unique selections without having to set them.
 *
 *  When searching for records, there are convenience methods available that perform variations on LIKE:
 *
 *  $singleRecord = $db->find(27);  //SELECT * FROM TABLE WHERE primaryKey = 27
 *
 *  $manyRecords = $db->findLike("status","Open");
 *
 *  $manyRecords = $db->beginsWith("name","John");
 *
 *  $manyRecords = $db->endsWith("email","@aol.com");
 *
 *  If you need to search for records on multiple columns, you'd do:
 *
 *  $records = $db->select("id,name,company,state,email,zip")
 *                ->whereLike("zip","90210")
 *                ->orWhereLike("state","Cali")
 *                ->get();
 *
 *
 *  The records you get back from these calls are in the form of DB_Records, which can
 *  represent any generic sort of record. The fields of the table become properties of
 *  the object.
 *
 *  $records = $db->get(); //this is like select * from table
 *
 *  foreach($records as $record) {
 *      echo $record->id;
 *      echo $record->projectname;
 *   }
 *
 *  With these records you can make changes like so:
 *
 *  $record->projectname = "This is a new name";
 *
 *  You can then have the DB save it back and update:
 *
 *  if($db->update($record)) {
 *      //it worked! 
 *  } else {
 *      //it didn't!
 *      echo "Update failed! Reason: ".$db->errno.", ".$db->error;
 *  }
 *
 *  The update function will look at the record you passed it and assume the primary key is
 *  named 'id' and use that for the update query.  However, if you want to make arbitrary
 *  changes to database records, you would use massUpdate:
 *
 *  $success = $db->where("status","Open")
 *                ->massUpdate(["status" =>"Closed"]);
 *
 *  The massUpdate function will take into account your WHERE commands and make numerous
 *  changes to database fields if any match up, but will be contrained by LIMIT clauses.
 *  Any constraints you've set in the class object will also apply.
 *  
 *
 *  You can insert new records as an array or singly if you have
 *  made DB records of your own:
 *
 *  $record1 = new DB_Record(["parent_id" => 0, "projectname" => "name1", "status" => "Open"]);
 *  $record2 = new DB_Record(["parent_id" => 0, "projectname" => "name2", "status" => "Open"]);
 *  $record3 = new DB_Record(["parent_id" => 0, "projectname" => "name3", "status" => "Open"]);
 *  
 *  $records = [ $record1, $record2, $record3 ];
 *  $db->insert($records); //returns true if they were inserted
 *
 *
 *  If you need to switch gears and delete items from the database, you can use the
 *  delete method:
 *
 *  $db->where("status","Deactivated")->delete();
 *
 *  The delete command can mass delete items if not used carefully:
 *
 *  $db->where("1",1)->delete(); //will clear the table completely
 *
 */

class DB {
    
    public $constraints = [];
    public $wheres = [];
    public $joins = [];
    public $orderby = "";
    public $limitby = "";
    public $selects = [];
    public $table = "model";
    public $query = "";
    public $primaryKey = "id";
    public $insert_id = 0;
    public $error = "";
    public $errno = 0;
    public $db;
    
    public function __construct() 
    {
        $this->db = Database::GetSharedDatabase();
        $this->selects[] = "*";
    }
    
    public static function table($table) 
    {
        $db = new DB();
        $db->table = $table;
        return $db;
    }
    
    public function reset()
    {
        $this->wheres = [];
        $this->joins = [];
        $this->selects = ["*"];
        $this->orderby = "";
        $this->limitby = "";
        $this->insert_id = 0;
        $this->errno = 0;
        $this->error = "";
    }
    
    public function orderBy($clause)
    {
        trim($clause);
        $this->orderby = $clause;
        return $this;
    }
    
    public function limit($clause)
    {
        trim($clause);
        $this->limitby = $clause;
        return $this;
    }
    
    public function unconstrain() 
    {
        $this->constraints = [];
        return $this;
    }
    
    public function constrain($where, $val, $operator = " = ",$conCat = " AND ", $quote="'")
    {
        trim($where);
        if ($where!='') {
            $this->constraints[] = ["column" => $where, "value" => $val, "operator" => $operator, "conCat" => $conCat, "quote" => $quote];
        }
        return $this;
    }
    
    public function join($where, $val, $val2, $operator = " = ", $kind = " JOIN ", $conCat = " AND ")
    {
        trim($where);
        if ($where!='') {
            $this->joins[] = ["join" => $kind, "table" => $where, "value1" => $val, "value2" => $val2, "operator" => $operator, "conCat" => $conCat];
        }
        return $this;
    }
    
    public function leftJoin($where, $val, $val2, $operator = " = ", $conCat = " AND ") 
    {
        return $this->join($where, $val, $val2, $operator, " LEFT JOIN  ", $conCat);
    }
    
    public function where($where, $val, $operator = " = ", $conCat = " AND ", $quote = "'") 
    {
        trim($where);
        if ($where != '') {
            $this->wheres[] = ["column" => $where, "value" => $val, "operator" => $operator, "conCat" => $conCat, "quote" => $quote];
        }
        return $this;
    }
    
    public function whereWithinLast24Hours($where, $ofDate = "NOW()") 
    {
        return $this->where($where,$ofDate." - INTERVAL 24 HOUR"," > ", " AND ","");
    }
    
    public function whereOlderThan24Hours($where, $ofDate = "NOW()") 
    {
        return $this->where($where,$ofDate." - INTERVAL 24 HOUR"," < ", " AND ","");
    }
    
    public function whereNot($where, $val, $conCat = " AND ") 
    {
        return $this->where($where, $val, " != ", $conCat);
    }
    
    public function orWhereNot($where, $val) 
    {
        return $this->where($where, $val, " != ", " OR ");
    }
    
    public function orWhere($where, $val, $operator = " = ") 
    {
        return $this->where($where, $val, $operator, " OR ");
    }
    
    public function whereLike($where, $val, $conCat = " AND ") 
    {
        return $this->where($where, '%'.$val.'%', " LIKE ", $conCat);
    }
    
    public function whereRLike($where, $val, $conCat = " AND ") 
    {
        return $this->where($where, '%'.$val.'%', " RLIKE ", $conCat);
    }
    
    public function orWhereLike($where, $val) 
    {
        return $this->whereLike($where, $val, " OR ");
    }
    
    public function select($select) 
    {
        $this->selects = [];
        if (is_string($select)) {
            $select = explode(',', $select);
        }
        foreach($select as $field) {
            trim($field);
            if($field!=''){
                $this->selects[] = $field;
            }
        }
        return $this;
    }
    
    public function buildOrderByString() 
    {
        if($this->orderby != "") {
            return " ORDER BY ".$this->orderby;
        }
        return "";
    }
    
    public function buildLimitString() 
    {
        if($this->limitby != "") {
            return " LIMIT ".$this->limitby;
        }
        return "";
    }
    
    /*********************************
     *
     *  Builds a string similar to
     *
     *  WHERE id=1
     *  or
     *  WHERE label="green" AND price<=60 OR stock!="Yes"
     *
     *  And so forth, all from the wheres array manipulated by the various where methods of this
     *  class.
     *
     */
    public function buildWhereString($placeHolders=false) {
        $s = "";
        $lastOp = "";
        $whereClauses = array_merge($this->wheres,$this->constraints);
        if(count($whereClauses)>0){
            $s = " WHERE ";
            foreach($whereClauses as $where) {
                if($placeHolders == true){
                    $val = "?";
                } else {
                    $val = $where["quote"].$where["value"].$where["quote"];
                }
                if($lastOp!="") {
                    $lastOp = $where['conCat'];
                    $s = $s.$lastOp.$where["column"].$where["operator"].$val;
                } else {
                    $lastOp = $where['conCat'];
                    $s = $s.$where["column"].$where["operator"].$val;
                }
            }
        }
        return $s;
    }
    
    
    /******************************
     *
     *  Builds a string similar to:
     *
     *  JOIN table ON val1 = val2
     *
     */
    public function buildJoinString($placeHolders=false) {
        $s = "";
        $lastOp = "";
        $whereClauses = $this->joins;
        if(count($whereClauses)>0){
            $s = " ";
            foreach($whereClauses as $where) {
                $s = $s.$where['join'].$where['table'];
                if($placeHolders == true){
                    $val = "?";
                    $val2 = "?";
                } else {
                    $val = $where["value1"];
                    $val2 = $where["value2"];
                }
                $s = $s." ON ".$val.$where["operator"].$val2;
            }
        }
        return $s;
    }
    
    
    public function buildQuerySelectString() {
        $s = "";
        if(count($this->selects)>0) {
            $s = "SELECT ".implode(",",$this->selects);
            $s = $s." FROM ".$this->table.$this->buildJoinString().$this->buildWhereString().$this->buildOrderByString().$this->buildLimitString();
        }
        $this->query = $s;
    }
    
    
    public function buildQueryUpdateString($colsAndVals) {
        $s = "UPDATE " . $this->table . " SET ";
        //append the WHERE clause to the query
        return $s.$this->buildPlaceholderString($colsAndVals).$this->buildWhereString().$this->buildLimitString();
    }
    
    /**
     *
     * buildPlaceholderString
     *
     * We are looking to create something like this:
     *
     * "name=?, status=?, phone1=?, email=?"
     * 
     * Essentially we are just glueing some array elements together with '=?,'
     *
     */
    public function buildPlaceholderString($attributes,$skipPrimary = true) {
        $s = "";
        //if we don't want the primary key to be added to the string, let's unset it
        if($skipPrimary==true & isset($attributes[$this->primaryKey])){
            unset($attributes[$this->primaryKey]);
        }
        foreach(array_keys($attributes) as $col) {
            $s = $s . $col . "=?, ";
        }
        return rtrim($s,', ');
    }
    
    
    /**
     *
     * bindAndExecute
     *
     * binding an array is tricky business with mysqli for some reason.  I guess they never thought
     * anyone would have an interest in doing so.
     *
     * To get around the limitation, we create an array that looks like:
     *
     * ["SSSISIS",&$var1,&var2,&var3"] and so on
     *
     * We then use call_user_func_array to call bind_param method on the supplied statement object and
     * then supply the array of variables.
     */
    public function bindAndExecute($statement,$values) {
        $types = "";
        $paramRefs = [];
        for($i = 0; $i < count($values); $i++) {
            if(is_numeric($values[$i])) {
                $types = $types."s";
            } else {
                $types = $types."s";
            }
            $paramRefs[] = &$values[$i];
        }
        array_unshift($paramRefs,$types);
        call_user_func_array(array($statement, "bind_param"), $paramRefs);
        if(!$statement->execute()) {
            $this->error = $statement->error;
            $this->errno = $statement->errno;
        } else {
            $this->insert_id = $statement->insert_id;
            $this->affected_rows = $statement->affected_rows;
            $this->error = $statement->error;
            $this->errno = $statement->errno;
        }
        $statement->close();
    }
    
    public function find($primaryKeyValue) 
    {
        $this->wheres = [];
        $this->where($this->primaryKey, $primaryKeyValue);
        return $this->getOne();
    }
    
    public function findLike($where,$val) {
        $this->where($where, '%'.$val.'%', " LIKE ");
        return $this->get();
    }
    
    public function beginsWith($where,$val) {
        $this->where($where, $val.'%', " LIKE ");
        return $this->get();
    }
    
    public function endsWith($where,$val) {
        $this->where($where, '%'.$val, " LIKE ");
        return $this->get();
    }
    
    public function get(){
        $records = [];
        $this->buildQuerySelectString();
        $db = $this->db;
        $results = $db->query($this->query);
        if($results){
            while ($row = $results->fetch_assoc()) {
                $newRec = new DB_Record($row,true);
                $newRec->primaryKey = $this->primaryKey;
                $records[] = $newRec;
            }
        } else {
            $this->error = $db->mysqli->error;
            $this->errno = $db->mysqli->errno;
        }
        return $records;
    }
    
    public function count() {
        $currentState = $this->selects;
        $this->selects = [];
        $this->selects[] = "COUNT(".$this->table.".".$this->primaryKey.")";
        $results = $this->get();
        $this->selects = $currentState;
        if(count($results)) {
            return $results[0]->attributes["COUNT(".$this->table.".".$this->primaryKey.")"];
        } else {
            return 0;
        }
    }
    
    public function getOne() 
    {
        $records = $this->get();
        if (count($records) > 0) {
            return $records[0];
        }
        return null;
    }
    
    public function getFirst() {
        return $this->getOne();
    }
    
    
    public function insert($inserts) {
        $success = false;
        $s = "";
        if(!is_array($inserts)) {
            $inserts = array($inserts);
        }
        if(count($inserts)>0) {
            $db = $this->db;
            foreach($inserts as $recordToInsert) {
                $recordToInsert->synchronize();
                if($recordToInsert->exists){
                    $success = $this->update($recordToInsert);
                } else {
                    $s = "INSERT INTO ".$this->table." (";//) VALUES ()";
                    $columns = implode(",",array_keys($recordToInsert->attributes));
                    $placeHolders = implode(',', array_fill(0, count($recordToInsert->attributes), '?'));
                    $s = $s.$columns.") VALUES (".$placeHolders.");";
                    if($statement = $db->mysqli->prepare($s)){
                        $this->bindAndExecute($statement,array_values($recordToInsert->attributes));
                        $recordToInsert->attributes[$this->primaryKey] = $this->insert_id;
                        $key = $this->primaryKey;
                        $recordToInsert->$key = $this->insert_id;
                        $recordToInsert->exists = true;
                        $success = $this->errno == 0 ? true : false;
                    } else {
                        $this->insert_id = 0;
                        $this->error = $db->mysqli->error;
                        $this->errno = $db->mysqli->errno;
                    }
                }
            }
        }
        return $success;
    }
    
    public function delete() 
    {
        $success = false;
        $s = 'DELETE FROM ' . $this->table;
        $s = $s . $this->buildWhereString(true);
        $db = $this->db;
        if ($statement = $db->mysqli->prepare($s)) {
            $whereClauses = array_merge($this->wheres, $this->constraints);
            $params = array_column($whereClauses, 'value');
            $this->bindAndExecute($statement, $params);
            $success = true;
        } else {
            $this->error = $db->mysqli->error;
            $this->errno = $db->mysqli->errno;
        }
        return $success;
    }
    
    public function massUpdate($colsAndVals) {
        $success = false;
        $s = "";
        //we want an array even if its just one item
        if(!is_array($colsAndVals)) {
            $colsAndVals = array($colsAndVals);
        }
        //make sure we got a valid array and go
        if(count($colsAndVals) > 0) {
            //now we begin the mysql query string creation
            $s = $this->buildQueryUpdateString($colsAndVals);
            $db = $this->db;
            if($statement = $db->mysqli->prepare($s)) {
                $this->bindAndExecute($statement, array_values($colsAndVals));
                $success = $this->errno == 0 ? true : false;
            } else {
                $this->error = $db->mysqli->error;
                $this->errno = $db->mysqli->errno;
                $success = false;
            }
        }
        return $success;
    } 
    
    
    public function update($updates) {
        $success = false;
        $s = "";
        //we want an array even if its just one item
        if(!is_array($updates)) {
            $updates = array($updates);
        }
        //make sure we got a valid array and go
        if(count($updates) > 0) {
            $db = $this->db;
            foreach($updates as $recordToUpdate) {
                //synchronize the properties of the db_record with the attributes array
                $recordToUpdate->synchronize();
                //important!  did they send us a record that doesn't exist in the db by accident?
                if(!$recordToUpdate->exists) {
                    //if they did, route this object to insert instead
                    $success = $this->insert($recordToUpdate);
                } else {
                    //we'll kill any WHERE clauses, we assume they sent us a record to update, not a mass update
                    $this->wheres = [];
                    //now we begin the mysql query string creation
                    $attributes = $recordToUpdate->attributes;
                    //we'll assume they want WHERE id=myRecord's id
                    $this->where($recordToUpdate->primaryKey, $attributes[$recordToUpdate->primaryKey]);
                    unset($attributes[$this->primaryKey]);
                    //append the WHERE clause to the query
                    $s = $this->buildQueryUpdateString($attributes);
                    if($statement = $db->mysqli->prepare($s)) {
                        $this->bindAndExecute($statement,array_values($attributes));
                        $success = $this->errno == 0 ? true : false;
                        echo $this->error;
                    } else {
                        $this->error = $db->mysqli->error;
                        $this->errno = $db->mysqli->errno;
                    }
                }
            }
        }
        return $success;
    }    
}