<?php 

/**
 * Application Router
 *
 * The router is called by the index.php in the application root. The router uses 
 * the application url to determine the controller, method, and  paramters. If no 
 * method is found then the method name in the url becomes a parameter.
 * @example http://website/controller/method/param1/param2
 * @example http://website/account/activate/username/key
 */
function router()
{
    // Start a session if one is not already started
    if (!isset($_SESSION)) { session_start(); }

    versionCheck();

    // Get the url, sanitize it and explode it by forward slashes
    if (isset($_GET['url'])) {
        $url = explode('/', filter_var(trim($_GET['url'], '/'), FILTER_SANITIZE_URL));
    }

    // Set the default controller and the default controller path.
    $controller = 'home.php';
    $controller_path = CONTROLLERS_DIR . '/common/home.php';

    // If the url array is set. If it is, use the first value as the controller file.
    // Using that controller file name recursively iterate through the controllers 
    // directory and set the path using that controller name.
    if (isset($url)) {
        if (isset($_SESSION['user_id'])) {
            // Set the controller name to the first value of the url array.
            $controller = $url[0] . '.php';
        } else {
            switch ($url[0]) {
                case 'test':
                    $controller = "test";
                    break;
                case 'signup':
                    $controller = 'signup';
                    break;
                case 'reset':
                    $controller = 'reset';
                    break;
                case 'invitation':
                    $controller = 'invitation';
                    break;
                case 'slack':
                    $controller = 'slack';
                    break;
                default:
                    $controller = 'login';
                    break;
            }
        }

        // Recursively iterate through the controller directory and create and 
        // array of controller file paths.
        $dir = new RecursiveDirectoryIterator(CONTROLLERS_DIR);
        foreach (new RecursiveIteratorIterator($dir) as $filename) {
            $files[] = $filename->__toString();
        }

        // Look through each file path and if the path contains the controller.
        // Set the controller path to the path that contains the controller string.
        foreach ($files as $file) {
            if (strpos($file, $controller)) {
                $controller_path = str_replace('\\', '/', $file);
            }
        }

        // Unset the first item in the url array.
        unset($url[0]);
    } else {
        header('Location:/login');
    }

    // Require the controller using the controller path.
    require_once $controller_path;

    // If the controller filename contains underscores, explode the filename and
    // use the end piece as the controller name.
    if (strpos($controller, '_')) {
        $controller = explode('_', $controller);
        $controller = end($controller);
    }

    // Remove the extension from the controller and set the first char to uppercase.
    $controller = str_replace('.php', '', ucfirst($controller . 'Controller'));

    // If the class exists instantiate the class. If it does not instantiate the 
    // home controller.
    if (class_exists($controller)) {
        $controller = new $controller;
    } else {
        $controller = new HomeController();
    }

    // Set the default method to be called if no method is set.
    $method = 'index';

    // Check if the first item in the url array is set.
    if (isset($url[1])) {
        $url_method = $url[1];
        // If the first item of the url array is set check it for underscores.
        if (strpos($url_method, '_') == true) {   
            // Remove underscores
            $url_method = str_replace('_', '', $url_method);
        }

        // Check if the method from the url exists.
        if (method_exists($controller, $url_method)) {
            $method = $url_method;
            unset($url[1]);
        }
    }

    // Create a params array of parameters using the pieces of the url array 
    // that come after the controller and method. Rebase the keys of the 
    // array so the param array keys start at 0.
    $params = [];
    if (isset($url)) {
        $params = $url ? array_values($url) : [];
    }

    // If the controllers method is callable call the controllers method 
    // with params.
    if (is_callable([$controller, $method])) {
        call_user_func_array([$controller, $method], $params);
    }
}