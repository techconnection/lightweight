<?php 

/*********************************************************************
 *
 *	Derek's Generic Display Class
 *
 *	Reason for existing:
 *
 *	Pass your array of DB_Records to this class and it will generate an HTML table that displays them.
 *
 *	Records like:
 *
 *	DB_Record {
*		id -> int
*		name -> string
*		address -> string
*		phone -> string
 *	}
 *
 *	Will output as follows:
 *
 *	<table>
 *	<tr><th>Id</th><th>Name</th><th>Address</th><th>Phone</th></tr>
 *	<tr><td>1</td><td>John</td><td>Main St.</td><td>1234</td></tr>
 *	</table>
 *
 *	Optional formatting classes can be attached that create the following changes:
 *
 *	<th>Label</th><th>Label2</th> which override the MySQL column names
 *
 *	A 'linkables' attribute that builds a link around a value, replacing %id% with the primary key of a record:
 *
 *	$linkFormat = "/projects/detail/%id%";
 *	$linkColumn = "name";
 *
 *	Results in:
 *	<td>1</td><td><a href="/projects/detail/1">John</a></td>....
 *
 *	Lastly, there is the format of 'type' for the data to be displayed, the options are:
 *	 hidden - do not tabulate this data, but include it in a hidden input field for a form
 *	 ignore - do not tabulate, do not show on a form
 *	 text - standard text box size
 *	 textarea - large text box size
 *
 *	Let's go over an example:
 *
 *	 $records = DB::table('tickets')->limit(10)->get();
 *	 $displayer = new DB_Displayer();
 *
 *	 echo $displayer->table($records);  //now we have a sweet table
 *
 *	Alternatively, if you made an html file already, you can do this:
 *	
 *	HTML file:
 *		<tr><td>%company%</td><td>%first% %last%</td></tr>
 *	And call:
 *		$displayer->template($record,$fileName);
 *		
 *	For repeating rows you can do:
 *	$displayer->template($records,$filename);
 *	
 *	This version will find look for a [loop] tag and then replace the contents with each record.
 *
 *	Format the HTML file like so for this usage:
 *
 *  <table>
 *	[loop]<tr><td>%company%</td><td>%first% %last%</td></tr>[end]
 *	</table>
 *
 *	The method doesn't care what you loop, for a list:
 *
 *	<ul>
 *	[loop]<li>%company% - %first% %last%</li>[end]
 *	</ul>
 *
 *	
 *	
 * 
 *********************************************************************/

class DB_Displayer 
{
	public $displayOrder = [];
	public $formTypes = [];
	public $linkFormats = [];
	public $linkables = [];
	
	public function __construct($formats = null) 
	{
		if ($formats != null) {
			$this->setFormat($formats);
		}
	}
	
	public function setFormat($formats) 
	{
		$this->displayOrder = $formats->displayOrder;
		$this->formTypes = $formats->formTypes;
		$this->linkFormats = $formats->linkFormats;
		$this->linkables = $formats->linkables;
	}
	
	
	public function form($activeRecord, $blank = false) 
	{
		$this->createDisplayOrder($activeRecord->attributes);
		var_dump($this->displayOrder);
		ob_start();
		foreach ($this->displayOrder as $formElement => $value){
			$label = $this->getColumnLabel($formElement);
			$value = "";
			if (!$blank) {
				$value = $activeRecord->attributes[$formElement];
			}
			switch ($this->getColumnType($formElement)) {
				case "text":
					echo '<div class="row form-row project-name">'."\r\n";
					echo '<label class="new-proj-label">'.$label.':</label>'."\r\n";
					echo '<input type="text" value="'.$value.'" name="'.$formElement.'" placeholder="" class="form-control new-proj-input required">'."\r\n";
					echo '</div>'."\r\n";
					break;
				case "textarea":
					echo '<div class="row form-row project-name">';
                    echo '<label class="new-proj-label">'.$label.':</label>';
                    echo '<textarea name="'.$formElement.'" placeholder="" required class="form-control new-proj-input required">'.$value.'</textarea>';
                    echo '</div>';
					break;
				case "hidden":
					echo '<input type="hidden" value="'.$value.'" name="'.$formElement.'" class="form-control new-proj-input required">'."\r\n";
					break;
				default:
					//do nothing
			}
		}
		return ob_get_clean();
	}
	
	
	public function getColumnType($formElement) 
	{
		return !isset($this->formTypes[$formElement]["type"]) ? "text" : $this->formTypes[$formElement]["type"];
	}
	
	public function getColumnLabel($formElement) 
	{
		return isset($this->formTypes[$formElement]["label"]) ? $this->formTypes[$formElement]["label"] : ucwords($formElement);
	}
	
	public function createDisplayOrder($columns) 
	{
		foreach ($columns as $formElement => $value) {
			$this->displayOrder[$formElement] = !isset($this->displayOrder[$formElement]) ? 1000 : $this->displayOrder[$formElement];
		}
		asort($this->displayOrder);
	}
	
	/* *******************
	 * 
	 * 	Will output as follows:
	 *
	 *	<table>
	 *	<tr><th>Id</th><th>Name</th><th>Address</th><th>Phone</th></tr>
	 *	<tr><td>1</td><td>John</td><td>Main St.</td><td>1234</td></tr>
	 *	</table>
	 *	
	 */
	public function table($recordsToDisplay, $file = "") 
	{
		//if they pass us a file we will base table around that, otherwise build dynamically.
		if ($file != "") {
			$this->tableUsingTemplateFile($recordsToDisplay, $file);
			return;
		}
		//without a template file we can't continue with a null array
		if ($recordsToDisplay == null) return;
		//let's begin by ordering the fields for display
		$this->createDisplayOrder($recordsToDisplay[0]->attributes);
		ob_start();
		echo "<table><tr>";
		foreach ($this->displayOrder as $formElement => $value){
			$label = $this->getColumnLabel($formElement); 
			$columnType = $this->getColumnType($formElement);
			switch ($columnType) {
				case "hidden":
				case "ignore":
					//do nothing
					break;
				default:
					echo "<th>".$label.'</th>';
					break;
			}
		}
		echo "</tr>";
		$allRecords = $recordsToDisplay;
		foreach ($allRecords as $activeRecord) {
			echo "<tr>";
			if ($activeRecord!=null) {
				foreach ($this->displayOrder as $formElement => $value) {
					$value = $activeRecord->attributes[$formElement];
					$columnType = $this->getColumnType($formElement);
					switch ($columnType) {
						case "text":
						case "textarea":
							echo "<td>";
							if (isset($this->linkables[$formElement])) {
								$link = $this->linkFormats[$formElement];
								foreach ($activeRecord->attributes as $find => $replace) {
									$link = str_replace('%'.$find.'%', $replace, $link);
								}
								echo "<a href=".$link.">";
							}
							echo $value;
							if (isset($this->linkables[$formElement])) {
								echo "</a>";
							}
							echo "</td>";
							break;
						default:
							//do nothing
					}
				}
			}
			echo "</tr>";
		}
		echo "</table>";
		return ob_get_clean();
	}
	
	protected function slice($file)
    {
        $file = VIEWS_DIR . '/htm/' . $file . '.htm';
        ob_start();
        require($file);
        return ob_get_clean();
    }
	
	public function import($file)
    {
		if (is_file(VIEWS_DIR . '/htm/' . $file)) {
			$file = VIEWS_DIR . '/htm/' . $file;
			ob_start();
			require($file);
			return ob_get_clean();
		} else {
			return "{Warning! include " . $file . " is missing!}";
		}
    }
	
	/***********************************************
	 *
	 *	expandIncludes
	 *
	 *	This function accepts incoming HTML string data and searches it for stuff like this:
	 *
	 *	{include somefile.htm}
	 *
	 *	It then goes and gets that file and inserts the contents into the HTML string, thus
	 *	expanding the string to replace the placeholders with actual file data.
	 *
	 *	A quick example for the usage intent:
	 *
	 *	<html>{include header.htm}<body>
	 *	<div=class="toolbar">
	 *	{include toolbar.htm}
	 *	</div>
	 *	</body>
	 *	</html>
	 *
	 *	The function does not process the incoming file recursively to look for more {include}
	 *	statements at this time, so such things will show in a literal format.
	 *
	 **********************************************/
	public function expandIncludes($html) 
	{
		$search = '/\{\s*include\s+([\-\/A-Za-z0-9.\_]+)\s*\}/i';
		preg_match_all($search, $html, $matches, PREG_SET_ORDER, 0);
		foreach ($matches as $script) {
			$filename = $script[1];
			$includeHTML = $this->import($filename);
			$html = str_replace($script[0], $includeHTML, $html);
		}
		return $html;
	}
	
	
	public function expandVariables($html, $variables= null) 
	{
		$search = '/\%\s*([\_A-Za-z0-9.]+)\s*\%/i';
		preg_match_all($search, $html, $matches, PREG_SET_ORDER, 0);
		foreach ($matches as $script) {
			$fullMatch = $script[0];
			$varname = $script[1];
			if ($variables != null) {
				$vars = $variables;
			} else {
				$vars = $this->variables;
			}
			if (isset($vars[$varname])) {
				$html = str_replace($fullMatch, $vars[$varname], $html);
			}
		}
		return $html;
	}
	
	/********************************************
	 *
	 *	Given a string section like so:
	 *
	 *	[if myvariable]
	 *	text if true
	 *	[else]
	 *	text if false
	 *	[endif]
	 *
	 *	This regex will capture 3 times, once for my variable, once for text if true and once for text if false.
	 *	However, the else statement and text is optional and not always present.
	 *
	 *
	 *******************************************/
	public function expandIfs($html,$vars=null) 
	{
		//return $html;
		$ifHTML = "";
		if($vars==null) {
			$vars = $this->variables;
		}
		else {
			if (is_object($vars)) {
				if (get_class($vars)==="DB_Record") {
					$vars = $vars->attributes;
				}
				else {
					return $html;
				}
			}
		}
		$re = '/\[\s*if\s*(.*?)\s*\](.*?)(?:\[\s*else\s*\](?:.*?)(.*?))*\[\s*endif\s*\]/is';
		preg_match_all($re, $html, $matches, PREG_SET_ORDER, 0);
		foreach ($matches as $script) {
			$fullMatch = $script[0];
			$varname = $script[1];
			$trueHTML = $script[2];
			if (isset($script[3])) {
				$falseHTML = $script[3];
			} else {
				$falseHTML = "";
			}
			if (isset($vars[$varname])) {
				if ($this->variables[$varname] != "" & $vars[$varname] != 0) {
					$ifHTML  = $trueHTML;
				} else {
					$ifHTML = $falseHTML;
				}
			} else {
				$ifHTML = $falseHTML;
			}
			$html = str_replace($fullMatch,$ifHTML,$html);
		}
		return $html;
	}
	
	
	public function expandLoops($html) 
	{
		$loopedHTML = "";
		$re = '/\[\s*for\s+(\w*)\s*](.*)\[\s*next\s*]/isU';
		preg_match_all($re, $html, $matches, PREG_SET_ORDER, 0);
		foreach ($matches as $script) {
			$fullMatch = $script[0];
			$varname = $script[1];
			$insertHTML = $script[2];
			if (isset($this->variables[$varname])) {
				$loopedHTML  = $this->placeholder_replace($this->variables[$varname],$insertHTML);
			} else {
				//$loopedHTML .= "";//"Warning! Variable $".$varname." is not available.";
			}
			$html = str_replace($fullMatch,$loopedHTML,$html);
		}
		return $html;
	}
	
	public function str_array_replace($array, $string, $debug = false) 
	{
		if (!is_array($array)) {
			return $string;
		}
		$thisRow = "";
		foreach ($array as $find => $replace) {
			if (is_object($replace)) {
				if (get_class($replace)==="DB_Record") {
					$thisRow .= $this->str_array_replace($replace->attributes,$string,$debug);
				} else {
					if ($debug) echo "Cannot array_replace ".get_class($replace)."";
				}
			} else {
				$thisRow = $this->expandVariables($string, $array);
				break;
			}
		}		
		return $thisRow;
	}
	
	public function placeholder_replace ($array, $string, $debug = false) 
	{
		if (is_object($array)) {
			if (get_class($array)==="DB_Record") {
				$string = $this->str_array_replace($array->attributes,$string,$debug);
			} else {
				//they sent neither an array nor a DB_Record, let's just exit
				return $string;
			}
		} else {
			$string = $this->str_array_replace($array,$string,$debug);
		}
		return $string;
	}
	
	
	/**********************************************************
	 *
	 *
	 *	This function accepts an array of variables and a file name.
	 *
	 *	If $file is null it assumes the first param was the file name.
	 *
	 *	The file is expected to contain various field names wrapped in
	 *	%, like %id%, and %projectname%, etc.
	 *
	 *	The loops contained herein will find and replace these with the
	 *	actual array values.
	 *
	 *	You can also make an entry of [for variable]...[next] and the
	 *	array replacement will be repeated.  Example:
	 *
	 *	<ul>[for projects]<li>%projectname%</li>[next]</ul>
	 *
	 *	This method will turn this into multiple <li>data</li> entries
	 *	to match the array objects given.
	 *
	 *
	 *
	 */
	public function template($record, $file=null, $html=null) {
		//if they sent only 1 param it is presumed to be a file name
		if($file==null & $html==null) {
			$file = $record;
			$record = null;
		}
		$this->variables = $record;

		//load the html
		if($file!=null){
			$html = $this->slice($file);
		}
		if($html==null) return;
		
		//do a pre-process of the template html and look for includes and loops
		//check for {include afilehere.htm} type patterns
		$html = $this->expandIncludes($html);
		//check for [for variable]repeat lines[next] type patterns
		$html = $this->expandLoops($html);
		//check for [if var]this[else]this instead[endif] patterns
		$html = $this->expandIfs($html);
		//check for all matching %varname% patterns
		$html = $this->expandVariables($html);
		
		//now that we finished, kill any forgotten/unmatched %fieldname% items:
		$html = preg_replace('~\%(.+?)\%~', "", $html);
		echo $html;
	}
	
}
