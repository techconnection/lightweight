<?php

/**
 * Application Controller
 *
 * The controller class is the main controller of the application system.
 * All controller classes will be extensions of this class.
 */
class Controller {
    
    /**
    * Array of alerts.
    * @var array
    */
    public $alert = [];

    /**
    * Array of data for use in the view.
    * @var array
    */
    public $viewParams;

    /**
    * Title of page.
    * @var string
    */
    public $title = "";

    /**
    * Access the search model.
    * @var object
    */
    public $search;
    
    /**
    * Access the user model.
    * @var object
    */
    public $user_model;

    /**
    * Access the project model.
    * @var object
    */
    public $project_model;

    /**
    * Access the client model.
    * @var object
    */
    public $client_model;

    /**
    * Access the language library.
    * @var object
    */
    public $language;

    /**
     * Access the sessions library.
     * @var object
     */
    protected $session;
    
    /**
     * Check if the browser is a mobile browser.
     *  @var boolean
     */
    
    public $isMobile = false;
    
    /**
     * Controller construct
     * 
     * Controller classes are extended form this class so everytime 
     * you load a controller class this construct will be called.
     */
    public function __construct() {
        //this var is passed to the view object later, let's init it with some basic info
        $this->viewParams = $this->GetSessionVariables();
        $this->viewParams['search'] = $this->slice('common/searchbar');
        $this->search_model = $this->model('search');
        $this->user_model = $this->model('user');
        $this->log_model = $this->model('log');
        $this->project_model = $this->model('project');
        $this->client_model = $this->model('client');
        $this->newsletter_model = $this->model('newsletter');
        $this->language = $this->library('language');
        $this->session = $this->library('sessions');
        $this->isMobile = $this->isMobileBrowser();
        
        /*if (isset($_SESSION['user_id'])) { 
            $loggedInUser = $this->user_model->find($_SESSION['user_id']);
            if($loggedInUser != null) {
                if (time() - $loggedInUser->last_activity > 15 * 60) {
                    
                }
                $this->user_model->update(['last_activity' => time(), "user_id" => $_SESSION['user_id']]);
            }
        }*/
    }
    
    function isMobileBrowser() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
    
    public function GetSessionVariables() {
        $data = [];
        if (isset($_SESSION['user_id'])) {
            @$data['logged_user_id'] = $_SESSION['user_id']; 
            @$data['logged_user_group'] = $_SESSION['user_group'];
            @$data['logged_account_id'] = $_SESSION['account_id'];
            @$data['logged_user'] = $_SESSION['username'];
        }
        return $data;
    }

    /**
     * Controller loader
     * 
     * Require and instantiate a controller class based on the parameter passed to the method.
     * The parameter should be the controllers parent dir and the controller file.
     * Expected param: account/signup
     * 
     * @param string $controller
     * @return object
     */
    public function controller($controller)
    {
        // Check if the controller is a file and/or if it exists. If not exit with notification.
        if (is_file(CONTROLLERS_DIR . '/' . $controller . '.php')) {
            // Require the controller file.
            require_once CONTROLLERS_DIR . '/' . $controller . '.php';

            // Explode the controller path so the path is an array.
            // Expected result: www/path_to_controller/controller
            $controller_path = explode('/', $controller);

            // Set the controller as the final piece of the exploded controller path array.
            $controller = end($controller_path);

            // If the controller filename contains underscores, explode the filename and
            // use the end piece as the controller name.
            if (strpos($controller, '_')) {
                $controller = explode('_', $controller);
                $controller = end($controller);
            }

            // Set the controller name and set the first char to be uppercase.
            // Expected result: AccountController
            $controller = ucfirst($controller . 'Controller');

            // If the index method of the controller is callable instantiate 
            // the controller and return the index method. If the method is
            // not callable exit with notification.
            if (is_callable([$controller, 'index'])) {
                $controller = new $controller();
                return $controller->index();
            } else {
                exit('The ' . $controller . ' class cannot be instantiated.');
            }
        } else {
            exit('The ' . $controller . ' controller does not exist.');
        }
    }
    
    public function model($model) {
        if (is_file(MODELS_DIR . '/' . $model . '.php')) {
            require_once MODELS_DIR . '/' . $model . '.php';
            $model = ucfirst($model . 'Model');
            return new $model();
        } else {
            exit('The model ' . $model . ' does not exist.');
        }
    }
    
    public function view($view) {
        if (is_file(VIEWS_DIR . '/htm/' . $view . '.htm')) {
            $displayer = new DB_Displayer();
            $displayer->template($this->viewParams, $view);
        } else {
            exit('The page ' . $view . '.htm does not exist.');
        }
    }
    
    //this function appends all of the attribute array keys and values to viewParams
    //when view or page is called, all of the record fields will become variables.
    public function extractRecord(DB_record $record) {
        foreach ($record->attributes as $key => $val) {
            $this->viewParams[$key] = $record->$key;
        }
    }
    
    function popup($name) {
        //$this->view('head');
        $this->view($name);
    }
    
    public function page($name) {
        $this->view($name);
    }
    
    public function redirect($where) {
        $this->view("common/head");
        echo '<script>window.location.assign("'.$where.'");</script>';
    }

    public function helper($helper) {
        if (is_file(SYSTEM_DIR . '/helpers/' . $helper . '.php')) {
            require_once SYSTEM_DIR . '/helpers/' . $helper . '.php';
        } else {
            exit('The ' . $helper . ' helper does not exist.');
        }
    }

    protected function library($library)
    {
        if (is_file(SYSTEM_DIR . '/libraries/' . $library . '.php')) 
        {
            require_once SYSTEM_DIR . '/libraries/' . $library . '.php';
            return new $library();
        } else {
            exit('The library ' . $library . ' does not exist.');
        }
    }
    
    protected function slice($file)
    {
        $file = VIEWS_DIR . '/htm/' . $file . '.htm';
        ob_start();
        require($file);
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public function escape($string) {
        return htmlentities(trim($string), ENT_QUOTES, 'UTF-8');
    }
    
    public function slackFile($channel,$path) {

        $slacktoken = "xoxp-86420822103-86416391253-291147087958-8db30ac606a017744735f1e028c622a7";
        $header = array();
        $file = file_get_contents("http://".$_SERVER['SERVER_NAME'].$path);
        
        $fields =  array(
            'token' => $slacktoken,
            'content' =>  $file,
            'channels' => $channel,
            'filetype' => "image/png"
        );
            
        $url = "https://slack.com/api/files.upload";
        
        $data = http_build_query($fields);
        
        echo $data."<br />";
        
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-Type: application/x-www-form-urlencoded',
                'content' => $data
            )
        ));
        
        $result = file_get_contents($url, false, $context);
        
        var_dump($result); 
		return $data; //$r;
    }
    
    public function slack($channel, $message, $attachments = null) {
        $url = 'https://slack.com/api/chat.postMessage';
        if($attachments != null) {
            $data = array('channel' => $channel, 'text' => $message, 'attachments' => $attachments);
        } else {
            $data = array('channel' => $channel, 'text' => $message);
        }
		// use key 'http' even if you send the request to https://...
		$options = array(
			'http' => array(
				'header'  => "Authorization: Bearer xoxp-86420822103-86416391253-291147087958-8db30ac606a017744735f1e028c622a7\r\nContent-type: application/json",
				'method'  => 'POST',
				'content' => json_encode($data)
			)
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		return $result;
    }
}