<?php 

/**
 * Bot Test Helper
 *
 * Expects the "red_herring" input from forms as a parameter. If the 
 * "red_herring" input is not empty then exit immediately. The assumption 
 * being that only bots would fill out these hidden fields.
 */
function botTest($input) 
{
    if (!empty($input)) {
        exit('YOU DIDN\'T SAY THE MAGIC WORD!');
    }
}

botTest($_POST['red_herring']);