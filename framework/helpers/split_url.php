<?php 

/*
*--------------------------------------------------------------------------
* Split Helper
*--------------------------------------------------------------------------
* Gets the url from the site and explodes it into pieces. Good for when 
* you want to use the url like an array. website/page/param becomes 
* array('website', 'page', 'param').
*/
function splitUrl($url)
{
    $url = explode('/', filter_var(trim($url, '/'), FILTER_SANITIZE_URL));
    return $url;
}