<?php 

// Define app directory contstants
define('ROOT_DIR', str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']));
define('HOST', $_SERVER['HTTP_HOST']);
define('APPLICATION_DIR', ROOT_DIR . '/userland');
define('MODELS_DIR', ROOT_DIR . '/userland/models');
define('VIEWS_DIR', ROOT_DIR . '/userland/views'); 
define('CONTROLLERS_DIR', ROOT_DIR . '/userland/controllers');
define('UPLOADS_DIR', ROOT_DIR . '/userland/documents/');
define('IMAGES_DIR', ROOT_DIR . '/userland/images');
define('LANGUAGE_DIR', ROOT_DIR . '/userland/language');
define('SUCCESS', '{"OK": 1}');
define('FAILURE', '{"OK": 0}');

// System directories
define('SYSTEM_DIR', ROOT_DIR . '/framework');
define('LIBRARIES_DIR', ROOT_DIR . '/framework/libraries');
define('HELPERS_DIR', ROOT_DIR . '/framework/helpers');
define('PLUGINS_DIR', ROOT_DIR . '/framework/plugins'); 

// Basic Settings
define('VERSION', '0.0.0');
define('SITENAME', 'ProjectTracker');
define('STRONG_PASSWORD', false);
date_default_timezone_set('Pacific/Honolulu');

// Database Settings
define('HOSTNAME', '127.0.0.1');
define('USERNAME', 'root');
define('PASSWORD', '');
define('DBNAME', 'projects');

// Error Settings
error_reporting(E_ALL);
ini_set("display_errors", 'On');

// Get files necessary for the app to run.
require_once SYSTEM_DIR . '/router.php';
require_once SYSTEM_DIR . '/database.php';
require_once SYSTEM_DIR . '/DB_Schema.php';
require_once SYSTEM_DIR . '/DB_Displayer.php';
require_once SYSTEM_DIR . '/controller.php';
require_once SYSTEM_DIR . '/DB.php';
require_once SYSTEM_DIR . '/DB_Record.php';
require_once SYSTEM_DIR . '/tables.php';
require_once SYSTEM_DIR . '/libraries/factory.php';

router();