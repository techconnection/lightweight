<?php

/**
* Tickets Controller Class
*
*/
class TicketsController extends Controller 
{

    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/tickets
     * - http://root/tickets/index
     */
    public function index() 
    {
        $this->viewParams['title'] = $this->language->get('tickets/title');
        $this->controller('common/header');
        $this->page('tickets/tickets');
    }
}        