<?php

class SettingsController extends Controller {

    public function index($action = '') 
    {   
        $this->controller('common/header');

        switch ($action) {
            case 'preferences':
                $this->page('settings/preferences');
                break;
            case 'system':
                $this->page('settings/system');
                break;
            case 'users':
                $this->viewParams["userTable"] = $this->user_model->tableElements();
                $this->page('settings/users');
                break;
            case 'invite':
                $this->page('settings/invite');
                break;
            default:
                $this->page('settings/preferences');
                break;
        }
        
        $this->controller('common/footer');
    }
    
    public function validateInvite() 
    {
        $mail_library = $this->library('mail');
        $key = md5(mt_rand());
        $subject = 'You\'ve been invited to Project Tracker';
        $link = 'http://' . HOST . '/invitation/' . $key;
        $body = str_replace('{{activation_link}}', $link, $mail_library->getTemplate('activate'));
        $mail_library->sendMail($send_to = [$_POST['email']], 'Do Not Reply', $subject, $body, $link);
        if ($mail_library->send_success) {
            $fields = $_POST;
            $fields['token'] = $key;
            $fields['account_id'] = $_SESSION['account_id'];
            $signup_model = $this->model('invites');
            if ($signup_model->insert($fields) == FAILURE) {
                exit($this->language->get('settings/database_error'));
            }

            exit(str_replace('{{email}}', $_POST['email'], $this->language->get('settings/activation_sent')));

        } else {
            exit($this->language->get('settings/activation_not_sent'));
        }
    }

}