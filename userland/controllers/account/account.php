<?php 

/**
 * Account Controller Class
 *
 * This class gets the users data from the database for display on the 
 * account view. It also handles edits to the account that are made by the 
 * account holder, activation and password resets.
 */
class AccountController extends Controller 
{
    /**
    * Username from account form.
    * @var string
    */
    private $username;

    /**
    * Firstname from account form.
    * @var string
    */
    private $firstname;

    /**
    * Lastname from account form.
    * @var string
    */
    private $lastname;

    /**
    * Email from account form.
    * @var string
    */
    private $email;

    /**
    * Access the newsletter model.
    * @var object
    */
    private $newsletter;

    /**
    * Access the mail library.
    * @var object
    */
    private $mail;

    /**
    * Access the user model.
    * @var object
    */
    private $user;
    
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/account
     * - http://root/account/index
     *
     * This index method gets user data from the database using the user session.
     * This method will also prepare the data for the account view.
     */
    public function index() 
    {  
        // If the user is logged load all their information.
        if ($this->session->isLogged()) { 
            $this->viewParams['id'] = $this->session->id;
            $this->viewParams['group'] = $this->session->group;
            $this->viewParams['firstname'] = $this->session->firstname;
            $this->viewParams['lastname'] = $this->session->lastname;
            $this->viewParams['username'] = $this->session->username;
            $this->viewParams['email'] = $this->session->email;
            $this->viewParams['website'] = $this->session->website;
            $this->viewParams['registered'] = $this->session->registered;
            $this->viewParams['avatar'] = $this->session->avatar;

            if ($this->session->birthday) {
                $birthday = date('d-F-Y', strtotime($this->session->birthday));
                $birthday = explode('-', $birthday);
                $this->viewParams['day'] = $birthday[0];
                $this->viewParams['month'] = $birthday[1];
                $this->viewParams['month_num'] = date('m', strtotime($birthday[1]));
                $this->viewParams['year'] = $birthday[2];
            }

            $this->controller('common/header');
            $this->view('account/account', $this->viewParams);

        } else {
            $this->controller('common/home');
        }
        $this->controller('common/footer');
    }
    
    public function uploadAvatar()
    {
        if ($this->session->isLogged()) {
            if (!empty($_FILES['avatar'])) {
                $filename = trim(strtolower(str_replace(' ', '_', $_FILES['avatar']['name'])));          
                $filename = mt_rand(100000, 999999) . '_' . $filename;
                $filename = preg_replace('#[^a-z0-9._]#i', '', $filename);
                $fileTmp = $_FILES['avatar']['tmp_name'];
                $file_type = $_FILES['avatar']['type'];
                $file_size = $_FILES['avatar']['size'];
                $file_error = $_FILES['avatar']['error'];
                $extensions = ['jpg', 'jpeg', 'png', 'gif'];
                $tmp = explode('.', $filename);
                $file_extension = strtolower(end($tmp));
                if (in_array($file_extension, $extensions) === false) {
                    $this->alert['file_invalid'] = $this->language->get('account/file_invalid');
                    return;
                } 
                if ($file_size > 1048576) {   
                    $this->alert['file_big'] = $this->language->get('account/file_big');
                    return;
                }
                if (move_uploaded_file($fileTmp, APPLICATION_DIR . 'storage/uploads/account/' . $filename)) {
                    $user_model = $this->model('user');
                    $user_model->updateUser('avatar', $filename, $this->session->id);
                    if (!$user_model->user_updated) {
                        exit('Couldn\'t update user avatar');
                    }
                    $image_source = APPLICATION_DIR . 'storage/uploads/account/' . $filename;
                    if ($file_type == 'image/jpg' || $file_type == 'image/jpeg') {
                        $img = imagecreatefromjpeg($image_source);
                    }
                    if ($file_type == 'image/png') {
                        $img = imagecreatefrompng($image_source);
                    }
                    if ($file_type == 'image/gif') {
                        $img = imagecreatefromgif($image_source);
                    }
                    $size = min(imagesx($img), imagesy($img));
                    $img2 = imagecrop($img, ['x' => 0, 'y' => 0, 'width' => $size, 'height' => $size]);
                    if ($img2 !== false) {
                        imagejpeg($img2, APPLICATION_DIR . 'storage/uploads/account/' . $filename);
                    }

                    $this->alert['file_uploaded'] = str_replace('{{username}}', $this->session->username, $this->language->get('account/file_uploaded'));
                    return;
                } else {
                    $this->alert['uploaded_failed'] = $this->language->get('account/uploaded_failed');
                    return;
                }
            }
        }
    }

    public function validate() 
    {
        if ($this->session->isLogged()) {
            $this->helper('bot_test');
            $this->newsletter = $this->model('account');
            $this->user = $this->model('user');
            $this->validateUsername();
            $this->validateFirstname();
            $this->validateLastname();
            $this->validateEmail();
            $this->validateWebsite();
            $this->validateBirthday();
            $this->validateCountry();
            $this->validatePrivacy();
            $this->validatePassword();

            exit(str_replace('{{username}}', $this->username, $this->language->get('account/account_updated')));
        }
    }
    public function validateUsername()
    {
        if (!empty($_POST['username'])) {
            $this->username = trim(str_replace(' ', '', preg_replace('/[^A-Za-z0-9]/', '', $_POST['username'])));
            if (strlen($this->username) > 20) {
                exit($this->language->get('account/username_invalid'));
            }
            $found_user = $this->user->getUser('username', $this->username);
            if ($found_user && $found_user['id'] != $this->session->id) {
                exit(str_replace('{{username}}', $this->username, $this->language->get('account/username_taken')));
            } else {
                $this->user->updateUser('username', $this->username, $this->session->id);
                if (!$this->user->user_updated) {
                    exit('couldn\'t update username.');
                }
            }
        }
    }

    public function validateFirstname()
    {
        $this->firstname = preg_replace('/[^A-Za-z\-]/', '', trim($_POST['firstname']));
        if (strlen($this->firstname) > 20) { 
            exit($this->language->get('account/name_invalid')); 
        }
        $this->user->updateUser('firstname', $this->firstname, $this->session->id);
        if (!$this->user->user_updated) {
            exit('Couldn\'t update first name.');
        }
    }

    public function validateLastname()
    {
        $this->lastname = preg_replace('/[^A-Za-z\-]/', '', trim($_POST['lastname']));
        if (strlen($this->lastname) > 20) { 
            exit($this->language->get('account/name_invalid')); 
        }
        $this->user->updateUser('lastname', $this->lastname, $this->session->id);
        if (!$this->user->user_updated) {
            exit('Couldn\'t update last name.');
        }
    }

    public function validateEmail()
    {
        if (!empty($_POST['email'])) {
            $this->email = filter_var(trim(strtolower($_POST['email'])), FILTER_SANITIZE_EMAIL);
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                exit($this->language->get('account/email_invalid'));
            }
            $found_user = $this->user->getUser('email', $this->email);
            if ($found_user && $found_user['id'] != $this->session->id) {
                exit(str_replace('{{email}}', $this->email, $this->language->get('account/email_taken')));
            } else {
                $this->user->updateUser('email', $this->email, $this->session->id);
                if (!$this->user->user_updated) {
                    exit('couldnt update email');
                }
            }
        }
    }

    public function validateWebsite()
    {
        $website = $_POST['website'];
        if (preg_match('/^([A-Z0-9-]+\.)+[A-Z]{2,4}$/i', $website) == true || empty($website)) {
            $this->user->updateUser('website', $website, $this->session->id);
            if (!$this->user->user_updated) {
                exit('couldnt update website');
            }
        } else {
            exit($this->language->get('account/website_invalid'));
        }
    }

    public function validateBirthday()
    {
        $day = $_POST['day'];
        $month = $_POST['month'];
        $year = $_POST['year'];
        if (!empty($_POST['day']) && !empty($_POST['month']) && !empty($_POST['year'])) {
            if (is_numeric($day) && is_numeric($year)) {
                $birthday = $year . '-' . $month . '-' . $day;
                $this->user->updateUser('birthday', $birthday, $this->session->id);
                if (!$this->user->user_updated) {
                    exit('couldnt update birthday');
                }   
            } else {
                exit($this->language->get('account/birthday_invalid'));
            }
        }
    }

    public function validatePrivacy()
    {
        if (isset($_POST['privacy'])) { 
            $status = 1; 
        } else {
            $status = 0;
        }

        $this->user->updateUser('privacy', $status, $this->session->id);
        if (!$this->user->user_updated) {
            exit('unable to update privacy');
        }
    }

    public function validatePassword()
    {
        if (!empty($_POST['password']) && !empty($_POST['confirm'])) {
            $password = $_POST['password'];
            $confirm = $_POST['confirm'];
            $settings = $this->model('settings');
            $strong_pw = $settings->getSetting('value', 'name', 'strong_pw');
            if ($strong_pw['value']) {
                $pw_check = $this->helper('passwords');
                $pass_weak = checkPasswordStrength($password);
                if ($pass_weak) {
                    exit($this->language->get('account/pw_weak'));
                }
            }
            if ($password != $confirm) {
                exit($this->language->get('account/pw_match'));
            } 
            $password_hash = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
            $this->user->updateUser('password', $password_hash, $this->session->id);
            if ($this->user->user_updated) {
                unset($_SESSION['id']);
                unset($_SESSION['group']);
                session_destroy();
                exit($this->language->get('account/pw_changed'));
            }
        }
        if (!empty($_POST['password']) && empty($_POST['confirm']) ||  empty($_POST['password']) && !empty($_POST['confirm'])) {
            exit($this->language->get('account/pw_match'));
        }
    }

    /**
    * Validate the users activation key and return true if good and false if bad.
    * @param string $key User key parameter from the url.
    */
    public function activate($key = null)
    {
        $this->viewParams['activation_key'] = $key;
        $raw_this->viewParams = null;

        $user_model = $this->model('user');
        $found_user = $user_model->getUser('key', $key);
        if ($found_user) {
            if ($found_user['group'] >= 1) {
                $raw_this->viewParams['alert'] = $this->language->get('account/key_invalid');
            }
        } else {
            $raw_this->viewParams['alert'] = $this->language->get('account/key_invalid');
        }

        if (!$raw_this->viewParams['alert']) {
            $user_model->updateUser('group', 1, $found_user['id']);
            if ($user_model->user_updated) {
                $raw_this->viewParams['alert'] = $this->language->get('account/activation_success');
            } else {
                $raw_this->viewParams['alert'] = $this->language->get('account/activation_fail');
            }
        }

        $this->controller('common/header');
        $this->view('account/activate', $this->viewParams, $raw_this->viewParams);
        $this->controller('common/footer');
    }

    public function sendActivateMail()
    {
        $user_model = $this->model('user');
        $user_session = $this->session->isLogged();

        $user_model->getUser('email', $user_session['email']);

        $mail_library = $this->library('mail');
        $activation_link = 'http://' . HOST . '/account/activate/' . $user_session['key'];
        $subject = 'Activate Your Account';
        $body = str_replace('{{activation_link}}', $activation_link, $mail_library->getTemplate('activate'));
        $mail_library->sendMail($send_to = [$user_session['email']], 'Do Not Reply', $subject, $body, $activation_link);
        if ($mail_library->send_success) {
            exit($this->language->get('account/activate_mail_sent'));
        } else {
            exit($this->language->get('account/activate_mail_fail'));
        }
    }

    public function usernameLiveCheck() {
        if (isset($_POST)) {
            $username = trim(strtoupper($_POST['username']));
            $this->user = $this->model('user');
            $users = $this->user_model->getAll();   
            foreach ($users as $user) {
                if ($username == strtoupper($user->username)) {
                    echo $username;
                }
            }
        }
    }

    public function emailLiveCheck() {
        if (isset($_POST)) {
            $email = trim(strtolower($_POST['email']));
            $this->user = $this->model('user');
            $emails = $this->user_model->getAll();   
            foreach ($emails as $e) {
                if ($email == strtolower($e->email)) {
                    echo $email;
                }
            }
        }
    }
}