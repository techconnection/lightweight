<?php 

/*
|--------------------------------------------------------------------------
| Activate Controller
|--------------------------------------------------------------------------
| This class activates the users account. The Activate controller can also 
| send activation links if the user did not receive theirs.
*/

class ActivateController extends Controller {
    
    public function index($link_key = '') {


        if (!empty($link_key)) { $this->validateActivation($link_key); } else { $this->requestActivation(); }

        if (isset($this->alert['key_expired'])) { $data['key_expired'] = $this->alert['key_expired']; } else { $data['key_expired'] = ''; }
        if (isset($this->alert['key_invalid'])) { $data['key_invalid'] = $this->alert['key_invalid']; } else { $data['key_invalid'] = ''; }
        if (isset($this->alert['email_invalid'])) { $data['email_invalid'] = $this->alert['email_invalid']; } else { $data['email_invalid'] = ''; }
        if (isset($this->alert['email_exists'])) { $data['email_exists'] = $this->alert['email_exists']; } else { $data['email_exists'] = ''; }
        if (isset($this->alert['activation_success'])) { $data['activation_success'] = $this->alert['activation_success']; } else { $data['activation_success'] = ''; }
        if (isset($this->alert['activation_fail'])) { $data['activation_fail'] = $this->alert['activation_fail']; } else { $data['activation_fail'] = ''; }
        if (isset($this->alert['activate_mail_sent'])) { $data['activate_mail_sent'] = $this->alert['activate_mail_sent']; } else { $data['activate_mail_sent'] = ''; }
        if (isset($this->alert['activate_mail_fail'])) { $data['activate_mail_fail'] = $this->alert['activate_mail_fail']; } else { $data['activate_mail_fail'] = ''; }

        $this->page('activate');

    }

    public function validateActivation($link) {
        $db = DB::table("potentials");
        $results = $db->where("user_key",$link)->getFirst();
        if($results == null) {
            $this->alert['key_invalid'] = $this->language->alert('key_invalid');
            return;
        } else {
            //mathematically speaking there should only be 1 result possible....
            $attribs["company"] = $results->company;
            $attribs["primary_phone"] = $results->primary_phone;
            $attribs["address"] = $results->address;
            $attribs["admin_email"] = $results->email;
            $attribs["verified"] = 1;
            $attribs["license"] = 1;
            $masterAccount = new DB_Record($attribs);
            $ma = DB::table("master_accounts");
            if($ma->insert($masterAccount)) {
                $account_id = $ma->insert_id;
                $masterAccount->exists = true;
                $masterAccount->attributes["id"] = $account_id;
                $masterAccount->id = $account_id;
                //we made a master account! on to next step, make the primary user
                $attribs2["account_id"] = $account_id;
                $attribs2["username"] = $results->username;
                $attribs2["email"] = $results->email;
                $attribs2["password"] = $results->password;
                $user = new DB_Record($attribs2);
                $users = DB::table("users");
                $users->primaryKey = "user_id";
                if($users->insert($user)) {
                    $adminUserID = $users->insert_id;
                    //okay, phase 3, let's go back and tell the masteraccuont who the admin user id is
                    $masterAccount->attributes["admin_user_id"] = $adminUserID;
                    $masterAccount->admin_user_id = $adminUserID;
                    if($ma->update($masterAccount)) {
                        //done with that, let's lastly kill the record from potentials
                        $db->delete(); //why only this?  it still has the old where clause from up top
                        echo "All went well: Master account: ".$account_id." Admin User: ".$adminUserID;
                    } else {
                        $this->alert = str_replace('%error%', $ma->error, $this->language->alert('database_error'));
                        return;
                    }
                } else {
                    $this->alert = str_replace('%error%', $users->error, $this->language->alert('database_error'));
                    return;
                }
            } else {
                $this->alert = str_replace('%error%', $ma->error, $this->language->alert('database_error'));
                return;
            }
        }
    }
    
    
    protected function chasevalidateActivation($link_key) {
        $this->user_model->getUser('user_key', $link_key);
        if (mysqli_num_rows($this->user_model->query) > 0) {
            if ($this->user_model->row['user_group'] >= 1) {
                $this->alert['key_expired'] = $this->language->alert('key_expired');
                return;
            }
        } else {
            $this->alert['key_invalid'] = $this->language->alert('key_invalid');
            return;
        }
        $user_id = $this->user_model->row['user_id'];
        $this->user_model->activateUser('user_group', 1, $user_id);
        if ($this->user_model->account_activated) {
            $this->alert['activation_success'] = $this->language->alert('activation_success');
            return;
        } else {
            $this->alert['activation_fail'] = $this->language->alert('activation_fail');
            return;
        }
    }

    protected function requestActivation() {
        if (isset($_POST['send'])) {
            $email = trim(strtolower($_POST['email']));
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->alert = str_replace('%email%', $email, $this->alerts->email_invalid);
                return $this->alert;
            }
            $this->user_model->getUser('email', $email);
            if (mysqli_num_rows($this->user_model->query) < 1) {
                $this->alert = str_replace('%email%', $this->email, $this->alerts->email_doesnt_exist);
                return $this->alert;
            }
            $activation_link = 'http://' . HOST . '/activate/' . $this->user_model->row['user_key'];
            $subject = 'Activate Your Account';
            $email_body = '<!DOCTYPE html>
                            <html>
                                <head>
                                    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
                                    <title>Devi Deva Yoga Contact Form</title>
                                </head>
                                <body>
                                    <div style="display:block; width:800px; margin:auto; text-align:center;">
                                        <a href="http://techsourcehawaii.com"><img src="http://techsourcehawaii.com/views/img/logo.png" alt="Email Logo"></a>
                                        <div>
                                            <div>Click the link below to activate your account.</div> <br>
                                            ' . $activation_link . '
                                            <div>If the link does not open copy and paste it to the url in your browser.</div> <br>
                                        </div>
                                    </div>
                                </body>
                            </html>';
            $this->mail = $this->inc('mail');
            $this->mail->sendMail($send_to = [$email], 'Do Not Reply', $subject, $email_body, $activation_link);
            if ($this->mail->send_success) {
                $this->alert['activate_mail_sent'] = $this->language->alert('activate_mail_sent');
                return;
            } else {
                $this->alert['activate_mail_fail'] = $this->language->alert('activate_mail_fail');
                return;
            }
        }
    }
}