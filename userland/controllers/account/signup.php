<?php 

/**
 * Signup Controller Class
 *
 * This class handles user sign up. It saves post data from the signup form 
 * to the database and also sends the initial activation email.
 */
class SignupController extends Controller 
{

    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://tekcx.com/tracker/signup
     * - http://tekcx.com/tracker/signup/index
     */
    public function index() 
    {   

        $this->viewParams['title'] = $this->language->get('signup/title');
        
        $this->page('account/signup');
    }
    
    /**
     * Incoming format:
     * 'company' => string 'Kokua PC' (length=8)
     * 'address' => string '' (length=0)
     * 'phone' => string '' (length=0)
     * 'username' => string 'max' (length=3)
     * 'email' => string 'max@yourtechconnection.com' (length=26)
     * 'password' => string 'Sp@mhol3' (length=8)
     * 'confirm_password' => string 'Sp@mhol3' (length=8)
     * 'sign_up' => string 'Sign up' (length=7)
     */
    public function validate() {
        $this->user_model = $this->model('user');

        $this->validateCompanyName();
        $this->validateAddress();
        $this->validatePirmaryPhone();
        $this->validateFirstname();
        $this->validateLastname();
        $this->validateUsername();
        $this->validateEmail();
        $this->validatePassword();
        $this->signupUser();
        $this->createPotential();
        $this->sendActivationMail();
    }

    public function validateCompanyName()
    {
        $this->company = preg_replace('/[^A-Za-z0-9]/', '', $_POST['company']);
    }

    public function validateAddress()
    {
        $this->address = preg_replace('/[^A-Za-z0-9, ]/', '', $_POST['address']);
    }

    public function validatePirmaryPhone()
    {
        $this->phone = preg_replace('/[^0-9() ]/', '', $_POST['phone']);
        if (strlen($this->phone) > 16) {
            exit($this->language->get('signup/phone_invalid'));
        }
    }

    public function validateFirstname() 
    {
        $this->firstname = preg_replace('/[^A-Za-z0-9]/', '', $_POST['firstname']);
        if (strlen($this->firstname) > 20) {
            exit($this->language->get('signup/firstname_invalid'));
        }
    }

    public function validateLastname() 
    {
        $this->lastname = preg_replace('/[^A-Za-z0-9]/', '', $_POST['lastname']);
        if (strlen($this->lastname) > 20) {
            exit($this->language->get('signup/lastname_invalid'));
        }
    }

    public function validateUsername() 
    {
        $this->username = strtolower($this->firstname . $this->lastname);
        /*$this->username = preg_replace('/[^A-Za-z0-9]/', '', $_POST['username']);
        if (strlen($this->username) > 20) {
            exit($this->language->get('signup/username_invalid'));
        }
        $stored_username = $this->user_model->getUser('username', $this->username);
        if ($stored_username) {
            exit($this->language->get('signup/username_taken'));
        }*/
    }

    public function validateEmail()
    {
        $this->email = filter_var(trim(strtolower($_POST['email'])), FILTER_SANITIZE_EMAIL);
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            exit($this->language->get('signup/email_invalid'));
        }
        $stored_email = $this->user_model->getUser('email', $this->email);
        if ($stored_email) {
            exit($this->language->get('signup/email_taken'));
        }
    }

    public function validatePassword()
    {
        $password = $_POST['password'];
        $confirm_password = $_POST['confirm_password'];
        $settings_model = $this->model('settings');
        $setting = $settings_model->getSetting('name', 'strong_pw');
        if ($setting->value) {
            $pw_check = $this->helper('password-strength');
            $pass_weak = checkPasswordStrength($password);
            if ($pass_weak) {
                exit($this->language->get('signup/pw_weak'));
            }
        }
        if ($password !== $confirm_password) {
            exit($this->language->get('signup/pw_match'));
        } 
        $hashed_password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
        $this->hashed_password = $hashed_password;
    }

    public function createPotential()
    {
        $potentials_model = $this->model('potentials');
        $potentials_model->insertPotential($this->company, $this->address, $this->phone, $this->username, $this->email, $this->hashed_password, $this->user_key);
        if (!$potentials_model->potential_inserted) {
            exit('Unable to insert potential');
        }
    }

    public function signupUser()
    {
        $this->user_key = md5(mt_rand());
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->user_model->insertUser($this->company, $this->address, $this->phone, $this->firstname, $this->lastname, $this->username, $this->email, $this->hashed_password, $this->user_key, $ip);
        if (!$this->user_model->user_inserted) {
            exit($this->language->get('signup/signup_fail'));
        }
        $user = $this->user_model->getUser('username', $this->username);
        $this->user_model->updateUser('account_id', $user->user_id, $user->user_id);
        $this->user_model->updateUser('parent_id', $user->user_id, $user->user_id);
        if ($user->user_id == 1) {
            $this->user_model->updateUser('user_group', 3, $user->user_id);
        }
    }

    public function sendActivationMail()
    {
        $mail_library = $this->library('mail');
        $subject = 'Activate Your Account';
        $activation_link = 'http://' . HOST . '/account/activate/' . $this->user_key;
        $body = str_replace('{{activation_link}}', $activation_link, $mail_library->getTemplate('activate'));
        $mail_library->sendMail($send_to = [$this->email], 'Do Not Reply', $subject, $body, $activation_link);
        if ($mail_library->send_success) {
            $mail_library->sendMail($send_to = ['chase@techsourcehawaii.com'], '', 'New User!', str_replace('{{sitename}}', SITENAME, $mail_library->getTemplate('signup')), 'new user registered at' . SITENAME);
            exit($this->language->get('signup/signup_success'));
        } else {
            exit($this->language->get('signup/activate_mail_fail'));
        }
    }
}