<?php 

/**
* Logout Controller Class
*
* The Logout controller will verify logouts and logout inactive users.
*/
class LogoutController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/logout
     * - http://root/logout/index
     *
     * This index method uses the action parameter to decide what kind of
     * logout is to take place.
     *
     * @param string
     */
    public function index($action = '')
    {       
        if ($action == '') {
            $this->viewParams['inactive'] = null;
            $user_model = $this->model('user');  
            $settings = $this->model('settings');
            $verify_logout = $settings->getSetting('name', 'verify_logout');
            if (!$verify_logout->value) {
                $this->logout(true);
            }
        } elseif ($action == 'confirm') {
            $this->logout(true);
        } elseif ($action == 'inactive') {
            $this->viewParams['inactive'] = true;
            $this->logout();
        } else {
            header('Location:/home');
        }

        $this->controller('common/header');
        $this->page('account/logout');
    }

    public function logout($redirect = null)
    {
        unset($_SESSION['user_id']);
        session_destroy();
        if ($redirect) {
            header('Location:/home');
        }
    }
}