<?php

/**
* Login Controller Class
*
* The login controller class validates user logins and creates sessions to 
* track users that have logged in. It also tracks failed login attempts.
*/
class LoginController extends Controller 
{

    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://tekcx.com/tracker/login
     * - http://tekcx.com/tracker/login/index
     */
    public function index() 
    {
        $this->viewParams['title'] = $this->language->get('login/title');
        $this->page('account/login');
    }

    /**
     * Validate the login form
     *
     * The post data is submitted by ajax in the login view. If the data is 
     * valid this method will also access the session library to create a 
     * session and log the user in.
     */
    public function validate()
    {   
        // Test for bots using the bot test helper.
        $this->helper('bot_test');

        // Declare vars for users post data and sanitize the data.
        $email = filter_var(trim(strtolower($_POST['email'])), FILTER_SANITIZE_EMAIL);
        $password = $_POST['password'];

        // Load the user model class.
        $user_model = $this->model('user');

        // Get everything from the users table where the stored email adress is the 
        // same as the email submitted. If there is no user with that email or the
        // user is not in good standing exit with an error.
        $current_user = $user_model->getUser('email', $email);
        if (!$current_user) {
            exit($this->language->get('login/login_fail'));
        }
        if ($current_user->user_group == 4) {
            exit($this->language->get('login/locked'));
        }

        // If the current user email is not null and the password is verified
        // update the users saved data and create the users sessions. If not
        // log some information about the users login attempt.
        if ($current_user->email && password_verify($password, $current_user->password)) {
            $user_model->updateUser('login_attempts', 0, $current_user->user_id);
            $user_model->updateUser('locked_time', null, $current_user->user_id);
            $user_model->updateUser('last_active', time(), $current_user->user_id);
            $user_model->updateUser('ip', $_SERVER['REMOTE_ADDR'], $current_user->user_id);
            $this->session->createSession('user_id', $current_user->user_id);
            $this->session->createSession('user_group', $current_user->user_group);
            $this->session->createSession('account_id', $current_user->account_id);
            $this->session->createSession('username', $current_user->username);
        } else {
            // Increment the login attempt number.
            $attempts = ++$current_user->login_attempts;

            // Update the users table with the new login attempt number.
            $user_model->updateUser('login_attempts', $attempts, $current_user->user_id);

            // Declare the current date and time.
            $date = new DateTime();

            // Check the login attemps and if there have been 5 or more lock the
            // account for 5 minutes. If there have been 10 attempts or more ban
            // the account.
            if ($attempts >= 5 && $attempts <= 10) {
                if (!$current_user->locked_time) {
                    $user_model->updateUser('locked_time', $date->format('U'), $current_user->user_id);
                }
                if (time() - $current_user->locked_time < 5 * 60) {
                    exit($this->language->get('login/locked_5'));
                }
            }
            if ($attempts >= 10) {
                $user_model->updateUser('user_group', 4, $current_user->user_id);
                exit($this->language->get('login/locked'));
            }

            // Exit with basic login error if all other checking failed to find 
            // a reason.
            exit($this->language->get('login/login_fail'));
        }
    }
}        