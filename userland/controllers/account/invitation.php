<?php 

/*
 * Invitation Controller Class
 *
 */
class InvitationController extends Controller 
{
    public function index($token) 
    {
        $db = DB::table('invites');
        $results = $db->where('token', $token)->getFirst();
        if (!$results) {
            exit($this->language->get('invitation/token_invalid'));
        } 

        $this->viewParams['email'] = $results->email;

        $this->popup('account/invitation');
    }

    public function activate()
    {
        $db = DB::table('invites');
        $results = $db->where('token', $token)->getFirst();
        //mathematically speaking there should only be 1 result possible....
        $account_id = $results->account_id;
        $password_hash = password_hash($_POST['password'], PASSWORD_BCRYPT, array('cost' => 12));
        $attr['account_id'] = $account_id;
        $attr['email'] = $results->email;
        $attr['password'] = $password_hash;
        $user = new DB_Record($attr);
        $users = DB::table('users');
        $users->primaryKey = 'user_id';
        if ($users->insert($user)) {
            //done with that, let's lastly kill the record from invites
            $db->delete(); //why only this?  it still has the old where clause from up top
            header('Location:/home');
        } else {
            exit('FAIL');
        }
    }
}