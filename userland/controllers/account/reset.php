<?php 


class ResetController extends Controller 
{

    public function index($id = '', $token = '')
    {   
        $this->viewParams['change_pw'] = null;
        $this->viewParams['link_invalid'] = null;

        $account_model = $this->model('account');
        if (!empty($id) && !empty($token)) {
            $link = $account_model->getRecoveryLink('token', $token);
            $created_time = strtotime($link->date_created);
            if (time() - $created_time > 5 * 60) {
                $account_model->deleteRecoveryLink('token', $token);
                $this->viewParams['link_invalid'] = $this->language->get('account/link_invalid');
            } else {
                $this->viewParams['change_pw'] = true;
            } 
        }
        $this->viewParams['id'] = $id;

        $this->page('account/reset');
    }

    public function sendRecoveryMail()
    {
        $email = filter_var(trim(strtolower($_POST['email'])), FILTER_SANITIZE_EMAIL);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            exit(str_replace('{{email}}', $email, $this->language->get('account/email_invalid')));
        }

        $user_model = $this->model('user');
        $account_model = $this->model('account');
        $found_user = $user_model->getUser('email', $email);
        if ($found_user) {
            $token = bin2hex(openssl_random_pseudo_bytes(16));
            $recovery_link = 'http://' . HOST . '/reset/' . $found_user->user_id . '/' . $token;
            $subject = 'Password Reset';
            $mail_library = $this->library('mail');
            $body = str_replace('{{recovery_link}}', $recovery_link, $mail_library->getTemplate('reset'));
            $mail_library->sendMail($send_to = [$email], 'Do Not Reply', $subject, $body, $recovery_link);
            if ($mail_library->send_success) {
                $account_model->insertLink($email, $token);
                if ($account_model->link_inserted) {
                    exit($this->language->get('account/recovery_sent')); 
                }
            } else {
                exit(str_replace('{{email}}', $email, $this->language->get('account/recovery_not_sent')));
            }
        }
    }

    public function saveResetPassword()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
        }

        if (!empty($_POST['password']) && !empty($_POST['confirm_pw'])) {
            $password = $_POST['password'];
            $confirm_pw = $_POST['confirm_pw'];
            $settings_model = $this->model('settings');
            $strong_pw = $settings_model->getSetting('name', 'strong_pw');
            if ($strong_pw->value) {
                $pw_check = $this->helper('passwords');
                $pass_weak = checkPasswordStrength($password);
                if ($pass_weak) {
                    exit($this->language->get('account/pw_weak'));
                }
            }
            if ($password != $confirm_pw) {
                exit($this->language->get('account/pw_match'));
            } 
            $password_hash = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
            $user_model = $this->model('user');
            $user_model->updateUser('password', $password_hash, $id);
            if ($user_model->user_updated) {
                unset($_SESSION['id']);
                unset($_SESSION['group']);
                session_destroy();
                exit($this->language->get('account/pw_changed'));
            }
        }
        if (!empty($_POST['password']) && empty($_POST['confirm_pw']) ||  empty($_POST['password']) && !empty($_POST['confirm_pw'])) {
            exit($this->language->get('account/pw_match'));
        }
    }

}