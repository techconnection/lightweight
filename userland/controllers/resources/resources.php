<?php 

/**
 * Resources Controller Class
 */
class ResourcesController extends Controller 
{
    
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://tekcx.com/tracker/resources
     * - http://tekcx.com/tracker/resources/index
     */
    public function index() 
    {   
        $this->viewParams['title'] = $this->language->get('resources/title');
        $project_model = $this->project_model;
        $this->viewParams['resources'] = App::resources()->get();
        $this->controller('common/header');
        $this->page('resources/resources');
        $this->controller('common/footer');
    }
    
    public function edit($resourceID) 
    {
        $resource = App::resources()->find($resourceID);
        $this->viewParams['title'] = 'Edit Resource';
        $this->extractRecord($resource);
        $this->controller('common/header');
        $this->page('resources/edit');
        $this->controller('common/footer');
    }
}