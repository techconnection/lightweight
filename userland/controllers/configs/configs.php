<?php

class ConfigsController extends Controller {
    
    public function __construct() {
        parent::__construct();
        $this->project_model = $this->model("config");
    }

    public function index($action = '', $parent_id = 0) {
        if ($this->validateNewProject($parent_id) == true) $this->redirect('http://' . $_SERVER['SERVER_NAME'] . '/configs/configurations');//.$this->project_model->DB->insert_id);
        $this->viewParams['parent_id'] = $parent_id;

        $this->controller('common/header');

        if ($action == 'new') {
            $this->page('configs/new');
        } else if ($action == '') {
            $this->viewParams["configs"] = $this->project_model->getAll();
            $this->page('configs/configurations');
        }
        $this->controller('common/footer');
    }
    
    public function newTask($projID) {
        $this->index($projID);
    }

    protected function validateNewProject($parent_id) {
        if (isset($_POST['project_name'])) {
            $project_name = trim(preg_replace('/[^A-Za-z0-9?![:space:]]/', '', $_POST['project_name']));
            $this->project_model->quickInsert($project_name, $parent_id, 0, 'config');
            if (!$this->project_model->project_inserted) {
                exit('Failed to create new configuration');
            }
            return true;
        }
        return false;
    }    
    
    public function configurations($parent_id = 0) {
        if ($this->validateNewProject($parent_id) == true) $this->redirect('http://' . $_SERVER['SERVER_NAME'] . '/configs/configurations');//.$this->project_model->DB->insert_id);
        $this->viewParams["configs"] = $this->project_model->getAll();
        $this->controller('common/header');
        $this->page('configs/configurations');
        $this->controller('common/footer');
    }
    
    
    public function detail($projID) {
        $project = $this->project_model->find($projID);
        $this->viewParams['title'] = 'Config Detail';
        $this->viewParams['projID'] = $projID;
        $this->viewParams = array_merge($project->attributes,$this->viewParams);
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID);
        $this->controller('common/header');
        $this->page('configs/detail');
        $this->controller('common/footer');
    }
    
    
    public function files($projID) {
        $this->viewParams['title'] = 'Config Files';
        $project_model = $this->project_model;
        $project = $project_model->find($projID);
        $this->viewParams['title'] = 'Project Detail';
        $this->viewParams = array_merge($project->attributes,$this->viewParams);
        $this->viewParams['projID'] = $projID;
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID);
        $this->viewParams['documents'] = displayDocuments($projID);
        $this->controller('common/header');
        $this->page('configs/files');
        $this->controller('common/footer');
    }
    
    
    public function upload($projID){
        $this->viewParams['title'] = 'Config-Upload Documents';
        $this->viewParams['id'] = $projID;
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID);
        $this->controller('common/header');
        $this->page('configs/uploader');
        $this->controller('common/footer');
    }
}