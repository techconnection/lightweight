<?php 

class AssetsController extends Controller 
{
    public function index() 
    {     
        $this->controller('common/header');
        $this->page('assets/assets');
        $this->controller('common/footer');
    }
}