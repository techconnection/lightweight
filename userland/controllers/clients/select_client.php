<?php 

class Select_ClientController extends Controller {

    public function index() {
        $allClients = $this->client_model->getAll();
        $this->viewParams['allClients'] = $allClients;
        $this->view('clients/select');
    }
}