<?php 

class ClientsController extends Controller {
    
    protected function validateNewProject($client_id,$type="project") 
    {
        if (isset($_POST['project_name'])) {
            $project_name = trim(preg_replace('/[^A-Za-z0-9?![:space:]]/', '', $_POST['project_name']));
            $this->project_model->quickInsert($project_name, 0, $client_id, $type);
            if (!$this->project_model->project_inserted) {
                exit('Failed to create project');
            }
            return true;
        }
        return false;
    }
    
    public function index() {

        $this->client_model->DB->reset();

        $this->viewParams['title'] = 'Clients';
        $this->client_model->formTypes["id"]["type"] = "ignore"; //I want a blank form, don't try to track a null id
        $this->viewParams['clientForm'] = $this->client_model->formElements();
        $this->viewParams["clients"] = $this->client_model->getAll();
        $this->controller('common/header');
        $this->page('clients/clients');
        $this->controller('common/footer');
    }
    
    
    public function summary($clientid) {
        $this->viewParams['title'] = 'Client';
        $clientRecord = $this->client_model->find($clientid);
        $this->extractRecord($clientRecord);
        $places_model = $this->model("places");
        $places = $places_model->getPlacesForID($clientid);
        if(count($places) != 0) {
            $this->viewParams['places'] = $places;
        }
        $this->viewParams['inprogress'] = $this->project_model->getRootProjectsCount($clientid);
        $this->viewParams['configs'] = $this->project_model->getRootConfigurationsCount($clientid);
        $this->viewParams['archived'] = $this->project_model->getRootProjectsCount($clientid,"archive");
        $this->viewParams['completed'] = $this->project_model->getCompletedProjectsCount($clientid);
        $this->people_model = $this->model("people");
        $clientRecord = $this->client_model->find($clientid);
        $this->viewParams['people'] = $this->people_model->getPeopleForID($clientid);
        $this->controller('common/header');
        $this->page('clients/summary');
        $this->controller('common/footer');
    }
    
    public function projects($clientid) {
        
        if ($this->validateNewProject($clientid,"project") == true) $this->redirect('http://' . $_SERVER['SERVER_NAME'] . '/clients/projects/'.$clientid);
        
        $clientRecord = $this->client_model->find($clientid);
        $this->viewParams['projects'] = $this->project_model->getRootProjects($clientid);
        $this->extractRecord($clientRecord);
        $this->viewParams['title'] = "Projects";
        $this->controller('common/header');
        $this->page('clients/projects');
        $this->controller('common/footer');
    }
    
    
    public function people($clientid) {
        $this->people_model = $this->model("people");
        $clientRecord = $this->client_model->find($clientid);
        $this->viewParams['people'] = $this->people_model->getPeopleForID($clientid);
        $this->extractRecord($clientRecord);
        $this->viewParams['title'] = "People";
        $this->controller('common/header');
        $this->page('clients/people');
        $this->controller('common/footer');
    }
    
    
    public function places($clientid) {
        $this->places_model = $this->model("places");
        $clientRecord = $this->client_model->find($clientid);
        $this->viewParams['places'] = $this->places_model->getPlacesForID($clientid);
        $this->extractRecord($clientRecord);
        $this->viewParams['title'] = "Places";
        $this->controller('common/header');
        $this->page('clients/places');
        $this->controller('common/footer');
    }
    
    
    public function configs($clientid) {
        if ($this->validateNewProject($clientid,"config") == true) $this->redirect('http://' . $_SERVER['SERVER_NAME'] . '/clients/configs/'.$clientid);
        
        $clientRecord = $this->client_model->find($clientid);
        $this->viewParams['projects'] = $this->project_model->getRootConfigurations($clientid);
        $this->extractRecord($clientRecord);
        $this->viewParams['title'] = "Configs";
        $this->controller('common/header');
        $this->page('clients/configs');
        $this->controller('common/footer');
    }
    
    
    public function detail($clientid) {
        $this->viewParams['title'] = 'Edit Client';
        $clientRecord = $this->client_model->find($clientid);
        $this->viewParams = array_merge($clientRecord->attributes, $this->viewParams);
        $this->controller('common/header');
        $this->page('clients/edit');
        $this->controller('common/footer');
    }
    
    
    public function delete($clientid) {

        $clientRecord = $this->client_model->find($clientid);

        $this->viewParams['title'] = 'Delete Client';

        if($clientRecord == null) {
            $this->index();
        } else {
            $this->extractRecord($clientRecord);
            $this->view('clients/delete');
        }
    }
    
    public function delconfirmed($clientid) {
        $this->client_model->delete($clientid);
        $this->redirect("http://" . $_SERVER['SERVER_NAME'] . '/clients');
    }
    
}