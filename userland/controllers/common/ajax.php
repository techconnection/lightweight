<?php 

/**
 * AJAX Controller
 *
 * This is an API interface, i,e, we call this for functions where we want 
 * to have a calculation performed and then get back some HTML that shows 
 * the result of the calculation, either in the form of some list, divs, 
 * tables, etc or if that isn't appropriate we will pass back some JSON.
 *
 * This file is called primarily by javascript within the rest of the app, 
 * so for error handling some json may be sent back, but my preference is to 
 * use the element.innerHTML property and set what I want via php.  Dual 
 * processing of the pages via javascript in some cases and php in others 
 * is a real upkeep challenge.
 */
function JSStringEscape($data)
{
    $safe = "";
    for ($i = 0; $i < strlen($data); $i++) {
        if (ctype_alnum($data[$i])) {
            $safe .= $data[$i];
        } else {
            $safe .= sprintf("\\x%02X", ord($data[$i]));
        }
    }
    return $safe;
}

function encodeForJS(DB_Record $item) {
    $item->attributes["displayName"] = JSStringEscape($item->company);
    return $item;
}

class AjaxController extends Controller {
    
    public function index() {
        $project_model = $this->project_model;
        $client_model = $this->client_model;
        if (!isset($_POST['command'])) {
            echo FAILURE;
            return;
        } else {
            $command = $_POST['command'];
            unset($_POST['command']); //we don't want POST to contain any non-data related info for this file
            unset($_POST['client-name']);
        }
        switch($command) {
            case "refreshSearchResults":
                $foundClients = $this->client_model->search("company", $_POST['filter']);
                $allClients = array_map("encodeForJS", $foundClients);
                $this->viewParams['allClients'] = $allClients;
                $this->view('clients/select');
                break;
            case "clientTable":
                //echo $client_model->tableElements();//getClientTable();
                $display = new DB_Displayer();
                $data["clients"] = $client_model->getAll();
                $display->template($data,"clients/table");
                break;
            case "insertClient":
                $this->log_model->insert(["description" => "Created client record " . $_POST['company']]);
                echo $client_model->insert($_POST);
                break;
            case "updateClient":
                $this->log_model->insert(["description" => "Modified client record " . $_POST['company'], "clientid" => $_POST['id']]);
                echo $client_model->update($_POST);
                break;
            case "updateProject":
                $this->log_model->insert(["description" => "Edited ticket " . $_POST['projectname'], "ticketid" => $_POST['id']]);
                echo $project_model->update($_POST);
                break;
            case "archiveProject":
                //$this->log_model->insert(["description" => "Archiving ".$_POST['projectname'], "ticketid" => $_POST['id']]);
                if(isset($_POST['projects'])) {
                    $projects = $_POST['projects'];
                    if(is_array($projects)) {
                        $db = App::tickets();
                        foreach($projects as $proj) {
                            $db->where("id",$proj)->massUpdate(["state" => "archive"]);
                            $db->reset();
                        }
                    }
                }
                echo $project_model->displayProjectList(null,$_POST['filter']);
                break;
            case "unarchiveProject":
                //$this->log_model->insert(["description" => "Archiving ".$_POST['projectname'], "ticketid" => $_POST['id']]);
                if(isset($_POST['projects'])) {
                    $projects = $_POST['projects'];
                    if(is_array($projects)) {
                        $db = App::tickets();
                        foreach($projects as $proj) {
                            $db->where("id",$proj)->massUpdate(["state" => "active"]);
                            $db->reset();
                        }
                    }
                }
                echo $project_model->displayProjectList(null,$_POST['filter']);
                break;
            case "insertTask":
                $this->log_model->insert(["description" => "Created sub task " . $_POST['projectname']]);
                $project_model->quickInsert($_POST['projectname'], $_POST['parent_id']);
                echo displayTaskList($_POST['parent_id']);
                break;
            case "insertPerson":
                $this->log_model->insert(["description" => "Created person " . $_POST['first'] . " " . $_POST['last']]);
                $people_model = $this->model("people");
                $people_model->insert($_POST);
                echo $people_model->displayPeopleList($_POST['parent_id']);
                break;
            case "deletePerson":
                if(isset($_POST['people'])) {
                    $this->log_model->insert(["description" => "Deleting persons from " . $_POST['parent_id']]);
                    $people_model = $this->model("people");
                    $persons = $_POST['people'];
                    foreach($persons as $guy) {
                        $people_model->delete($guy);
                    }
                    echo $people_model->displayPeopleList($_POST['parent_id']);
                }
                break;
            case "insertPlace":
                $this->log_model->insert(["description" => "Created place " . $_POST['name'] . " at " . $_POST['street']]);
                $p_model = $this->model("places");
                $p_model->insert($_POST);
                echo $p_model->displayPlacesList($_POST['parent_id']);
                break;
            case "deletePlace":
                if(isset($_POST['places'])) {
                    $this->log_model->insert(["description" => "Deleting place from " . $_POST['parent_id']]);
                    $p_model = $this->model("places");
                    $p = $_POST['places'];
                    foreach($p as $guy) {
                        $p_model->delete($guy);
                    }
                    echo $p_model->displayPlacesList($_POST['parent_id']);
                }
                break;
            case "insertResource":
                $this->log_model->insert(["description" => "Created resource " . $_POST['name']]);
                $project_model->insertResource($_POST);
                $display = new DB_Displayer();
                $data["resources"] = App::resources()->get();
                $display->template($data,"resources/table");
                break;
            case "updateResource":
                $this->log_model->insert(["description" => "Edited resource " . $_POST['name'], "resourceid" => $_POST['id']]);
                echo $project_model->updateResource($_POST);
                break;
            case "deleteResource":
                $this->log_model->insert(["description" => "Deleted resource " . $_POST['name'], "resourceid" => $_POST['id']]);
                echo $project_model->deleteResource($_POST["id"]);
                break;
            case "deleteTicket":
                $this->log_model->insert(["description" => "Deleted config ".$_POST['id']]);
                echo $project_model->delete($_POST["id"]);
                break;
            case "assignResource":
                if(isset($_POST['resource_id'])) {
                    $this->log_model->insert(["description" => "Assigned resource " . $_POST['resource_id'], "resourceid" => $_POST['resource_id']]);
                    $project_model->assignResource($_POST);
                }
                echo displayAssignedResourcesList($_POST['ticket_id']);
                break;
            case "unassignResource":
                $this->log_model->insert(["description" => "Unassigned resource " . $_POST['id'], "resourceid" => $_POST['id']]);
                $project_model->unassignResource($_POST['id']);
                echo displayAssignedResourcesList($_POST['ticket_id']);
                break;
            default:
                echo FAILURE;
        }
    }
}