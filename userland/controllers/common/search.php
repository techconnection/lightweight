<?php 

/*
|--------------------------------------------------------------------------
| Search Controller
|--------------------------------------------------------------------------
| This class handles user searches and the display of the search results.
*/

class SearchController extends Controller {

    public function index() {   
        
        $this->searchAll();
        $this->viewParams['title'] = 'Search Page';
        $this->viewParams['search_term'] = $this->search_term;
        $this->viewParams['users'] = $this->users;
        if (!empty($this->posts)) {
            foreach ($this->posts as $p) {
                $this->viewParams['blog_posts'][] = [
                    'blog_id' => $p['blog_id'],
                    'author' => $p['author'],
                    'blog_title' => $p['blog_title'],
                    'body' => $p['body'],
                    'image' => $p['image'],
                    'date_posted' => date('d M, Y', strtotime($p['date_posted'])),
                    'blog_link' => strtolower(str_replace(' ', '_', $p['blog_title'])),
                ];
            }
        }

        $this->page('search');
    }

    protected function searchAll() {
        $this->search_term = '';
        $this->users = '';
        $this->posts = '';
        if (isset($_POST['search'])) {
            if (strlen($_POST['search_term']) > 0) {
                $this->search_term = trim($_POST['search_term']);
                $this->users = $this->search->searchUsers($this->search_term);
                $this->posts = $this->search->searchBlogPosts($this->search_term);
            }
        }
    }
}