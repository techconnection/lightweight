<?php 

class HomeController extends Controller 
{
    public function index() 
    {     
        $this->viewParams['title'] = 'Dashboard';
        $this->viewParams['history'] = $this->log_model->tableElements();
        $this->viewParams['today'] = $this->log_model->recentItems();
        $this->viewParams['older'] = $this->log_model->olderItems();
        $this->viewParams['inprogress'] = $this->project_model->getRootProjectsCount();
        $this->viewParams['prospects'] = $this->project_model->getRootProjectsCount(null, 'prospect');
        $this->viewParams['archived'] = $this->project_model->getRootProjectsCount(null, 'archive');
        $this->viewParams['completed'] = $this->project_model->getCompletedProjectsCount(null);
        $this->viewParams['todays'] = $this->project_model->getTodaysProjects(null, 'active');
        $this->viewParams['tasks'] = $this->project_model->getTodaysProjects(null, 'active', true);
        $this->controller('common/header');
        $this->page('common/home');
        $this->controller('common/footer');
    }
}