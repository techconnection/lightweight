<?php 

/**
 * Slack Controller
 *
 * This is an API interface for slack.  Slack commands embedded in our channel send us JSON to interpret.
 *
 * Our slack workspace calls this directly, so none of the usual SESSION variables will be available.
 * I've made some functions that attempt to determine what account_id we should be using when an incoming
 * Slack call is encountered.
 *
 * 
 */
function JSStringEscape($data)
{
    $safe = "";
    for ($i = 0; $i < strlen($data); $i++) {
        if (ctype_alnum($data[$i])) {
            $safe .= $data[$i];
        } else {
            $safe .= sprintf("\\x%02X", ord($data[$i]));
        }
    }
    return $safe;
}

function encodeForJS(DB_Record $item) {
    $item->attributes["displayName"] = JSStringEscape($item->company);
    return $item;
}

function findSlackAccount($token) {
    $db = DB::table('master_accounts');
    $record = $db->where("token",$token)->getFirst();
    if($record != null) {
        $_SESSION['user_id'] = $record->admin_user_id;
        $_SESSION['username'] = "Slack";
        return $record->id;
    } else {
        return -1;
    }
}

class SlackController extends Controller {
    
    public function index() {
        $project_model = $this->project_model;
        $client_model = $this->client_model;
        if (!isset($_POST['command'])) {
            echo FAILURE;
            return;
        } else {
            $command = $_POST['command'];
            unset($_POST['command']); //we don't want POST to contain any non-data related info for this file
            unset($_POST['client-name']);
        }
        $_SESSION['account_id'] = findSlackAccount($_POST['token']);
        if($_SESSION['account_id'] != -1){
            switch($command) {
                case "/log":
                    $db = App::tickets();
                    $ticket = $db->where("slack",$_POST['channel_name'])->getFirst();
                    if($ticket != null) {
                        $date = new DateTime();
                        $date = $date->format("m/d/y h:iA");
                        $ticket->scope = $ticket->scope."\n\r<br />".$date." - ".$_POST['user_name'].": ".$_POST['text'];
                        $text = $_POST['user_name'].": ".$_POST['text'];
                        $db->update($ticket);
                        header('Content-Type: application/json');
echo <<<EOT
{
    "response_type": "in_channel",
    "text": "Updated: $text"
}
EOT;
                    } else {
                        echo "Failed, no project entry for this channel.";
                    }
                    break;
                default:
                    echo "Failed, unusual command.";
            }
        } else {
            echo "Failed, API token not matched.";
        }
    }
}