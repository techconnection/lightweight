<?php 

class HeaderController extends Controller 
{
    public function index() 
    {     
        // Check if the user is logged in.
        if ($this->session->isLogged()) {
            $this->viewParams['logged'] = true;
            $this->viewParams['firstname'] = $this->session->firstname;
            $this->viewParams['lastname'] = $this->session->lastname;
        } else {
            $this->viewParams['logged'] = null;
        }

        // Load the split url helper.
        $this->helper('split_url');
        //Break the url into an array.
        $url = isset($_GET['url']) ? splitUrl($_GET['url']) : [''];

        // Use the url array to set the page title and meta description.
        switch ($url[0]) {
            case '':
                $this->viewParams['title'] = $this->language->get('home/title');
                $this->viewParams['description'] = $this->language->get('home/description');
                break;
            case 'search':
                $search_param = isset($url[1]) ? $url[1] : '';
                $this->viewParams['title'] = 'Search: ' . $search_param;
                $this->viewParams['description'] = $this->language->get('search/search');
                break;
            case 'projects':
                $action = isset($url[1]) ? $url[1] : '';
                $this->viewParams['title'] = 'Projects: ' . ucfirst($action);
                $this->viewParams['description'] = $this->language->get($url[0] . '/description');
                break;
            case 'settings':
                $action = isset($url[1]) ? $url[1] : 'Preferences';
                $this->viewParams['title'] = 'Settings: ' . ucfirst($action);
                $this->viewParams['description'] = $this->language->get($url[0] . '/description');
                break;
            default:
                $this->viewParams['title'] = $this->language->get($url[0] . '/title');
                $this->viewParams['description'] = $this->language->get($url[0] . '/description');
                break;
        }

        return $this->page('common/header');
    }
}