<?php 

/**
 * Calendar Controller Class
 */
class CalendarController extends Controller 
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://tekcx.com/tracker/calendar
     * - http://tekcx.com/tracker/calendar/index
     */
    public function index() 
    {
        $this->viewParams['title'] = $this->language->get('calendar/title');
        $this->controller('common/header');
        $this->page('calendar/calendar');
        $this->controller('common/footer');
    }

}