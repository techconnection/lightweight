<?php 

/**
 * Projects Controller Class
 */
class ProjectsController extends Controller {
    /*
    public function getDailyProjectsTable() {
        $projects = $this->project_model->getRootProjects();
        ob_start();
        ?>
        <table>
            <tr>
                <th>Client</th><th>Description</th><th>Status</th><th>Type</th><th>Date range</th>
            </tr>
            
            <?php
            foreach ($projects as $project) {
                printDailyProjectRow($project,0,true);
            }
            ?>
            
        </table>
        <?php 
        return ob_get_clean();
    }

    public function cron() {
        $project_model = $this->project_model;
        $this->viewParams['title'] = 'Daily Tasks';
        
        $this->viewParams["dailyTable"] = $this->getDailyProjectsTable();
        $this->page('project-cron');
    }
    */

    public function newTask($projID) 
    {
        $this->index($projID);
    }

    protected function validateNewProject($parent_id) 
    {
        if (!empty($_POST['project_name'])) {
            $project_name = trim(preg_replace('/[^A-Za-z0-9?![:space:]]/', '', $_POST['project_name']));
            $this->project_model->quickInsert($project_name, $parent_id);
            if (!$this->project_model->project_inserted) {
                exit('Failed to create project');
            }
            return true;
        }
        return false;
    }
   
    public function active($parent_id = 0) 
    {
        if ($this->validateNewProject($parent_id) == true) $this->redirect('http://' . $_SERVER['SERVER_NAME'] . '/projects/active');
        
        $projects = $this->project_model->getRootProjects();
        $this->viewParams['projects'] = $projects;
        $this->viewParams['filter'] = 'active';
        $this->viewParams['command'] = 'archive';
        $this->viewParams['button'] = 'Archive';
        $this->viewParams['breadcrumb'] = 'Active';
        $this->controller('common/header');
        $this->page('projects/active');
        $this->controller('common/footer');
    }

    public function prospects()
    {
        $this->viewParams['projects'] = $this->project_model->getRootProjects(null, 'prospect');
        $this->viewParams['filter'] = 'prospect';
        $this->viewParams['command'] = 'archive';
        $this->viewParams['button'] = 'Archive';
        $this->viewParams['breadcrumb'] = 'Prospects';
        $this->controller('common/header');
        $this->page('projects/active');
        $this->controller('common/footer');
    }

    public function archived()
    {
        $this->viewParams['projects'] = $this->project_model->getRootProjects(null, 'archive');
        $this->viewParams['filter'] = 'archive';
        $this->viewParams['command'] = 'unarchive';
        $this->viewParams['button'] = 'Un-archive';
        $this->viewParams['breadcrumb'] = 'Archived';
        $this->controller('common/header');
        $this->page('projects/active');
        $this->controller('common/footer');
    }
    
    public function summary($projID) 
    {
        $project_model = $this->project_model;
        $projRecord = $project_model->get($projID);
        $clientRecord = $this->client_model->find($projRecord->clientid);
        $this->viewParams['projResourcesCount'] = $project_model->assignedResourcesCount($projID);
        $this->viewParams['projChildrenCount'] = getChildCount($projID);
        $this->viewParams['projDocumentCount'] = getDocumentCount($projID);
        $this->viewParams['projID'] = $projID;
        $this->viewParams['taskList'] = displayTaskList($projID);
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID);
        $this->viewParams['clientname'] = "";
        $this->viewParams['first'] = "";
        $this->viewParams['last'] = "";
        $this->viewParams['street'] = "";
        $this->viewParams['phone1'] = "";
        if ($clientRecord != null) {
            $this->viewParams['clientname'] = $this->client_model->getClientName($clientRecord->id);
            $this->viewParams['first'] = $clientRecord->first;
            $this->viewParams['last'] = $clientRecord->last;
            $this->viewParams['street'] = $clientRecord->street;
            $this->viewParams['phone1'] = $clientRecord->phone1;
            $loc = App::places()->find($projRecord->location);
            if($loc != null) {
                $this->viewParams['places'] = array($loc);
            }
        }
        $this->extractRecord($projRecord);
        
        $this->controller('common/header');
        $this->page('projects/summary');
        $this->controller('common/footer');

    }
    
    function taskArray($projID, $indent = 0) 
    {
        $tasks = [];
        $projects = App::tickets()->where("parent_id", $projID)->get();
        if (!empty($projects)) {
            foreach($projects as $projRecord) {
                $tasks[] = array("text" => $projRecord->projectname);
            }
        }
        return $tasks;
    }   
    
    public function slackpost($projID) 
    {
        $project_model = $this->project_model;
        $projRecord = $project_model->get($projID);
        $clientRecord = $this->client_model->find($projRecord->clientid);
        $this->viewParams['projResourcesCount'] = $project_model->assignedResourcesCount($projID);
        $this->viewParams['projChildrenCount'] = getChildCount($projID);
        $this->viewParams['projDocumentCount'] = getDocumentCount($projID);
        $this->viewParams['projID'] = $projID;
        $this->viewParams['taskList'] = displayTaskList($projID);
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID);
        $this->viewParams['clientname'] = "";
        $this->viewParams['first'] = "";
        $this->viewParams['last'] = "";
        $this->viewParams['street'] = "";
        $this->viewParams['phone1'] = "";
        if ($clientRecord != null) {
            $this->viewParams['clientname'] = $this->client_model->getClientName($clientRecord->id);
            $this->viewParams['first'] = $clientRecord->first;
            $this->viewParams['last'] = $clientRecord->last;
            $this->viewParams['street'] = $clientRecord->street;
            $this->viewParams['phone1'] = $clientRecord->phone1;
            $loc = App::places()->find($projRecord->location);
            if($loc != null) {
                $this->viewParams['places'] = array($loc);
            }
        }
        $this->extractRecord($projRecord);
        $newcontent = str_replace("</p>", "\n\r", $projRecord->scope);
        $newcontent = str_replace("&#39;", '\'', $newcontent);
        $newcontent = str_replace("&quot;", '\'', $newcontent);
        $newcontent = str_replace("<p>", "", $newcontent);
        $newcontent = str_replace("<b>", "*", $newcontent);
        $newcontent = str_replace("</b>", "*", $newcontent);
        $newcontent = str_replace("<strong>", "*", $newcontent);
        $newcontent = str_replace("</strong>", "*", $newcontent);
        $newcontent = str_replace("&nbsp;", " ", $newcontent);
        $this->slack($projRecord->slack, $newcontent, $this->taskArray($projID));
        $this->controller('common/header');
        $this->page('projects/summary');
        $this->controller('common/footer');

    }
    
    
    public function workOrder($projID) 
    {
        $project_model = $this->project_model;
        $projRecord = $project_model->get($projID);
        $clientRecord = $this->client_model->find($projRecord->clientid);
        $this->viewParams['projResourcesCount'] = $project_model->assignedResourcesCount($projID);
        $this->viewParams['projChildrenCount'] = getChildCount($projID);
        $this->viewParams['projDocumentCount'] = getDocumentCount($projID);
        $this->viewParams['projID'] = $projID;
        $this->viewParams['taskList'] = App::tickets()->where("parent_id", $projID)->where("status","Open")->get();
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID);
        if ($clientRecord != null) {
            $this->viewParams['clientname'] = $this->client_model->getClientName($clientRecord->id);
            $this->viewParams['first'] = $clientRecord->first;
            $this->viewParams['last'] = $clientRecord->last;
            $this->viewParams['street'] = $clientRecord->street;
            $this->viewParams['phone1'] = $clientRecord->phone1;
            $loc = App::places()->find($projRecord->location);
            if($loc != null) {
                $this->viewParams['places'] = array($loc);
            }
        }
        $this->extractRecord($projRecord);
        
        //$this->controller('common/header');
        $this->page('projects/workOrder');

    }
    
    
    public function detail($projID) 
    {
        $project_model = $this->project_model;
        $project = $project_model->get($projID);
        $this->viewParams['projID'] = $projID;
        $client_model = $this->client_model;
        
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID);
        if ($project->clientid == 0) {
            $clientName = "None assigned";
        } else {
            $client = $client_model->find($project->clientid);
            if ($client == null) {
                $clientName = "ID ".$project->clientid." not found.";
            } else {
                $clientName = $client->company;
                $loc = App::places()->find($project->location);
                if($loc != null) {
                    $project->append("locationName",$loc->name);
                }
            }
        }
        $this->extractRecord($project);
        $this->viewParams['clientName'] = $clientName;
        $people_model = $this->model("people");
        $this->viewParams['people'] = $people_model->getPeopleForID($project->clientid);
        $places_model = $this->model("places");
        $this->viewParams['places'] = $places_model->getPlacesForID($project->clientid);
        $this->controller('common/header');
        $this->page('projects/detail');
        $this->controller('common/footer');
    }
    
    public function resources($projID) 
    {
        $project_model = $this->project_model;
        $project = $project_model->get($projID);
        $this->viewParams['projID'] = $projID;
        $this->viewParams['projName'] = $project->projectname;
        $this->viewParams['projStatus'] = $project->status;
        $this->viewParams['projType'] = $project->type;
        $this->viewParams['projScope'] = $project->scope;
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID);
        $this->viewParams['resourcesSelect'] = displayResourcesSELECT($projID);
        $this->viewParams['assignedResourcesList'] = displayAssignedResourcesList($projID);
        $this->controller('common/header');
        $this->page('projects/resources');
        $this->controller('common/footer');
    }
    
    
    public function tasks($projID) 
    {
        $project_model = $this->project_model;
        $project = $project_model->get($projID);
        $this->viewParams['projID'] = $projID;
        $this->viewParams['projName'] = $project->projectname;
        $this->viewParams['projStatus'] = $project->status;
        $this->viewParams['projType'] = $project->type;
        $this->viewParams['projScope'] = $project->scope;
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID);
        $this->viewParams['tasks'] = displayTaskList($projID);
        $this->controller('common/header');
        $this->page('projects/tasks');
        $this->controller('common/footer');
    }
    
    
    public function files($projID) 
    {
        $project_model = $this->project_model;
        $project = $project_model->get($projID);
        $this->viewParams['projID'] = $projID;
        $this->viewParams['projName'] = $project->projectname;
        $this->viewParams['projStatus'] = $project->status;
        $this->viewParams['projType'] = $project->type;
        $this->viewParams['projScope'] = $project->scope;
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID);
        $this->viewParams['documents'] = displayDocuments($projID);
        $this->controller('common/header');
        $this->page('projects/files');
        $this->controller('common/footer');
    }
    

    public function upload($projID)
    {
        $this->viewParams['projID'] = $projID;
        $this->viewParams['breadcrumbs'] = displayBreadCrumbs($projID, true);
        $this->controller('common/header');
        $this->page('projects/uploader');
        $this->controller('common/footer');

    }
       
    public function receive($projID) 
    {
        $project_model = $this->project_model;
        $project = $project_model->get($projID);
        if (empty($_FILES) || $_FILES["file"]["error"]) {
          exit(FAILURE);
        }
        
        $fileName = $_FILES["file"]["name"];
        $tmpName = $_FILES["file"]["tmp_name"];
        $ondiskName = hash("sha256",$fileName.$tmpName);
        move_uploaded_file($tmpName, UPLOADS_DIR . '/' . $ondiskName);
        
        $docRec = new DB_Record(["projectid" => $projID, "nameondisk" => $ondiskName, "documentname" => $fileName, "account_id" => $_SESSION['account_id']]);
        App::documents()->insert($docRec);
        
        exit(SUCCESS);
    }
    
}