<?php

/***************************
 *
 *  Testing is my very simple unit test file.  I tend to comment out old tests and leave them behind for
 *  getting back into the groove of how methods and functions were expected to be used.
 *
 *  In any program under development, make a separate test file like this one that does a bunch of simple
 *  calls to your classes before you put them into live use, and test everything from the simple cases
 *  all the way to using every function all at once and anything in between.  You'll catch a lot of
 *  crazy behavior before it ends up in the user interface (where you'll test some more) and here
 *  there won't be any css or javascript or page refreshes or who knows what to get in the way of your
 *  output.
 *
 *
 */

class NewTicketModel extends DB_Schema {
	
	public function __construct() {
		parent::__construct();
		$this->table = "tickets";
		$this->primaryKey = "id";
		$this->init();
		$this->DB->constrain("account_id",$_SESSION['account_id']);
	}
	
}

class ticketFormat {
    public $displayOrder = ["startDate" => 1, "endDate" => 2, "type" => 3];
	public $formTypes = [];
	public $linkFormats = [];
	public $linkables = [];
    
    public function __construct() {
        $this->formTypes["id"]["type"]="hidden";
		$this->formTypes["account_id"]["type"]="ignore";
        $this->formTypes["parent_id"]["type"]="ignore";
		
		$this->linkables["projectname"] = true;
		$this->linkFormats["projectname"] = "/projects/detail/%id%";
        
        $this->linkables["company"] = true;
		$this->linkFormats["company"] = "/clients/detail/%clientid%";
    }
}

 
 
class TestController extends Controller {
    
    
    public function index()
    {
		
		//test of the file browser
        //$this->controller('common/header');
        //$this->page('test/test');

        echo "Starting my run:\n";
		
		//check curl availability
		
		$curl_handle=curl_init();
		curl_setopt($curl_handle,CURLOPT_URL,'http://google.com');
		curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
		curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, true);
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);
		
		if (empty($buffer))
		{
			print "Sorry, google.com are a bunch of poopy-heads.<p>";
		}
		else
		{
			print $buffer;
		}
		
		/******************************************************************************************************/
		//This is a test to check on slack API access
		//echo $this->slack("random","A random thought.");
		//echo $this->slackFile("betatest","/application/storage/images/doc.png");
		
		/******************************************************************************************************/
		//this is a test to figure out if we have correctly determined who is assigned to what projects
		//$ticket = 85;
//		$ticket = App::tickets()->find(85);
//		$assignments = App::resource_assignment()
//                         ->where("ticket_id",$ticket->id)
//                         ->get();
//		//we now have whatever was assigned to this ticket, be it vehicles, and so on.
//		//zero in on staff only
//		echo "Staff assigned to: ".$ticket->projectname;
//		foreach($assignments as $assignment){
//			$resource = App::resources()->where("id",$assignment->resource_id)
//									->where("Type","Staff")
//									->getFirst();
//			if($resource) {
//				echo $resource->name."<br />"; //this should only list staff members assigned to this ticket
//			}
//        }
		/******************************************************************************************************/

		/******************************************************************************************************/
		// //test for mobile browser detection
		// echo $this->isMobile;
		
		// if ($this->isMobile) {
		// 	echo "Mobile!";
		// } else {
		// 	echo "Not mobile!";
		// }
		/******************************************************************************************************/
		
		/******************************************************************************************************/
		//record counting tests
		/*
		$client = null;
		$state = "active";
        $db = App::tickets()->select("tickets.*, clients.company")
                ->where("parent_id", 0)
                ->where("type","project")
                ->where("tickets.state",$state)
                ->leftJoin("clients","tickets.clientid","clients.id")->orderBy("clients.company");
        if($client!=null) {
            $db->where("clientid",$client);
        }   
        echo "I see ".$db->count()." records";
        */
        /******************************************************************************************************/
		
        /************************************
         *
         *  This section is to work out mySql JOIN logic
         *
         */
        
        //okay, so tickets has a clientid field that is a foreign key to clients table.  Most of the time when we select * from tickets
        //we will want the client's name, but all we actually get back is a clientid.  So that leads us to make a function that gets
        //the client's name from the clients table as a separate database call.
        //
        //However, we can do it in one pass by using a join, so for instance:
        /*
         *
         *  SELECT tickets.*, clients.company FROM tickets LEFT JOIN clients on tickets.clientid = clients.id
         *
         *  This returns all the ticket fields and also includes a company field if there was a match.
         *
         */
        //test 1
        //$s = "SELECT tickets.*, clients.company FROM tickets JOIN clients on tickets.clientid = clients.id";
        //$results = $db->db->query($s);
        //while ($row = $results->fetch_assoc()) {
        //    var_dump($row);
        //}
        /*
         *  This presents a problem if sent back to us as DB_Records, because we cannot send a DB_Record containing a field
         *  called company back to the database for updating or inserting.  To address this issue, I made a class called
         *  DB_Schema, and when called it checks the actual database column names during insert and update, and excludes
         *  anything not matching.
         *
         */
        //test 2
//		$this->view('head');
//        $db = DB::table('tickets');
//        $db->select("tickets.*, clients.company")->leftJoin('clients',"tickets.clientid","clients.id");
//        $db->orderBy("clientid, id");
//        $records = $db->getFirst();
//        $displayer = new DB_Displayer();
//        $displayer->setFormat(new ticketFormat());
//        echo $displayer->template($records,"config-table");
        /******************************************************************************************************/
        
        /***********************
         *
         *  These are the tests for the DB_Schema class
         */
        //test1
       // $console = new Log_Model();
        //var_dump($client_model->columns); //let's confirm we see the column names
        //test2
        //$pretendPOST = ["command" => "updateClient", "id" => "20", "company" => "Beta Testers", "client-name" => "Computed Form Name", "phone1" => "(808)-230-1327"];
        //var_dump($client_model->conform($pretendPOST)); //let's confirm we see only valid columns
        //test3
        //generate a quick form to edit this table
        //var_dump($client_model->formTypes);
        //$console->insert(["description" => "Created a new %t"]);
        //$console->tableElements();
        
        /******************************************************************************************************/
		
        /***********************
         *  These are the original tests for the DB class to be sure it did what it was supposed to.
         */
        //$db = DB::table('tickets');
        //$db->where("status","Open");
        //$records = App::users()->find(1);
        //  testing the new DB class methods
        ////example 1
        //echo "<pre>\nexample1--------------\n";
        //$records = DB::table("tickets")->get();
        //foreach($records as $project) {
            //var_dump($project);
        //    echo "\nName:".$project->projectname;
        //    echo "\nID:".$project->id;
        //    echo "\nStatus:".$project->status;
        //    echo "\nParent:".$project->parent_id;
        //    echo "\nAccount:".$project->account_id;
        //    echo "\n";
       // }
        //
        //
        ////example 2
        //echo "\nexample2--------------\n";
        //$records = DB::table('tickets')
        //    ->select("id, projectname")
        //    ->whereNot("id",1)
        //    ->whereNot("parent_id",0)
        //    ->get();
        //foreach($records as $project) {
        //    echo $project->projectname;
        //    echo $project->id;
        //    echo "\n";
        //}
        //
        //
        ////example 3
        //echo "\nexample3--------------------\n";
        ////
        //$attribs = [
        //        "projectname" => "Inserted via new DB class",
        //        "status" => "Open",
        //        "parent_id" => 0
        //        ];
        //$record = new DB_Record($attribs);
        //echo $record->projectname;
        ////echo $record->id; //does not exist until loaded from db
        //echo $record->parent_id;
        //echo $record->status;
        //echo "\n";
        //if($db->insert($record)) {
        //    echo "Insert worked okay, got auto-incremented id: ".$db->insert_id;
        //} else {
        //    echo "Insert did not go well :(";
        //}
        //
        //
        ////example 4
        //echo "\nexample 4--------------------\n";
        //$record1 = new DB_Record(["projectname" => "Inserted1",]);
        //$record2 = new DB_Record(["projectname" => "Inserted2",]);
        //$record3 = new DB_Record(["projectname" => "Inserted3",]);
        //echo "\n";
        ////if(DB::table("tickets")->insert([$record1,$record2,$record3])) {
        ////    echo "Insert worked okay.";
        ////} else {
        ////    echo "Insert did not go well :(";
        ////}
        //
        ////example 5
        //echo "\nexample5--------------\n";
        //$db = DB::table('tickets')
        //    ->select("id, projectname")
        //    ->where("id",1);
        //$records = $db->get();
        //foreach($records as $project) {
        //    $project->projectname = $project->projectname." (Updated)";
        //    echo $project->projectname;
        //    echo "\n";
        //    //DB::table('tickets')->update($project);
        //}
        //
        //echo "\ntest6--------------\n";
        //$db = DB::table('tickets')
        //    ->select("id, projectname")
        //    ->whereNot("id",1)
        //    ->orWhere("id",2);
        //$db->buildQuerySelectString();
        //echo $db->query;
        //
        //echo "\ntest7--------------\n";
        //$db = DB::table('tickets')
        //    ->where("status","Open");
        ////if($db->massUpdate(["status" => "Closed"])){
        ////    echo "mass update worked.";
        ////} else {
        ////    echo "mass update failed.";
        ////}
        //$db = DB::table('tickets');
        ////if($db->massUpdate(["status" => "Open"])){
        ////    echo "mass update worked.";
        ////} else {
        ////    echo "mass update failed.";
        ////}
        //$db = DB::table('tickets');
        //$db->where("status","Deactivated")->delete();
        
    }

    public function upload()
    {
        $upload_dir = $_POST['upload_dir'];

        $upload_library = $this->library('Upload');
        $upload_library->uploadImage($_FILES['upload_image'], $upload_dir);

        if ($upload_library->file_invalid) {
            exit($this->language->get('account/file_invalid'));
        }
        if ($upload_library->filebig) {
            exit($this->language->get('account/file_big'));
        }

        if ($upload_library->upload_success) {
            exit($this->language->get('pages/file_uploaded')); 
        } else {
            exit('fak');
        }
    }
    
}