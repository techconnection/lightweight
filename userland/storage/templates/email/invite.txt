<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
        <title>User Invitation</title>
    </head>
    <body>
        <div style="display:block; width:800px; margin:auto; text-align:center;">
            <a href="http://yourtechconnection.com">ProjectTracker</a>
            <div>
                <div>You are invited to Project Tracker.  Click the link below to activate your account.</div> <br>
                {{$activation_link}}
                <div>If the link does not open copy and paste it to the url in your browser.</div> <br>
            </div>
        </div>
    </body>
</html>