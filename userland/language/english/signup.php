<?php 

/**
 * Title
 */
$_['title'] = 'Signup';

/**
 * Description
 */
$_['description'] = 'This is the signup page description and it is about 160 characters long, which is super important for seo or (search engine optimization). Try to keep it so.';

/**
 * Alerts
 */
$_['username_taken'] = '<div class="alert error"><strong>Error!</strong> The username you have chosen is already taken. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['username_invalid'] = '<div class="alert error"><strong>Error!</strong> Usernames must be letters A-Z and no more than 20 characters long. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['firstname_invalid'] = '<div class="alert error"><strong>Error!</strong> Names must be letters A-Z and no more than 20 characters long. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['lastname_invalid'] = '<div class="alert error"><strong>Error!</strong> Names must be letters A-Z and no more than 20 characters long. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['email_taken'] = '<div class="alert error"><strong>Error!</strong> The email address you have chosen is already taken. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['email_invalid'] = '<div class="alert error"><strong>Error!</strong> The email address you entered was invalid.</div>';
$_['pw_weak'] = '<div class="alert error"><strong>Error!</strong> The password is too weak. <br><b>Passwords must:</b> <br>- Have at least 1 number. <br>- Have at least 1 upper case letter. <br>- Be 8 or more characters. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['pw_match'] = '<div class="alert error"><strong>Error!</strong> The passwords you entered did not match. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['signup_success'] = '<div class="alert success"><strong>Success!</strong> Your account has been created and an activation email has been sent to you. (<a href="/login">Continue?</a>)</div>';
$_['signup_fail'] = '<div class="alert error">Unable to create your account.</div>';
$_['user_banned'] = '<div class="alert error"><strong>Error!</strong> This account has been permanently banned.</div>';
$_['phone_invalid'] = '<div class="alert error"><strong>Error!</strong> The Phone number you entered was invalid.</div>';