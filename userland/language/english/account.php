<?php 

/**
 * Title
 */
$_['title'] = 'Account';

/**
 * Description
 */
$_['description'] = 'This is the account page description and it is about 160 characters long, which is super important for seo or (search engine optimization). Try to keep it so.';

/**
 * Alerts
 */
$_['account_updated'] = '<div class="alert success"><strong>Success!</strong> Your account has been updated. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['username_taken'] = '<div class="alert error"><strong>Error!</strong> The username you have chosen is already taken. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['username_invalid'] = '<div class="alert error"><strong>Error!</strong> Usernames must be letters A-Z and no more than 20 characters long. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['name_invalid'] = '<div class="alert error"><strong>Error!</strong> Names should be letters A-Z and no more than 20 characters. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['email_taken'] = '<div class="alert error"><strong>Error!</strong> The email address you have chosen is already taken. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['email_invalid'] = '<div class="alert error"><strong>Error!</strong> The email address you entered was invalid.</div>';
$_['pw_weak'] = '<div class="alert error"><strong>Error!</strong> The password is too weak. <br><b>Passwords must:</b> <br>- Have at least 1 number. <br>- Have at least 1 upper case letter. <br>- Be 8 or more characters. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['pw_match'] = '<div class="alert error"><strong>Error!</strong> The passwords you entered did not match. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['pw_changed'] = '<div class="alert success"><strong>Success!</strong> Your password has been changed. You will be logged out in 5 seconds.</div><script type="text/javascript">setTimeout(function () {window.location.href="/home";},5000);</script>';
// Activate
$_['activate_mail_sent'] = '<div class="alert success"><strong>Success!</strong> An activation email has been sent to your inbox. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['activate_mail_fail'] = '<div class="alert error">The activation email could not be sent.</div>';
$_['key_invalid'] = '<div class="alert error"><strong>Error!</strong> The activation link appears to be not legitimate or expired. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['activation_success'] = '<div class="alert success"><strong>Success!</strong> Your account has been activated. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['activation_fail'] = '<div class="alert success">The account could not be activated.</div>';
// Password Recovery
$_['recovery_not_sent'] = '<div class="alert error">Error, the recovery link was not sent.</div>';
$_['recovery_sent'] = '<div class="alert success"><strong>Success!</strong> A recovery link has been sent you.</div>';
$_['link_invalid'] = '<div class="alert error"><strong>Error!</strong> The recovery link appears to be not legitimate or expired. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
// File Uploads
$_['file_invalid'] = '<div class="alert error"><strong>Error!</strong> File denied. Excepted files types for avatars are: jpg | jpeg | png | gif. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['file_big'] = '<div class="alert error"><strong>Error!</strong> File is too big ({{filesize}}MB). Max filesize: {{maxFilesize}}MB. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['uploaded_failed'] = '<div class="alert error"><strong>Error!</strong> The file could not be uploaded. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';