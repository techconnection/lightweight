<?php 

/**
 * Title
 */
$_['title'] = 'Settings';

/**
 * Alerts
 */
$_['activation_sent'] = '<div class="alert success"><strong>Success!</strong> The activation email has been sent to ({{email}}). (<a href="/settings">Continue?</a>)</div>';
$_['activation_not_sent'] = '<div class="alert success"><strong>Success!</strong> The activation email could not be sent. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';