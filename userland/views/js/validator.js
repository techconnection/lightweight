
/**
 * Validator
 *
 * The validator plugin validates user forms. It checks basic inputs like 
 * email and passwords. Using the options you can choose to check only
 * the inputs you want by setting the option to true.
 *
 * Example
 *     $('#contact-form').validator({
 *         firstnameValid : false,
 *         formButton : '#login-button'
 *     }); 
 */
(function($) {
    $.fn.validator = function(options) {

        var settings = $.extend({
            checkUsername : true,
            checkUsernameTaken : true,
            checkFirstname : true,
            checkLastname : true,
            checkEmail : true,
            checkEmailTaken : true,
            checkPhone : true,
            checkSubject : true,
            checkPassword : true,
            showPassword : true,
            formButton : '.btn'
        }, options);

        var action = {
            checkUsernameValidity: function(value) {
                if (settings.checkUsername == true) {
                    var username = $.trim(value.replace(/\s+/g, ''));
                    if (username.length > 0 && /^[a-zA-Z0-9]{1,20}$/.test(username) == false) {
                        $('.username .alert-inline').remove();
                        $('.username').append('<div class="alert-inline">Usernames should be A-Z 0-9 and 20 characters or less.</div>');
                        $('.username input').css({'border-color':'#f15454'});
                    } else {
                        $('.username .alert-inline').remove();
                        $('.username input').css({'border-color':''});
                    }
                }
            },
            checkUsernameAvailability: function(value) {
                if (settings.checkUsernameTaken == true) {
                    var username = $.trim(value.toUpperCase().replace(/\s+/g, ''));
                    $.post('/account/usernameLiveCheck', $('form').serialize()).done(function(data) {
                        if (username.length > 0 && username == data) {
                            $('.username .alert-inline').remove();
                            $('.username').append('<div class="alert-inline">Username taken.</div>');
                            $('.username').css({'border-color':'#f15454'});
                        }
                    });
                }
            },
            checkFirstnameValidity: function(firstname) {
                if (settings.checkFirstname == true) {
                    if (firstname.length > 0 && /^[a-zA-Z]{1,20}$/.test(firstname) == false) {
                        $('.firstname .alert-inline').remove();
                        $('.firstname').append('<div class="alert-inline">Names should be letters A-Z and no more than 20 characters.</div>');
                        $('.firstname input').css({'border-color':'#f15454'});
                    } else {
                        $('.firstname .alert-inline').remove();
                        $('.firstname input').css({'border-color':''});
                    }
                }
            },
            checkLastnameValidity: function(lastname) {
                if (settings.checkLastname == true) {
                    if (lastname.length > 0 && /^[a-zA-Z]{1,20}$/.test(lastname) == false) {
                        $('.lastname .alert-inline').remove();
                        $('.lastname').append('<div class="alert-inline">Names should be letters A-Z and no more than 20 characters.</div>');
                        $('.lastname input').css({'border-color':'#f15454'});
                    } else {
                        $('.lastname .alert-inline').remove();
                        $('.lastname input').css({'border-color':''});
                    }
                }
            },
            checkEmailValidity: function(email) {
                if (settings.checkEmail == true) {
                    if (email != '' && /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i.test(email) == false) {
                        $('.email .alert-inline').remove();
                        $('.email').append('<div class="alert-inline">Email address is invalid.</div>')
                        $('.email input').css({'border-color':'#f15454'});
                    } else {
                        $('.email .alert-inline').remove();
                        $('.email input').css({'border-color':''});
                    }
                }
            },
            checkEmailAvailability: function(email) {
                if (settings.checkEmailTaken == true) {
                    $.post('/account/emailLiveCheck', $('form').serialize()).done(function(data) {   
                        if (email.length > 5 && email == data) {
                            $('.email .alert-inline').remove();
                            $('.email').css({'border-color':'#f15454'}).append('<div class="alert-inline">Email taken.</div>');
                        }
                    });
                }
            },
            checkPhoneNumber: function(phonenumber) {
                if (settings.checkPhone == true) {
                    /*if (phonenumber.length > 0 && $.isNumeric(phonenumber) == false) {
                        $('.phone .alert-inline').remove();
                        $('.phone').css({'border-color':'#f15454'}).append('<div class="alert-inline">Phone numbers should contain only numeric characters.</div>');
                    } else {
                        $('.phone').removeAttr('style');
                        $('.phone .alert-inline').remove();
                    }*/  
                        
                    if (phonenumber.length > 2 && phonenumber.length < 4) {
                        $('#phone').val(phonenumber + '-');
                    }
                    if (phonenumber.length > 6 && phonenumber.length < 10) {
                        $('#phone').val(phonenumber + '-');
                    }

                }
            },
            checkSubjectValidity: function(subject) {
                if (settings.checkSubject == true) {
                    if (subject.length > 0 && /^[a-zA-Z0-9]{1,20}$/.test(subject) == false) {
                        $('.subject .alert-inline').remove();
                        $('.subject').css({'border':'1px solid #f15454'}).append('<div class="alert-inline">Subject lines should not include any special characters.</div>');
                    } else {
                        $('.subject').removeAttr('style');
                        $('.subject .alert-inline').remove();
                    }
                }
            },
            checkPasswordStrength: function(password) {
                if (settings.checkPassword == true) {
                    if (password.length > 0) {
                        if (/^(?=.*\d)(?=.*[a-z]).{8,}$/.test(password) == false) {
                            $('.password-strength').remove();
                            $('.password').append('<div class="password-strength"><i>WEAK</i></div>').css({'color':'#f15454'});
                        }
                        if (/^(?=.*\d)(?=.*[a-z]).{8,}$/.test(password) == true) {
                            $('.password-strength').remove();
                            $('.password').append('<div class="password-strength"><i>MEDIUM</i></div>').css({'color':'rgb(237, 234, 3)'});
                        }
                        if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password) == true) {
                            $('.password-strength').remove();
                            $('.password').append('<div class="password-strength"><i>STRONG</i></div>').css({'color':'rgb(90, 160, 85)'});
                        }
                        if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*=_+<>?]).{8,}$/.test(password) == true) {
                            $('.password-strength').remove();
                            $('.password').append('<div class="password-strength"><i>VERY STRONG</i></div>').css({'color':'rgb(90, 160, 85)'});
                        }
                    } else {
                        $('.password-strength').remove();
                    }
                }
            },
            checkPasswordMatch: function(password, confirm) {
                if (password && confirm) {
                    if (password != confirm) {
                        $('.confirm .alert-inline').remove();
                        $('.confirm').css({'border-color':'#f15454'}).append('<div class="alert-inline">Passwords do not match.</div>');
                    } else {
                        $('.confirm').removeAttr('style');
                        $('.confirm .alert-inline').remove();
                    }
                }
            },
            revealPassword: function() {
                if (settings.showPassword == true) {
                    var password = document.getElementById('password');
                    var confirm = document.getElementById('confirm');
                    $('.fa-eye').toggleClass('fa-eye-slash');
                    if (password.type == 'password') {
                        password.type = 'text';
                        confirm.type = 'text';
                    } else if (password.type == 'text') {
                        password.type = 'password';
                        confirm.type = 'password';
                    }
                }
            },
            checkRequiredFields: function() {
                var complete = true;
                $('.required').each(function() {
                    if ($(this).val() == '') {
                        complete = false;
                    }
                    if (complete == true) {
                        $(settings.formButton).removeClass('disabled');
                    } else {
                        $(settings.formButton).addClass('disabled');
                    }
                });
            }
        }

        return this.each(function() {
            $('form').on('keyup change', '#username', function() {
                action.checkUsernameValidity($(this).val());
                action.checkUsernameAvailability($(this).val());
            });
            $('form').on('keyup change', '#firstname', function() {
                action.checkFirstnameValidity($(this).val());
            });
            $('form').on('keyup change', '#lastname', function() {
                action.checkLastnameValidity($(this).val());
            });
            $('form').on('change', '#email', function() {
                action.checkEmailValidity($(this).val());
                action.checkEmailAvailability($(this).val());
            });
            $('form').on('keydown keyup change', '#phone', function(e) {
                if (e.keyCode == 32) { 
                   return false;
                }
                action.checkPhoneNumber($(this).val());
            });
            $('form').on('keyup change', '#subject', function() {
                action.checkSubjectValidity($(this).val());
            });
            $('form').on('keyup change', '#password', function() {
                action.checkPasswordStrength($(this).val());
            });
            $('form').on('keyup change', '#password, #confirm', function() {
                action.checkPasswordMatch($('#password').val(), $('#confirm').val());
            });
            $('.show-pw').on('mousedown', function() {
                action.revealPassword();
            });
            $('form').on('keyup change', 'input, textarea', function() {
                action.checkRequiredFields();
            });
            $('form').on('click', '.disabled', function(e) {
                e.preventDefault();
                return false;
            });
        });
    }
}(jQuery));