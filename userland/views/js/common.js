
/**
 * Common Javascript 
 *
 * Common js used site wide. Most of this will be used on every page because
 * a lot of it is for the header and navigation stuff. Dropdown menus, to 
 * top buttons, ect.
 */
var Common = {

    // Drop menus
    dropMenusDown: function() {

        if (typeof localStorage.active_menu === 'undefined') {
            var active_menus = [];
        } else {
            var active_menus = JSON.parse(localStorage.active_menu);
        }

        $('.nav ul').on('click', '.dropdown-button', function() {
            $(this).next().toggleClass('active');
            $(':first-child', this).toggleClass('fa-caret-down, fa-caret-up');

            var dd_class = '.' + $(this).text().toLowerCase().trim();

            if ($.inArray(dd_class, active_menus) === -1) {
                active_menus.push(dd_class);
            } else {
                active_menus = $.grep(active_menus, function(value) {
                    return value != dd_class;
                });
            }
            
            localStorage.active_menu = JSON.stringify(active_menus);
        });
        $('.onclick-menu-wrapper').on('click', '.onclick-menu', function() {
            $('.onclick-menu-content').toggleClass('active');
        });
        $('.account-links').on('click', '.dropdown-button', function() {
            $('.account-nav .dropdown-menu').toggleClass('active');
        });
    },

    // Mobile navigation
    openMobileNav: function() {
        $('.header').on('click', '.menu-button', function() {
            $('.nav').toggleClass('active');
        });   
    },

    // Hightlight nav link
    highlightNavLink: function() {
        var pieces = window.location.pathname.split('/');
        var nav_class = '.nav-link.' + pieces[1];
        $(nav_class).addClass('current');
        if (pieces[2]) {
            $('.' + pieces[2]).addClass('current');
        }
    },

    search: function() {
        $('.search button').click(function() {
            window.location.replace('/search/' + $('#search-term').val());
        });
        $('.search').keypress(function(e) {
            if (e.which == 13) { //Enter key pressed
                $('.search button').click(); //Trigger search button click event
                return false;
            }
        });
    },

    showLoader: function(image) {
        var loader = '<div class="loading"><div class="loading-spinner"><img src="' + image + '" alt="Loading"></div></div>';
        return loader;
    }
}

$(document).ready(function() {

    Common.dropMenusDown();
    Common.openMobileNav();
    Common.highlightNavLink();
    Common.search();

    $('body').on('click', '.alert-continue', function() {
        window.location.reload();
    });
    $('body').on('click', '.alert-close', function() {
        $(this).parent().remove();
    });
    $('body').on('click', '.alert-redirect', function() {
        window.location.replace('/home');
    });
});