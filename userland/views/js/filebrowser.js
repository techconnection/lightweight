
/**
 * Filebrowser
 *
 * The filebrowser plugin allows you to browse file on the server.
 * Along the filebrowser php libraries it can create new folders 
 * and with the help of dropzone upload new files.
 * the inputs you want by setting the option to true.
 *
 * Example
 *     $('#elem').filebrowser({
 *         dir : '/root/path/dir/',
 *     }); 
 */
(function($) {

    $.fn.filebrowser = function(options) {

        var settings = $.extend({
            dir : null,
        }, options);

        var action = {

            setCurrentDir: function(dir) {
                localStorage.current_path = null;
                if (localStorage.current_path === 'null') {
                    localStorage.current_path = dir;
                }
            },

            getFiles: function(dir) {

                action.setCurrentDir(dir);

                $('.alert').remove();
                $('.filebrowser-dirs').html('');
                $('.filebrowser-files').html('');

                $('#new-folder-dir').val(dir);
                $('#upload-dir').val(dir);
                $('.filebrowser-footer').text(dir);

                $.ajax({
                    url: '/filebrowser/browse',
                    type: 'post',
                    data: { 'dir' : dir },
                    dateType: 'json',
                    beforeSend: function() {
                        $('.filebrowser').append('<div class="loading"><div class="loading-spinner"><img src="/application/storage/images/loading.svg" alt="Loading" class="col col-single"></div></div>');
                    },
                    success: function(response) {
                        $('.loading').remove();
                        if (typeof(response) !== 'undefined') {
                            var data = jQuery.parseJSON(response);
                            var dirs = data.dirs;
                            var dirs_num = 0;
                            var files = data.files;
                            var files_num = 0;
                            var current_path = data.current_path;
                            $(dirs).each(function() {
                                $('.filebrowser-dirs').append('<li class="filebrowser-item filebrowser-dir"><i class="fa fa-folder-o" aria-hidden="true"></i> <a href="#" class="filebrowser-dir-link"></a><button type="button" class="btn-delete"><i class="fa fa-trash" aria-hidden="true"></i></button></li>');
                            });
                            $('.filebrowser-dir-link').each(function() { 
                                $(this).text(dirs[dirs_num]); 
                                dirs_num++; 
                            });
                            $(files).each(function() {
                                $('.filebrowser-dirs').append('<li class="filebrowser-item filebrowser-file"><a href="#" class="filebrowser-file-link"></a><button type="button" class="btn-delete"><i class="fa fa-trash" aria-hidden="true"></i></button></li>');
                            });
                            $('.filebrowser-file-link').each(function() {
                                $(this).text(files[files_num]);
                                files_num++;
                            });
                            $('.filebrowser-file').each(function() {
                                var filename = $(this).text();
                                var extension = filename.substr( (filename.lastIndexOf('.') +1) );
                                var ext_ico = action.getExtension(extension);
                                $(this).prepend(ext_ico + ' ');
                            });
                        } 
                    }
                });
            },

            descend: function(path) {
                var foldername = $(path).text().trim();
                var dir = localStorage.current_path + foldername + '/';
                action.getFiles(dir);
                $('#new-folder-dir').val(dir + '/');
                $('#upload-dir').val(dir + '/');
                $('.btn-back').css({'display' : 'block'});
                dir = dir.split('/');
                dir = dir[dir.length -2];
                $('.filebrowser .title').text('File Browser: ' + dir);
            },

            ascend: function() {
                var current_path = localStorage.current_path;
                var dirs = current_path.split('/');
                var current_dir_name = dirs[dirs.length -2];
                var main_dir = settings.dir;
                $('.filebrowser .title').text('File Browser: ' + current_dir_name);
                dirs = dirs.slice(0, -2);
                dir = dirs.join('/');
                localStorage.current_path = dir + '/';
                action.getFiles(localStorage.current_path);
                $('#new-folder-dir').val(localStorage.current_path);
                $('#upload-dir').val(localStorage.current_path);
                if (localStorage.current_path == main_dir) {
                    $('.btn-back').css({'display' : 'none'});
                }
            },

            makeFolder: function() {
                var dir = $('#new-folder-dir').val();
                $.ajax({
                    url: '/filebrowser/makeFolder',
                    type: 'post',
                    data: $('#new-folder-form').serialize(),
                    beforeSend: function() {
                        $('.filebrowser').append('<div class="loading"><div class="loading-spinner"><img src="/application/storage/images/loading.svg" alt="Loading" class="col col-single"></div></div>');
                    },
                    success: function(response) {
                        if (typeof(response) !== 'undefined') {
                            $('.loading').remove();
                            action.getFiles(dir);
                            $('.filebrowser-fileview').prepend(response);
                        } 
                    }
                });
            },

            delete: function(dir, file) {
                $.ajax({
                    url: '/filebrowser/delete',
                    type: 'post',
                    data: { 'dir' : dir, 'file' : file },
                    dateType: 'json',
                    beforeSend: function() {
                        
                    },
                    success: function(response) {
                        if (typeof(response) !== 'undefined') {
                            action.getFiles(dir);
                            $('.filebrowser-fileview').prepend(response);
                        } 
                    }
                });
            },

            getExtension: function(extension) {
                switch (extension) {
                    case 'jpg':
                    case 'JPG':
                    case 'jpeg':
                    case 'JPEG':
                    case 'png':
                    case 'PNG':
                    case 'gif':
                    case 'GIF':
                        return '<i class="fa fa-picture-o" aria-hidden="true"></i>';
                    break;                         
                    case 'zip':
                    case 'rar':
                        return '<i class="fa fa-file-archive-o" aria-hidden="true"></i>';
                    break;
                    case 'pdf':
                        return '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>';
                    break;
                    case 'htm':
                    case 'html':
                        return '<i class="fa fa-html5" aria-hidden="true"></i>';
                    break;
                    case 'php':
                        return '<i class="fa fa-code" aria-hidden="true"></i>';
                    break;
                    case 'css':
                        return '<i class="fa fa-css3" aria-hidden="true"></i>';
                    break;
                    case 'xlsx':
                        return '<i class="fa fa-file-excel-o" aria-hidden="true"></i>';
                    break;
                    case 'docx':
                        return '<i class="fa fa-file-word-o" aria-hidden="true"></i>';
                    break;
                    default:
                        return '<i class="fa fa-question-circle-o" aria-hidden="true"></i>';
                }
            }
        }

        return this.each(function() {

            $(this).append('<div class="filebrowser"><div class="filebrowser-titlebar"><span class="title">File Browser</span> <button type="button" class="btn btn-close pull-right"><i class="fa fa-times" aria-hidden="true"></i></button></div><div class="filebrowser-actions"><button type="button" class="btn btn-go btn-new-folder" title="New Folder"><i class="fa fa-plus" aria-hidden="true"></i></button><button type="button" class="btn btn-default btn-upload" title="Upload"><i class="fa fa-upload" aria-hidden="true"></i></button><button type="button" class="btn btn-default btn-back" title="Back"><i class="fa fa-reply" aria-hidden="true"></i></button><button type="button" class="btn btn-default btn-refresh" title="Refresh"><i class="fa fa-refresh" aria-hidden="true"></i></button></div><div class="filebrowser-fileview"><ul class="list-unstyled filebrowser-dirs"></ul><ul class="list-unstyled filebrowser-files"></ul></div><div class="filebrowser-footer"></div><div class="filebrowser-upload"><button type="button" class="btn btn-close-upload"><i class="fa fa-times" aria-hidden="true"></i></button><form action="" class="dropzone needsclick dz-clickable" id="dropzone"><input type="hidden" name="upload_dir" value="" id="upload-dir"></form></div><div class="filebrowser-new-folder"><form enctype="multipart/form-data" id="new-folder-form"><div class="row form-row"><label for="foldername">New folder</label><div class="row"><input type="hidden" name="new_folder_dir" value="" id="new-folder-dir"><input type="text" name="foldername" class="new-folder-input"><button type="button" class="btn-go btn-make-folder"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button></div></div></form></div></div>');

            $('body').on('click', '.btn-browse', function() {

                $('.filebrowser').css({'display': 'block'});
                action.getFiles(settings.dir);

            });

            $('.filebrowser').on('click', '.btn-close', function() { 
                $('.filebrowser').css({'display': 'none'}); 
            });

            $('.filebrowser').on('click', '.btn-refresh', function() { 
                $('.filebrowser-new-folder').removeClass('open');
                action.getFiles(localStorage.current_path); 
            });

            $('.filebrowser').on('click', '.btn-new-folder', function() { 
                $('.filebrowser-new-folder').toggleClass('open'); 
            });

            $('.filebrowser-new-folder').on('click', '.btn-make-folder', function() {
                action.makeFolder();
            });

            $('#new-folder-form').keypress(function(e) {
                if (e.which == 13) {
                    $('.btn-make-folder').click();
                    return false;
                }
            });

            $('.filebrowser').on('click', '.btn-upload', function() {
                $('.filebrowser-upload').toggleClass('open');
            });

            $('.filebrowser-upload').on('click', '.btn-close-upload', function() {
                $('.filebrowser-upload').removeClass('open');
                action.getFiles(localStorage.current_path); 
            });

            $('.filebrowser').on('click', '.filebrowser-dir-link', function(e) {
                e.preventDefault();
                action.descend($(this));
            });

            $('.btn-back').click(function() {
                action.ascend();
            });

            $('.filebrowser').on('click', '.btn-delete', function() {
                var path = localStorage.current_path;
                var parent = $(this).parent().get(0);
                var file = $(parent).text();
                action.delete(path, file);
            });

        });
    }
}(jQuery));