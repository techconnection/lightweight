
function postForm(formName) {
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
    var queryString = $(formName).serialize();
    $.post("/ajax/",queryString,function(data){
                    var result = $.parseJSON(data);
                    if(result.OK == true) {
                        $("#result").html("Saved");
                    } else {
                        $("#result").html("Error!");
                    }
    });
}


function refreshClientTable() {
    var queryString = "command=clientTable";
    $("#clientTable").delay(500);
    $.post("/ajax/",queryString,
            function(data){
                $("#clientTable").html(data);
            });
}

function postNewTask() {
    var queryString = $("#new-task-form").serialize();
    $.post("/ajax/",queryString,
            function(data){
                $("#result").html(data);
            });
    $("#new-task-form")[0].reset();
}

function postNewPerson() {
    var queryString = $("#new-person-form").serialize();
    $.post("/ajax/",queryString,
            function(data){
                $("#result").html(data);
            });
    $("#new-person-form")[0].reset();
}

function deletePerson() {
    var queryString = $("#people-form").serialize();
    $.post("/ajax/",queryString,
            function(data){
                $("#result").html(data);
            });
    $("#people-form")[0].reset();
}

function postNewPlace() {
    var queryString = $("#new-place-form").serialize();
    $.post("/ajax/",queryString,
            function(data){
                $("#result").html(data);
            });
    $("#new-place-form")[0].reset();
}

function deletePlace() {
    var queryString = $("#places-form").serialize();
    $.post("/ajax/",queryString,
            function(data){
                $("#result").html(data);
            });
    $("#places-form")[0].reset();
}

function archiveButton() {
    var queryString = $("#proj-form").serialize();
    $.post("/ajax/",queryString,
            function(data){
                $("#result").html(data);
            });
    $("#proj-form")[0].reset();
}

function postNewResource() {
    if ($('#name').val().trim().length < 1) {
        alert("Name invalid!");
    } else {
        var queryString = $("#new-resource-form").serialize();
        $.post("/ajax/", queryString, function(data) {
            $("#result").html(data);
        });
        $("#new-resource-form")[0].reset();
    }
}

function assignResource() {
    var queryString = $("#assign-resource-form").serialize();
    $("#resource_id option:selected").attr('disabled','disabled')
    $.post("/ajax/",queryString,
            function(data){
                $("#result").html(data);
            });
    $("#assign-resource-form")[0].reset();
}

function unassignResource(resID,assignmentID,ticketID) {
    var queryString = "command=unassignResource&ticket_id="+ticketID+"&id="+assignmentID;
    $('#resource_id option[value="'+resID+'"]').removeAttr('disabled')
    $.post("/ajax/",queryString,
            function(data){
                $("#result").html(data);
            });
    $("#assign-resource-form")[0].reset();
}


function refreshSearchResults() {
    var queryString = 'command=refreshSearchResults&filter='+$('#quickSearch').val();
    if ($('#quickSearch').val().length < 2) {
        $('.searchResults').css("display","none");
    } else {
        $('.searchResults').css("display","block");
        $.post("/ajax/",queryString,
         function(data){
                $(".searchResults").html(data);
        });
    }
}