<?php

class ClientModel extends DB_Schema {
    
    public function __construct() {
		parent::__construct();
		$this->table = "clients";
		$this->primaryKey = "id";
		$this->init();
		
		$this->formTypes["id"]["type"] = "hidden";
		$this->formTypes["account_id"]["type"] = "ignore";
		$this->formTypes["portal_enabled"]["type"] = "ignore";
		$this->formTypes["token"]["type"] = "ignore";
		
		$this->linkables["company"] = true;
		$this->linkFormats["company"] = "/clients/detail/%id%";
		
		$this->displayOrder = ["company" => 1, "first" => 2, "last" => 3, "street" => 4, "city" => 5, "state" => 6, "zip" => 7,"phone1" => 8, "phone2" => 9, "email" => 10];
		
		if (isset($_SESSION['account_id'])) {
			$this->DB->constrain("account_id", $_SESSION['account_id']);
		}
	}
    
    //override
    public function insert($fields) {
        //purpose of override is to catch the 'account_id' session var and insert it into the DB along with the form data.
		//Also, clients are composed of a name, people, and places, so the form data is split between various tables.
		$this->log_model = new LogModel();
		$newClient = new DB_Record(["account_id" => $_SESSION['account_id'], "company" => $fields['company']]);
		$clientTable = DB::table("clients");
		if($clientTable->insert($newClient)) {
			$clientid = $clientTable->insert_id;
			$this->log_model->insert(["description" => "DEBUG: Created client record " . $clientid]);
			$person = new DB_Record(["account_id" => $_SESSION['account_id'],
									 "first" => $fields['first'],
									 "last" => $fields['last'],
									 "email" => $fields['email'],
									 "work" => $fields['phone1'],"mobile" => $fields['phone2'],
									 "parent_id" => $clientid]);
			$peopleTable = App::people();
			if($person->first != "") {
				$peopleTable->insert($person);
			}
			$place = new DB_Record(["account_id" => $_SESSION['account_id'],
									 "name" => "Primary Address",
									 "street" => $fields['street'],
									 "city" => $fields['city'],
									 "state" => $fields['state'],"zip" => $fields['zip'],
									 "class" => "Office",
									 "parent_id" => $clientid]);
			$placesTable = App::places();
			if($place->street != "") {
				$placesTable->insert($place);
			}
			return SUCCESS;
		} else {
			//$this->log_model->insert(["description" => "DEBUG: failed client record " . $clientTable->error]);
			return FAILURE;
		}
    }
	
	    //override
    public function recordProvider() {
        //this function grabs records from the table
        return $this->DB->select("clients.*")->orderBy("company")->limit($this->limit)->get(); 
    }
	
	public function getClientName($id) {
		$clientName = "None";
		$clientRecord = $this->find($id);
		if($clientRecord != null){
			$clientName = $clientRecord->company;
		}
		return $clientName;
	}
    
}
?>