<?php 

/**
 * Account Model Class
 *
 * Interact with the database to process data related to accounts.
 */
class AccountModel extends DB_Schema
{
    /**
    * Sets to true if recovery link is inserted.
    * @var null
    */
    public $recovery_link_inserted;

    /**
     * Get country data
     *
     * This method gets a multi dimensional array of country data.
     * @return array
     */
    public function getCountries()
    {
        $query = [];

        $db = DB::table('countries');
        $result = $db->get();

        if ($result) {
            return $result;
        }
    }

    /**
     * Insert recovery link
     *
     * This method inserts a recovery link into the database. It expects a
     * email parameter and a token parameter.
     * @param string $email
     * @param string $token    
     */
    public function insertLink($email, $token) 
    {
        $link = new DB_Record([
            'email' => $email, 
            'token' => $token
        ]);
        $db = DB::table('reset_links');
        $db->primaryKey = 'id';
        if ($db->insert($link)) {
            $this->link_inserted = true;
        } else {
            echo $db->error;
        }
    }

    public function getRecoveryLink($param, $data) {
        $link = DB::table('reset_links')->where($param, $data)->getOne();
        if ($link) {
            return $link;
        }
    }

    public function deleteRecoveryLink($param, $data)
    {
        $link = DB::table('reset_links')->where($param, $data)->delete();
    }

    public function getSubscribers()
    {
        $query = [];
        $con = new Database();
        $result = $con->mysqli->query('SELECT * FROM `newsletter`');
        while ($row = $result->fetch_assoc()) {
            $query[] = $row;
        }
        $result->free();
        return $query;
    }

    public function getSubscriber($thing, $param, $data)
    {
        $con = new Database();
        $query = $con->mysqli->query('SELECT ' . $thing . ' FROM `newsletter` WHERE `' . $param . '` = "' . $data . '"');
        $row = $query->fetch_assoc();
        return $row;
    }

    public function insertSubscriber($email)
    {
        $con = new Database();
        $insert = $con->mysqli->prepare('INSERT INTO `newsletter` (email) VALUES(?)');
        $insert->bind_param('s', $email);
        if ($insert->execute()) {
            $this->subscription_inserted = true;
        }
    }

    public function deleteSubscription($email)
    {
        $con = new Database();
        $delete = $con->mysqli->prepare('DELETE FROM `newsletter` WHERE `email` = ?');
        $delete->bind_param('s', $email);
        if ($delete->execute()) {
            $this->subscription_deleted = true;
        }
    }
}