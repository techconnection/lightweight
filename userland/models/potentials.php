<?php

class PotentialsModel extends DB_Schema 
{
    public function __construct() 
    {
		parent::__construct();
		$this->table = "potentials";
		$this->primaryKey = "id";
		$this->init();
		
		$this->formTypes["id"]["type"]="hidden";
	}

    public function insertPotential($company, $address, $primary_phone, $username, $email, $password, $user_key) {
        $potential_record = new DB_Record([
            'company' => $company, 
            'address' => $address, 
            'phone' => $primary_phone,
            'username' => $username,
            'email' => $email,
            'password' => $password,
            'user_key' => $user_key
        ]);
        $db = DB::table('potentials');
        $db->primaryKey = 'id';
        if ($db->insert($potential_record)) {
            $this->potential_inserted = true;
        } else {
            echo $db->error;
        }
    }
}