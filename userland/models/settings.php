<?php 

/**
 * Settings Model Class
 *
 * Interact with the database to process data related to the settings.
 */
class SettingsModel extends DB_Schema
{
    public function getSetting($param, $data)
    {
        $setting = DB::table('settings')->where($param, $data)->getOne();
        if ($setting) {
            $param = $setting->attributes;
            return $setting;
        }
    }

    public function updateSetting($row, $data, $id) 
    {
        DB::table('settings')->where("id", $id)->massUpdate([$row => $data]);
        $this->user_updated = true;
    }
}