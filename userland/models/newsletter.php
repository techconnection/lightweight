<?php

/*
|--------------------------------------------------------------------------
| Subscriber Model
|--------------------------------------------------------------------------
|
|
|
*/

class NewsletterModel {
    public function getSubscribers() {
        //$query = [];
        //$db = Database::GetSharedDatabase();
        //$result = $db->query('SELECT * FROM subscribers');
        //while ($row = $result->fetch_assoc()) {
        //    $query[] = $row;
        //}
        //$result->free();
        //return $query;
        return DB::table('subscribers')->get();
    }

    public function getSubscriber($row, $data)
    {
        $db = Database::GetSharedDatabase();
        if ($this->query = $db->query('SELECT * FROM `newsletter` WHERE `' . $row . '` = "' . $data . '"')) {
            $this->row = $this->query->fetch_assoc();
        } 
    }

    public function insertSubscriber($email) {
        $db = Database::GetSharedDatabase();
        $insert = $db->prepare('INSERT INTO `newsletter` (subscriber_email) VALUES(?)');
        $insert->bind_param('s', $email);
        if ($insert->execute()) {
            $this->subscription_inserted = true;
        }
    }

    public function deleteSubscription($email) {
        $db = Database::GetSharedDatabase();
        $delete = $db->prepare('DELETE FROM `newsletter` WHERE `subscriber_email` = ?');
        $delete->bind_param('s', $email);
        if ($delete->execute()) {
            $this->subscription_deleted = true;
        }
    }
}