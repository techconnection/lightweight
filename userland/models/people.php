<?php

class PeopleModel extends DB_Schema {
    
    public function __construct() {
		parent::__construct();
		$this->table = "people";
		$this->primaryKey = "id";
		$this->init();
		
		if (isset($_SESSION['account_id'])) {
			$this->DB->constrain("account_id", $_SESSION['account_id']);
		}
	}
	
	public function displayPeopleList($id) {
		$people = $this->getPeopleForID($id);
		$display = new DB_Displayer();
		$data["people"] = $people;
		$display->template($data,"clients/peopleList");
	}
    
    //override
    public function insert($fields) {
        //purpose of override is to catch the 'account_id' session var and insert it into the DB along with the form data
        $attribs = array_merge($fields,["account_id" => $_SESSION['account_id']]);
        return parent::insert($attribs);
    }
	
	    //override
    public function recordProvider() {
        //this function grabs records from the table, however it also performs a join
        return $this->DB->select("people.*")->orderBy("id")->limit($this->limit)->get(); 
    }
	
	public function getPeopleForID($id) {
		$db = App::people()->where("parent_id",$id);
		return $db->get();
	}
    
}
?>