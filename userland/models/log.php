<?php

class LogModel extends DB_Schema {
    
    public function __construct() {
		parent::__construct();
		$this->table = "history";
		$this->primaryKey = "id";
		$this->orderBy = "date_added desc";
		$this->limit = "15";
		$this->init();
		
		$this->formTypes["id"]["type"]="hidden";
		$this->formTypes["account_id"]["type"]="ignore";
		$this->formTypes["clientid"]["type"]="ignore";
		$this->formTypes["ticketid"]["type"]="ignore";
		$this->formTypes["userid"]["type"]="ignore";
		
		$this->linkables["username"] = true;
		$this->linkFormats["username"] = "/users/detail/%id%";
		
		if (isset($_SESSION['account_id'])) {
			$this->DB->constrain("account_id", $_SESSION['account_id']);
		}
	}
    
    //override
    public function insert($fields = []) {
        //purpose of override is to catch the 'account_id' session var and insert it into the DB along with the form data
        $attribs = array_merge($fields,[
										"account_id" => $_SESSION['account_id'],
										"userid" => $_SESSION['user_id'],
										"username" => $_SESSION['username'],
										]);
        parent::insert($attribs);
    }
	
	public function recentItems() {
		$this->DB->reset();
		return $this->DB->whereWithinLast24Hours("date_added")->limit(" 10 ")->orderBy("date_added DESC")->get();
	}
	
	public function olderItems() {
		$this->DB->reset();
		return $this->DB->whereOlderThan24Hours("date_added")->limit(" 10 ")->orderBy("date_added DESC")->get();
	}
	
	public function getClientName($id) {
		$clientName = "None";
		$clientRecord = $this->find($id);
		if($clientRecord != null){
			$clientName = $clientRecord->company;
		}
		return $clientName;
	}
    
}
?>