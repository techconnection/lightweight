<?php 

/*
|--------------------------------------------------------------------------
| Project Model
|--------------------------------------------------------------------------
|
| 
|
*/
function printOneCrumb(DB_Record $projRecord, $useLink = true) 
{
    echo " > ";
    if ($useLink == true){
        echo '<a href="/projects/detail/'.$projRecord->id.'">'.$projRecord->projectname.'</a>';
    } else {
        echo $projRecord->projectname;
    }
    // echo "</li><li class=\"bc-divider\"></li>";
}
    
function getBreadCrumbs($projID, &$crumbs) 
{
    $projRecord = App::tickets()->find($projID);
    if ($projRecord != null) {
        $crumbs[] = $projRecord;
        getBreadCrumbs($projRecord->parent_id,$crumbs);
    } else {
        return;
    }
}

function displayBreadCrumbs($projID, $useLink = false) 
{
    ob_start();
    $crumbs = [];
    getBreadCrumbs($projID,$crumbs);
    $crumbs = array_reverse($crumbs);
    $current = 0;
    $last = count($crumbs)-1;
    if (!empty($crumbs)) {
        foreach($crumbs as $projRecord) {
            if ($current == $last) { 
                printOneCrumb($projRecord, $useLink);
            } else {
                printOneCrumb($projRecord);
            }
            $current++;
        }
    }
    return ob_get_clean();
}

/*
function printDailyProjectRow(DB_Record $projRecord, $indent = 0, $summarize = false) {
    
    $subTaskCount = getChildCount($projRecord->id, true); //let's count the children with open status
    $totalTaskCount = getChildCount($projRecord->id);
    $dateRange = $projRecord->endDate==null ? $projRecord->startDate : $projRecord->startDate." - ".$projRecord->endDate;
    $server = $_SERVER['SERVER_NAME'];
    $s = $summarize == false ? 'detail' : 'summary';
    $margin = $indent * 16;
    
    $clientName = "None";
    $clientid = "";
    $client_model = new Client_Model();
    $clientRecord = $client_model->find($projRecord->clientid);
    if ($clientRecord != null){
        $clientName = $clientRecord->company;
        $clientid = $clientRecord->id;
    }
    $fileCount = count(App::documents()->where("projectid",$projRecord->id)->get());
    $newFileCount = count(App::documents()->where("projectid",$projRecord->id)->whereWithinLast24Hours("dateuploaded")->get());
    echo <<<EOL
    <tr class="dr">
    <td>
    <a href="http://{$server}/clients/{$clientid}">{$clientName}</a>
    </td>
    <td>
    <a style="margin-left: {$margin}px;" href="http://{$server}/projects/{$s}/{$projRecord->id}">{$projRecord->projectname}</a><br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://{$server}/projects/tasks/{$projRecord->id}">{$subTaskCount}</a> open sub-tasks (out of {$totalTaskCount}).<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://{$server}/projects/files/{$projRecord->id}">{$newFileCount}</a> new files (out of {$fileCount}).<br>
    </td>
    <td>
    {$projRecord->status}
    </td>
    <td>
    {$projRecord->type}
    </td>
    <td>{$dateRange}
    </td>
    </tr>
EOL;
}
*/

function printProjectRow(DB_Record $projRecord, $indent = 0, $summarize = false) 
{
    $s = $summarize == false ? 'detail' : 'summary';
    $dateRange = $projRecord->endDate==null ? $projRecord->startDate : $projRecord->startDate." - ".$projRecord->endDate;
    $margin = $indent * 16;

    $client_model = new ClientModel();
    $clientName = $client_model->getClientName($projRecord->clientid);
    $clientid = $projRecord->clientid==0 ? "" : $projRecord->clientid;

    echo <<<EOL
    <tr class="dr">
    <td>
    <a class="{$projRecord->status}" style="margin-left: {$margin}px;" href="/projects/{$s}/{$projRecord->id}">{$projRecord->projectname}</a>
    </td>
    <td>
    {$projRecord->status}
    </td>
    <td>
    {$dateRange}
    </td>
    </tr>
EOL;
}

function displayChildOutline($projID, $indent = 0) 
{
    $projects = App::tickets()->where("parent_id", $projID)->get();
    if (!empty($projects)) {
        foreach($projects as $projRecord) {
            printProjectRow($projRecord, $indent);
            displayChildOutline($projRecord->id, $indent+1);
        }
    } else {
        return;
    }
}


function getDocumentCount($projID)
{
    return App::documents()->where("projectid",$projID)->count();
}


function getChildDocumentCount($projID) 
{
    $count = 0;
    $projects = App::tickets()->where("parent_id", $projID)->get();

    if (!empty($projects)) {
        foreach($projects as $projRecord) {
            $count += getDocumentCount($projRecord->id);
            $count += getChildDocumentCount($projRecord->id);
        }
    }
    return $count;
}




function getChildCount($projID,$open=false) 
{
    $count = 0;
    if ($open == false) {
        $projects = App::tickets()->where("parent_id", $projID)->get();
    } else {
        $projects = App::tickets()
        ->where("parent_id", $projID)
        ->where("status","Open")
        ->get();
    }
    
    if (!empty($projects)) {
        foreach($projects as $projRecord) {
            $count++;
            $count += getChildCount($projRecord->id,$open);
        }
    }
    return $count;
}


function displayTaskList($projID)
{
    ob_start();
    echo "<table><tr><th>Description</th><th>Status</th><th>Date range</th></tr>";
    displayChildOutline($projID);
    echo "</table>";
    return ob_get_clean();
}


function displayAssignedResourcesList($projID)
{
    ob_start();
    $resources = [];
    $assignments = App::resource_assignment()
                         ->where("ticket_id",$projID)
                         ->get();
    foreach($assignments as $assignment){
        $resource = App::resources()->find($assignment->resource_id);
        if ($resource) {
            $resource->append("assignmentid",$assignment->id);
            $resources[] = $resource;
        }
    }
    
    $display = new DB_Displayer();
	$data["resources"] = $resources;
    $data["projID"] = $projID;
	$display->template($data,"projects/resources-assigned");
    return ob_get_clean();
}


function displayResourcesSELECT($projID = 0)
{
    ob_start();
    $disabled = "";
    $allResources = App::resources()->get();
    foreach($allResources as $resource){
        if ($projID) {
            $alreadyAssigned = App::resource_assignment()
                         ->where("ticket_id",$projID)
                         ->where("resource_id", $resource->id)
                         ->get();
            if ($alreadyAssigned == null) {
                $disabled = "";
            } else {
                $disabled = " disabled";
            }
        }
        echo "<OPTION value=".$resource->id.$disabled.">".$resource->name."</OPTION>";
    }
    return ob_get_clean();
}

function displayDocuments($projID) 
{
    $docRecords = App::documents()->where("projectid",$projID)->get();
    ob_start();
    if (count($docRecords)!=0) {
        echo '<ul>';
    }
    foreach($docRecords as $doc){
        echo "<li>";
        echo "<a href='../../application/documents/{$doc->nameondisk}' target='_new'>";
        $ext = strtolower(pathinfo($doc->documentname, PATHINFO_EXTENSION));
        switch($ext) {
            case "png":
            case "jpg":
            case "jpeg":
            case "gif":
            case "svg":
            case "bmp":
                echo "<img src='../../application/documents/{$doc->nameondisk}' width=64 height=64>";
                break;
            case "pdf":
                echo "<img src='../../application/storage/images/pdf.png' width=64 height=64>";
                break;
            case "zip":
                echo "<img src='../../application/storage/images/zip.png' width=64 height=64>";
                break;
            default:
                echo "<img src='../../application/storage/images/doc.png' width=64 height=64>";  
        }
        echo "<h3>".$doc->documentname."</h3>";
        echo "</a>";
        echo '<input type="checkbox" name="selected" value="'.$doc->id.'">';
        echo "</li>";
    }
    if (count($docRecords)!=0) {
        echo "</ul>";
    }
    else {
        echo "<h3>No files uploaded.</h3>";
    }
    return ob_get_clean();
}


class ProjectModel extends DB_Schema 
{

    public $project_inserted;
    
    public function __construct() 
    {
		parent::__construct();
		$this->table = "tickets";
		$this->primaryKey = "id";
		$this->init();
		
		$this->formTypes["id"]["type"]="hidden";
		$this->formTypes["account_id"]["type"]="ignore";
		$this->formTypes["portal_enabled"]["type"]="ignore";
		$this->formTypes["token"]["type"]="ignore";
		
		$this->linkables["projectname"] = true;
		$this->linkFormats["projectname"] = "/projects/detail/%id%";
		
		if (isset($_SESSION['account_id'])) {
			$this->DB->constrain("account_id", $_SESSION['account_id']);
		}
	}
    
    public function rootProjects($client,$state) 
    {
        $db = App::tickets()->select("tickets.*, clients.company")
        ->where("parent_id", 0)
        ->where("type", "project")
        ->where("tickets.state",$state)
        ->leftJoin("clients", "tickets.clientid", "clients.id")->orderBy("clients.company");
        if ($client!=null) {
            $db->where("clientid", $client);
        }
        return $db;
    }
    
    /***********************************
    *
    *  getRootProjects
    *
    *  A root project is one that does not have any parent, ie, it is the 'top' or main ticket that is the parent for
    *  the rest of the tickets associated with the project.  The child tickets should inherit their fields from the
    *  root project unless the user overrides them.
    *
    *  This function returns an array of root projects in DB_Record format.
    *
    */
    public function getRootProjects($client = null, $state = "active") 
    {
        $db = $this->rootProjects($client, $state);
        $projects = $db->get();
        foreach($projects as $proj) {
            $progress = $this->getProgressAmount($proj->id);
            $proj->attributes["progress"] = $progress;
            if ($progress == 100) {
                $proj->attributes["progress"] = 'DONE!'; 
            } elseif ($progress == 0) {
                $proj->attributes["progress"] = '';
            } elseif ($progress == -1) {
                $proj->attributes["progress"] = '<div class="meter"><span style="width: 1%"></span></div>';
            } else {
                $proj->attributes["progress"] = '<div class="meter"><span style="width: '.$progress.'%"></span></div>';
            }
        }
        return $projects;
    }
    
    public function getRootProjectsCount($client = null, $state = "active") 
    {
        $db = $this->rootProjects($client, $state);
        return $db->count();
    }
    
    public function displayProjectList($client = null, $state = "active") 
    {
		$recs = $this->getRootProjects($client, $state);
		$display = new DB_Displayer();
		$data["projects"] = $recs;
		$display->template($data,"projects/table");
	}
    
    public function getTodaysProjects($client = null, $state = "active", $tasks = false) 
    {
        //select * from tickets where curdate() >= `startDate` AND startDate != "" UNION select * from tickets where curdate() <= `endDate`
        $compareString = $tasks ? " != " : " = ";
        $db = App::tickets()->select("tickets.*")
        ->where("type", "project")
        ->where("parent_id", "0", $compareString)
        ->where("tickets.state",$state)
        ->where("status", "Open")
        ->where("startDate", "curdate()", " < ", " AND ", "")
        ->where("startDate", "", " != ");
        if ($client != null) {
            $db->where("clientid", $client);
        }
        //$db->buildQuerySelectString();
        //echo $db->query;
        return $db->get();
    }
    
    public function getCompletedProjectsCount($client = null) 
    {
        $db = App::tickets()->select("tickets.*, clients.company")
                ->where("parent_id", 0)
                ->where("type","project")
                ->where("status","Closed")
                ->leftJoin("clients", "tickets.clientid", "clients.id")->orderBy("clients.company");
        if ($client!=null) {
            $db->where("clientid",$client);
        }
        return $db->count();
    }
    
    public static function getRootConfigurations($client = null) 
    {
        $db =  App::tickets()->where("parent_id", 0)->where("type","config");
        if ($client!=null) {
            $db->where("clientid",$client);
        }
        return $db->get();
    }
    
    public static function getRootConfigurationsCount($client = null) 
    {
        $db =  App::tickets()->where("parent_id", 0)->where("type","config");
        if ($client!=null) {
            $db->where("clientid",$client);
        }
        return $db->count();
    }
    
    public function getProgressAmount($projID) 
    {
        $totalItems = getChildCount($projID);
        $totalOpenItems = getChildCount($projID,true);
        $totalClosedItems = $totalItems - $totalOpenItems;
        if ($totalItems!=0) {
            $amount = ceil(($totalClosedItems/$totalItems)*100);
        } else {
            $amount = 0;
        }
        if ($amount==0 & $totalItems >= 1)
            return -1;
        else
            return $amount;
    }

    public function get($projID) 
    {
        return App::tickets()->find($projID); 
    }
    
    public function delete($projID) 
    {
        //kill the primary ticket, kill any children, kill all document references (files not touched)
        App::tickets()->where("id",$projID)->delete();
        App::tickets()->where("parent_id",$projID)->delete();
        App::documents()->where("projectid",$projID)->delete();
    }

    public function quickInsert($project_name, $parent, $clientid = 0, $type = "project") 
    {
        $attribs = ["projectname" => $project_name,
                    "parent_id" => $parent,
                    "clientid" => $clientid,
                    "type" => $type,
                    "state" => "active",
                    "status" => "Open",
                    "account_id" => $_SESSION['account_id']];
        //before the insert let's inherit from our parent ticket
        $parentRec = App::tickets()->find($parent);
        if ($parentRec != null) {
            $attribs["clientid"] = $parentRec->clientid;
            //whatever else it should inherit goes here
        }
        $record = new DB_Record($attribs);
        if (App::tickets()->insert($record)) {
            $this->project_inserted = true;
        }
    }
    
    public function insertResource($fields) 
    {
        $attribs = array_merge($fields,["account_id" => $_SESSION['account_id']]);
        $record = new DB_Record($attribs);
        App::resources()->insert($record);
    }
    
    public function assignResource($fields) 
    {
        $attribs = array_merge($fields,["account_id" => $_SESSION['account_id']]);
        $record = new DB_Record($attribs);
        if (App::resource_assignment()->insert($record)) {
            return SUCCESS;
        } else {
            return FAILURE;
        }
    }
    
    public function unassignResource($id) 
    {
        if (App::resource_assignment()->where("id",$id)->delete()) {
            return SUCCESS;
        } else {
            return FAILURE;
        }
    }
    
    public function deleteResource($id) 
    {
        App::resources()->where("id",$id)->delete();
        return SUCCESS;
    }
    
    public function updateResource($fields) 
    {
        $fields['name'] = trim(preg_replace('/[^A-Za-z0-9?![:space:]]/', '', $fields['name']));
        $record = new DB_Record($fields, true);
        if (App::resources()->update($record)) {
            return SUCCESS;
        } else {
            return FAILURE;
        }
    }
    
    public function assignedResourcesCount($projID) 
    {
        $recs = App::resource_assignment()->where("ticket_id",$projID)->get();
        return count($recs);
    }
    
    public function update($fields) 
    {
        $fields['projectname'] = trim(preg_replace('/[^A-Za-z0-9?![:space:]]/', '', $fields['projectname']));
        $record = new DB_Record($fields, true);
        if (App::tickets()->update($record)) {
            return SUCCESS;
        } else {
            return FAILURE;
        }
    }
}