<?php

class InvitesModel extends DB_Schema 
{
    public function __construct() 
    {
		parent::__construct();
		$this->table = "invites";
		$this->primaryKey = "id";
		$this->init();
		
		$this->formTypes["id"]["type"]="hidden";
	}   
}