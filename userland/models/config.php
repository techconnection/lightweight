<?php 

/*
|--------------------------------------------------------------------------
| Config Model
|--------------------------------------------------------------------------
|
| 
|
*/



class ConfigModel extends DB_Schema {
    
    public function __construct() {
		parent::__construct();
		$this->table = "tickets";
		$this->primaryKey = "id";
		$this->init();
		
		$this->formTypes["id"]["type"]="hidden";
		$this->formTypes["account_id"]["type"]="ignore";
        $this->formTypes["parent_id"]["type"]="ignore";
        $this->formTypes["clientid"]["type"]="hidden";
        $this->formTypes["type"]["type"]="ignore";
        $this->formTypes["status"]["type"]="ignore";
		
		$this->linkables["projectname"] = true;
		$this->linkFormats["projectname"] = "/configs/detail/%id%";
        $this->linkables["company"] = true;
		$this->linkFormats["company"] = "/clients/detail/%clientid%";
        
        $this->formTypes["projectname"]["label"]="Name";
        $this->formTypes["scope"]["label"]="Description";
        
        $this->displayOrder = ["company" => 1, "projectname" => 2, "scope" => 3];
		
		if (isset($_SESSION['account_id'])) {
			$this->DB->constrain("tickets.account_id", $_SESSION['account_id']);
		}
        $this->DB->constrain("tickets.type", "config");
	}
    

    public static function getRootConfigurations() {
        return App::tickets()->where("parent_id", 0)->where("type","config")->get();
    }

    
    //override
    public function recordProvider() {
        //this function grabs records from the table, however it also performs a join
        return $this->DB->select("tickets.*, clients.company")->leftJoin("clients","tickets.clientid","clients.id")->orderBy("clients.company, tickets.id")->limit($this->limit)->get(); 
    }
    
    
    //override
    public function delete($projID) {
        //kill the primary ticket, kill any children, kill all document references (files not touched)
        App::tickets()->where("id",$projID)->delete();
        App::tickets()->where("parent_id",$projID)->delete();
        App::documents()->where("projectid",$projID)->delete();
    }

    public function quickInsert($project_name, $parent, $clientid = 0, $type="config") {
        $attribs = ["projectname" => $project_name,
                    "parent_id" => $parent,
                    "clientid" => $clientid,
                    "type" => $type,
                    "account_id" => $_SESSION['account_id']];
        //before the insert let's inherit from our parent ticket
        $parentRec = App::tickets()->find($parent);
        if($parentRec != null) {
            $attribs["clientid"] = $parentRec->clientid;
            //whatever else it should inherit goes here
        }
        $record = new DB_Record($attribs);
        if (App::tickets()->insert($record)) {
            $this->project_inserted = true;
        }
    }
    
    //override
    public function update($fields) {
        $fields['projectname'] = trim(preg_replace('/[^A-Za-z0-9?![:space:]]/', '', $fields['projectname']));
        parent::update($fields);
    }
}