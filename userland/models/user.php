<?php 

/**
 * User Model Class
 *
 * Interact with the database to process data related to the users.
 */
class UserModel extends DB_Schema 
{
    public $user_inserted;
    public function __construct() 
    {
        parent::__construct();
        $this->table = "users";
        $this->primaryKey = "user_id";
        $this->init();
        
        $this->formTypes["user_id"]["type"]="hidden";
        $this->formTypes["account_id"]["type"]="ignore";
        $this->formTypes["user_key"]["type"]="ignore";
        $this->formTypes["password"]["type"]="ignore";
        $this->formTypes["date_added"]["type"]="ignore";
        $this->formTypes["ip"]["type"]="ignore";
        $this->formTypes["bio"]["type"]="ignore";
        $this->formTypes["newsletter"]["type"]="ignore";
        $this->formTypes["status"]["type"]="ignore";
        $this->formTypes["last_activity"]["type"]="ignore";
        $this->formTypes["user_group"]["type"]="ignore";
        
        $this->linkables["username"] = true;
        $this->linkFormats["username"] = "/users/detail/%id%";
        
    }
    
    public function insertUser($company, $address, $phone, $firstname, $lastname, $username, $email, $password, $user_key, $ip) 
    {
        $user_record = new DB_Record([
            'company' => $company,
            'address' => $address,
            'phone' => $phone,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'username' => $username, 
            'email' => $email, 
            'password' => $password, 
            'user_key' => $user_key, 
            'ip' => $ip
        ]);
        $db = DB::table('users');
        $db->primaryKey = "user_id";
        if ($db->insert($user_record)) {
            $this->user_inserted = true;
        } else {
            echo $db->error;
        }
    }

    public function activateUser($row, $data, $id) 
    {
        DB::table('users')
        ->where("user_id", $id)
        ->massUpdate([$row => $data]);
        $this->account_activated = true;
    }

    public function updateUser($row, $data, $id) 
    {
        DB::table('users')->where('user_id', $id)->massUpdate([$row => $data]);
        $this->user_updated = true;
    }

    public function getUser($param, $data) {
        $userRec = DB::table('users')->where($param, $data)->getOne();
        if ($userRec) {
            return $userRec;
        }
    }

    public function updateUsername($username, $id) 
    {
        DB::table('users')
        ->where('id', $id)
        ->massUpdate(["username" => $username]);
        $this->username_updated = true;
    }

    public function insertRecoveryLink($email, $token, $date_created) 
    {
        $linkRecord = new DB_Record([
            "email" => $email,
            "token" => $token,
            "date_created" => $date_created
        ]);
        if (DB::table('recovery_links')->insert($linkRecord)) {
            $this->recovery_link_inserted = true;
        }
    }

    public function getRecoveryLink($token) 
    {
        $userRec = DB::table('recovery_links')
            ->where('token', $token)
            ->getOne();
        if($userRec != null) {
            $this->row = $userRec->attributes;
        }
    }

    public function deleteRecoveryLink($token) 
    {
        DB::table('recovery_links')
        ->where('token', $token)
        ->delete();
    }
}