# README #

Project Tracker

### What is this repository for? ###

* Its for Project Tracker!
* Version 0.001
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Code Flow ###

The general format for this app at this time is as follows:
* All incoming http requests get redirected to index.php
* The index does all the basic includes and boot strapping and then creates a loader class and passes control.
* The loader class looks at the URL the browser requested and breaks it up into components
* Based on the URL, we look for a file with that name in app/controllers
* Example: tekcx.com/projects will look for projects.php in controllers.
* The loader will instantiate any matching named object defined in the controller file.
* The loader will then try and call a method on that object based on the URL
* teckcx.com/projects/detail/500 for instance instantiates class Projects and calls method detail(500);
* Control is now passed to this controller object
* The controller object loads its own model object and view object to display the page and do any calculations.
* Program flow completes.


### Contribution guidelines ###

* Fixed a mispelling
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact