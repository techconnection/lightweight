-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2016 at 02:30 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projects`
--

-- --------------------------------------------------------

--
-- Table structure for table `recovery_links`
--

CREATE TABLE `recovery_links` (
  `id` int(11) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `token` varchar(100) CHARACTER SET utf8 NOT NULL,
  `date_created` varchar(56) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `projectname` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `projectname`, `parent_id`) VALUES
(1, 'Vintage Cave Wiring', 0),
(2, 'Trutag Wiring', 0),
(3, 'Phase 1 - Paperwork', 1),
(4, 'Phase 1 - Paperwork', 2),
(5, 'Phase 2 - Materials', 1),
(6, 'Bid on job', 3),
(7, 'Collect Contact Info', 3),
(8, 'Purchase backboard from home depot', 5),
(9, 'Purchase LAN materials at Industrial', 5),
(10, 'Check with Manfred about shipping', 9),
(11, 'Seagull Schools WAP Install', 0),
(12, 'Gouviea Purity Foods LAN Install', 0),
(15, 'Test Project 1', 0),
(16, 'Test Project 2', 0),
(17, 'Test or', 0),
(18, 'Derek Job 2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `lastname` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(56) CHARACTER SET utf8 NOT NULL,
  `username` varchar(20) CHARACTER SET utf8 NOT NULL,
  `password` varchar(60) CHARACTER SET utf8 NOT NULL,
  `user_key` varchar(60) CHARACTER SET utf8 NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_activity` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `user_group` int(1) NOT NULL DEFAULT '0',
  `avatar` varchar(100) CHARACTER SET utf8 DEFAULT 'default.png',
  `bio` text CHARACTER SET utf8,
  `newsletter` int(1) NOT NULL DEFAULT '0',
  `ip` varchar(56) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `email`, `username`, `password`, `user_key`, `date_added`, `last_activity`, `status`, `user_group`, `avatar`, `bio`, `newsletter`, `ip`) VALUES
(1, NULL, NULL, 'derek@yourtechconnection.com', 'badman', '$2y$12$YgePlctZVA/6Y4xwLbaOhefipS3CTewkLoz0WQzzFrXVgzVOElHEm', '1f4a9f76d39c4b2924330135196bd814', '2016-11-09 22:25:26', '1479159989', 1, 3, 'default.png', NULL, 0, '::1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `recovery_links`
--
ALTER TABLE `recovery_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `recovery_links`
--
ALTER TABLE `recovery_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
